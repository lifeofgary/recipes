---
tags:
  - bowl
  - sauce
  - untried
---
From *Paleo Power Bowls* pg 323

| Amount  | Ingredient            |
| ------- | --------------------- |
| 3 Cup   | Basil (fresh, packed) |
| 2/3 Cup | Pine Nutes            |
| 4 Clove | Garlic                |
| 3 Tbsp  | Nutritional yeast     |
| 1/2 tsp | Salt                  |
| 1/2 Cup | Oil (olive)           |
1. Blend