---
tags:
  - untried
---
From: *PlantPure Comfort food* by Kim Campbell page 145 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1  | Onion (diced) |
| 2 teaspoon | Ginger (grated) |
| 4 cloves | Garlic |
| 1/4 teaspoon | Turmeric |
| 7 ounces | Tofu (extra firm) |
| 1 1/2 Cup | Pineapple (frozen or canned) |
| 1  | Red Pepper (diced) |
| 1  | Carrot (diced) |
| 3/4 Cup | Green onion (sliced) |
| 1/4 Cup | Soy Sauce |
| 1 Tablespoon | Maple Syrup |
| 1 Tablespoon | Lime Juice |
| 1-2 teaspoon | Sriracha |
| 3 Cup | Rice (cooked) |
| 1/4 Cup | Cilantro (fresh) |
| 1/2 Cup | Peanuts |
| 1  | Lime (in wedges) |

1. Sauté onion
2. Add and Sauté
	1. Ginger
	2. Garlic
	3. Turmeric
	4. Tofu
3. Add and Stir-fry for 3-4 minutes.  Add water as needed
	1. Pineapple
	2. Red Peppers
	3. Carrot
	4. Green Onion
	5. Peas
4. Add and mix well
	1. Soy Sauce
	2. Maple Syrup
	3. Lime Juice
	4. Sriracha
5. Add then turn heat down to medium low and cook until rice is heated
	1. Rice
6. Serve and top with
	1. Cilantro
	2. Peanuts
	3. Lime wedges for squeezing
