---
tags:
  - bowl
  - untried
---
From *Americas test kitchen - Bowls* pg 217

| Amount  | Ingredient         |
| ------- | ------------------ |
| 1 Cup   | Vinegar (Red Wine) |
| 1/3 Cup | Sugar              |
| 1.8 tsp | Salt               |
| 1       | Onion (Red)        |
| 2       | Jalapeno           |
1. Microwave til simmering (1-2 min)
	1. Jalapenos
	2. Vinegar
	3. Sugar
	4. Salt
2. Add and soak for 45 min
	1. Onion
3. Drain