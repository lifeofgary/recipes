---
tags:
  - instapot
  - stars/5
---
From: *Vegan Richa's Instant Pot Cookbook* page 50
Rating: 5/5

| Amount | Ingredient |
|----------|-------------|
| 6 (750 g) | Potatoes |
| 5 Clover | Garlic |
| 1/2 teaspoon | Salt |
| 1 1/2 Cup | Stock |
| 1 Tablespoon | Oil |
| 1/2 teaspoon | Parsley or Thyme |
| Pinch | Nutmeg |
| 1/2 - 1 Cup | Coconut Milk |

1. Add to Instapot 
	1. Potato
	2. Garlic
	3. Salt
	4. Stock
2. Cook at `High Pressure` for 4 minutes
3. Release pressure for 5 minutes
4. Mash and mix in
	1. Oil
	2. Parsley
	3. Nutmeg
	4. Coconut Milk

Works well wilth [[Shepherd's Pie]]