---
tags:
  - untried
---
From: *The Vegan Air Fryer* page 53
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Flour (gluten free) |
| 1 | [[Flax Egg]] |
| 1/2 Cup | Panko Bread Crumbs |
| 1 teaspoon | Chili Powder |
| 1 | Avocado |
| | Oil Spray |

1. Put `Flour` in a bowl
2. Put `[[Flax Egg]]` in a bowl
3. Mix in a bowl
	1. Panko
	2. Chili Powder
4. Dip `Avocado` in all 3 bowls
5. Spray `Avocado` with `Oil`
6. Airfry `Avocado` at 390F for 12 minutes


7. Sauté for 10 minutes