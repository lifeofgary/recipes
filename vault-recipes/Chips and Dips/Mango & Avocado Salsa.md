---
tags:
  - untried
---
From: *The Green Barbecue* page 153
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Avocado (unripe, 1/4 inch cubes) |
| 1 | Mango (unripe, 1/4 inch cubes) |
| 1/2 | Pomegranate (seeds only) |
| 1 | Red Chili |
| 2 | Limes (juice and zest) |
| 3/4 oz | Basil |
| 1 Pinch | Salt |
