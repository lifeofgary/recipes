---
tags:
  - bowl
  - sauce
  - untried
---
From *Paleo Power Bowls* pg 342

| Amount  | Ingredient    |
| ------- | ------------- |
| 1/2 Cup | Mayonnaise    |
| 1/4 Cup | Coconut Cream |
| 2 Tbsp  | Lemon Juice   |
| 1 Clove | Garlic        |
| 1/2 tsp | Salt          |
| 1 tsp   | Parsley       |
| 1 tsp   | Dill          |
| 1 tsp   | Chives        |
| 1 tsp   | Oregano       |
1. Mix