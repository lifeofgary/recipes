---
tags:
  - untried
---
From: *Unknown library book* page 17
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Coconut Oil |
| 1/8 teaspoon | Paprika |
| 1/8 teaspoon | Garlic Powder | 
| 1/8 teaspoon | Liquid Smoke |
| 1/4 teaspoon | Maple Syrup |

1. Combine
	1. Oil
	2. Paprika
	3. Garlic Powder
	4. Liquid Smoke
	5. Maple Syrup

Adds a smoky, bacony flavor