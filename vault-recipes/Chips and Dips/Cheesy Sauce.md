---
tags:
  - untried
  - sauce
---
From: *Unknown Library Book* page 75
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Milk (nondairy) |
| 1/2 Cup | Bell Pepper (orange) |
| 1/2 Cup | Nutritional Yeast |
| 2 Tablespoon | Arrowroot |
| 2 Tablespoon | Tahini |
| 2 teaspoon | Dijon Mustard |
| 1 1/2 teaspoon | Garlic Powder |
| 3/4 teaspoon | Onion Powder |
| 3/4 teaspoon | Salt |
| 3/4 teaspoon | Paprika |

1. Blend for 2 minutes
	1. 2/3 Cup Milk
	2. Bell Pepper
	3. Nutritional Yeast
	4. Arrow Root
	5. Tahini
	6. Dijon Mustard
	7. Garlic Powder
	8. Onion Powder
	9. Salt
	10. Paprika
2. Add 
	1. 1/3 Cup Milk
3. Blend for 15 seconds
4. Cook on medium for 3-5 minute