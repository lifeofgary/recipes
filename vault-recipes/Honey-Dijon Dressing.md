---
tags:
  - bowl
  - sauce
  - untried
---
From *Paleo Power Bowls* pg 332

| Amount  | Ingredient      |
| ------- | --------------- |
| 1/3 Cup | Oil (olive)     |
| 3 Tbsp  | Mustard (dijon) |
| 3 Tbsp  | Vinegar         |
| 2 Tbsp  | Honey           |
| 1/2 tsp | Salt            |
1. Mix