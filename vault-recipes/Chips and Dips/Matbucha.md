---
tags:
  - untried
  - instapot
---
From: *Unknown Library Book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 | Bell Peppers (red) |
| 1 Tablespoon | Oil (olive) |
| 2 | Jalapeños |
| 2 Cloves | Garlic |
| 1 Can | Tomatoes |
| 2 teaspoon | Sugar |
| 1  teaspoon | Cumin |
| 1 teaspoon | Salt |
| 1/2 teaspoon | Paprika |

1. Put Bell Peppers on trivet in Pressure Cooker with 1 Cup Water
2. Cook for 5 minutes, with manual release for 10 minutes
3. Remove skins from peppers
4. Sauté for 3 minutes
	1. Oil
	2. Jalapeños
5. Add and Sauté for 1 minute
	1. Garlic
6. Add and cook for 20 minutes with manual release for 10 minutes
	1. Peppers
	2. Tomatoes with juice
	3. Sugar
	4. Cumin
	5. Salt
	6. Paprika
7. Sauté on less for 30 minutes