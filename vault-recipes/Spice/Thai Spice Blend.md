---
tags:
  - untried
---
From: *Spice Diet* page 240
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 Tablespoons | Mint |
| 2 Tablespoon | Lime Zest or Lime Leaves |
| 2 teaspoon | Lemongrass |
| 1/4 teaspoon | Cumin |
| 1/2 teaspoon | Cayenne |
| 1 Tablespoon | Cilantro (dried) |
| 1 Tablespoon | Coconut (shredded, unsweetened) 
