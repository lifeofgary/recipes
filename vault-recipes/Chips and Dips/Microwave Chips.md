---
tags:
  - untried
---
From: *Unknown Library Book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Quarts | Hot Water |
| 1 lb. | Potato |
| 2 teaspoon | Oil (olive) |
| 1/2 teaspoon | Salt |
| | Cooking Spray |
| | Seasoning (your choice) |

1. Cut potato thin
2. Soak for 10 - 15 minutes
3. Drain
4. Rinse under hot water for a few seconds
5. Spread on towel and blot dry
6. Mix
	1. Potato
	2. Oil
	3. Salt
	4. Seasoning
7. Spray oil on cooking surface
8. Microwave on high for 3 minutes
9. Microwave on high for 1 minute
10. Repeat 9 until golden brown

Variation
	1. Use Yucca or Taro
	2. Try Carrot or non-starchy veggies, but skip steps 1-4