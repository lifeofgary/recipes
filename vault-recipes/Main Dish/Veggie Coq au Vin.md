---
tags:
  - untried
---
From: *30 Minute Vegan: Soups on* page 112 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 Cup | Stock |
| 2 Cup | Red Wine |
| 2 | Bay Leaves |
| 1 teaspoon | Thyme (dried) |
| 10 | Onion (pearl) |
| 1/2 Cup | Celery |
| 6 Cloves | Garlic |
| 1 Cup | Carrot |
| 1 Cup | Potato |
| 1 Cup | Mushrooms (cremini, quartered) |
| 12 oz or 1 1/2 Cup | Chicken or Protein |
| 3 Tablespoon | Tomato Paste |
| 1 Tablespoon | Vinegar (Balsamic) |
| 1/2 teaspoon | Liquid Smoke |
| 3 Tablespoon | Parsley |
| 2 teaspoon | Salt |
| 2 teaspoon | Tamari |
| 3 Tablespoon | Flour (gluten-free) |
| 1 1/2 Tablespoon | Oil (olive) |

1. Cook for 5 minutes
	1. Stock
	2. Wine
	3. Bay Leaves
	4. Thyme
	5. Onion
	6. Celery
	7. Garlic
	8. Carrot
	9. Potato
2. Add and cook for 15 minutes
	1. Mushrooms
	2. Chicken
	3. Tomato Paste
3. Add
	1. Vinegar
	2. Liquid Smoke
	3. Parsley
	4. Salt
	5. Tamari
4. Create a Roux by Sauteing for 1 minute
	1. Flour
	2. Oil
5. Add Roux to Soup and Cook for 5 minutes
6. Remove Bay Leaves

Variations
1. Sauté Onion, Celery and Garlic for 3 minutes in 1 Tablespoon of Oil
2. Omit Roux
3. Replace Cremini mushrooms with Morel, Porcini or Chanterelle
4. Add 2 Tablespoon Basil and 1 Tablespoon fresh Marjoram with Parsley


5. Sauté for 10 minutes