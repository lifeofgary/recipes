---
tags:
  - stars/3
---
From: *30-Minute Vegan Soups On!* page 101 
Rating: 3/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (sesame) |
| 1 Tablespoon | Curry Powder |
| 2 teaspoon | Cumin |
| 1 Cup | Onion (diced) |
| 3 cloves | Garlic |
| 6 Cups | Stock |
| 1/2 Cup | Lentils |
| 1/4 Cup | Rice (Basmati) |
| 1 1/2 Cup | Potato |
| 1 1/4 Cup | Apple |
| 1/2 Cup | Celery |
| 3/4 Cup | Carrot |
| 1 1/2 Cup | Coconut Milk |
| 2 teaspoon | [[Garam Masala]] |
| 1 teaspoon | Tamarind |
| 1 Tablespoon | Tamari |
| 1 teaspoon | Salt |

1. Sauté for 1 minutes
	1. Oil
	2. Curry
	3. Cumin
	4. [[Garam Masala]]
2. Add and cook for 2 minutes
	1. Onion
	2. Garlic
3. Add and bring to a boil
	1. Stock
	2. Lentils
	3. Rice
4. Add and cook for 20 minutes
	1. Potato
	2. Apple
	3. Celery
	4. Carrot
	5. Salt
5. Add and serve
	1. Coconut milk
	2. Tamarind
	3. Tamari

Variation
1. Replace Apple with Mushrooms
2. Garnish with Balsamic Vinegar