---
tags:
  - untried
---
From: *Spice: flavors of the eastern Mediterranean* page 110
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Chickpeas |
| 1 small | Baking Potato |
| 2 Tablespoon | Oil |
| 1 | Onion (Large) |
| 8 oz | Spinach |
| 2 Cloves | Garlic |
| 3 Tablespoon | Pine nuts (toasted) |
| 2 | apricots (dried) |
| 1 Tablespoon | Currants |
| 1 teaspoon | Pumpkin Pie Spice |
| 1 Tablespoon | Tahini |
| 3 | Tomatoes (plum) |


1. Puree 
	1. Chickpeas
	2. Potato (mashed)
	3. 
1. Sauté for 8 minutes
	1. Oil
	2. Onion
2. Add and cook for 5 minutes
	1. Spinach
3. Add
	1. Garlic
	2. Pine nuts
	3. Apriocts
	4. Currants
	5. Tahini
4. Wrap Spinach in Puree
5. Top
	1. Cut Tomatoe
	2. Oil