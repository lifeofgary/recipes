---
tags:
  - untried
---
From: *Cooking with Spice* page 15
Rating: ?/5

## Indian Style
- Cumin
- Coriander
- Turmeric
## Asian Style Leafy Greens
- Sesame seeds
- Chile Flakes
- Soy Sauce
## Moroccan Style Vegetables
- Cinnamon
- Ginger
- Turmeric
- Lemon Juice
## Caribbean Style
- Allspice
- Cinnamon
- Cayenne
- Lime Juice
## Latin Style
- Chili Powder
- Cumin
- Lime Juice
## BBQ Style
- Mustard
- Garlic Powder
- Paprika
- Lemon Juice
