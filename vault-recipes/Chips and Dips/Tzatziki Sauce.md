---
tags:
  - untried
  - sauce
---
From: *Keto Reset Cookbook* page 58
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| amount | ingredient |
| 1/2 | English Cucumber (pealed, grated)
| 1 Cup | Yogurt |
| 2 teaspoon | Lemon Juice |
| 2 clove | garlic (minced) |

1. Place grated cucumber in sieve, salt and let sit for 10 min
2. With back of large spoon, press out as much water as possible out of cucumber
3. Stir and repeat until no water comes out
4. Mix everything together in a bowl

`Keto reset cookbook pg 58`