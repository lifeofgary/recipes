---
tags:
  - untried
---
From: Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 Cup | Almond Milk |
| 1/2 Cup | Dates (tightly packed) |
| 1/2 Cup | Banana (ripe, mashed) |
| 3 Tablespoons | Caoca Powder |
| 1/8 teaspoon | cinnamon |
| 1/8 teaspoon | cardamom |
| pinch | Salt |
| 1 Tablespoon |  Maple Syrup or Agave |
| 1/2 teaspoon | Vanilla |
| 2 Tablespoon |  Mint (fresh, chiffonade) |
| 1 pint | Raspberries or Strawberries |
| 1/2 Cup | Cacao nibs (for Garnish) |


1. Blend
	1. Almond Milk
	2. Dates
	3. Banana
	4. Cacao Powder
	5. Cinnamon
	6. Cardamom
	7. Salt
	8. Maple Syrup
	9. Vanilla
 2. Mix in
	 1. Mint
	 2. Raberries
 3. Garnish with
	 1. Cacao Nibs

Variations
1. 1/2 teaspoon of mint extract instead of/in addition to, the vanilla
2. Replace Vanilla with ground Vanilla bean