---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (olive) |
| 1 | Onion |
| 2 Clove | Garlic |
| 1 Sprig | Rosemary |
| 1 Cup | Lentils |
| 1 Can (14 oz) | Tomato (chopped) |
| 5 Cup | Stock |
| 2 | Lemon (juiced) |
| Pinch | Salt |

1. Sauté for 10 minutes
	1. Oil
	2. Onion
2. Sauté for 2 minutes
	1. Garlic
	2. Rosemary
3. Add 
	1. Lentils
	2. Tomato
4. Simmer for 30 minutes