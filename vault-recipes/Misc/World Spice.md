[1509 Western Avenue](https://maps.google.com/maps?q=World+Spice+Merchants&near=Seattle,+WA)
Seattle, WA 98101
## Interesting Items
[Asafoetida](https://worldspice.com/collections/pure-spices/products/asafoetida) make sure it doesn't contain gluten as thinner
[Nigella Seed](https://worldspice.com/collections/pure-spices/products/nigella-seed)
[Sumac](https://worldspice.com/collections/pure-spices/products/sumac)
[Szechuan Pepper](https://worldspice.com/collections/pure-spices/products/szechuan-pepper)

Ajwain
