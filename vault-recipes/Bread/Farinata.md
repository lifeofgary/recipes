---
tags:
  - untried
---
From: *Unknown library book* page
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Chickpea Flour |
| 1 teaspoon | Salt |
| 1/2 teaspoon | Water (lukewarm) |
| 5 Tablespoon | Coconut Oil |

1. Mix 
	1. Chickpea Flour
2. Cover for 1-12 hours
3. Coat Pan and Back for 10 -15 minutes
	1. 3 Tablespoon Coconut Oil
4. Coat Bread with
	1. 2 Tablespoon Oil
5. Broil for 30-60 seconds (6-8 from heater)