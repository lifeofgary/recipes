---
tags:
  - untried
---
From: *The Primal Kitchen* page 129
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| amount | ingredient |
| 2 lb. | wings |
| 1 cup | Honey Mustard Vinaigrette |
| 1/2 | lemon juiced | 
| 1/2 | lemon zested |

1. Put everything in bag and mariante for at least 30 minutes
2. Cook at 400 for 40 min