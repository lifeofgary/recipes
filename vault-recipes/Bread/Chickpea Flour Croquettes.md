---
tags:
  - stars/3
---
From: *600 Curries*
Rating: ?/5

## Croquettes

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Chickpea Flour |
| 1/2 Cup | Rice Flour |
| 1 Cup | Onion (Red) |
| 1/4 Cup | Cilantro |
| 1 1/2 teaspoon | Salt |
| 1 teaspoon | Thyme (dried) |
| 1 recipe | [[Croquette Sauce]]
1. Combine
	1. Chickpea Flour
	2. Rice Flour
	3. Onion
	4. Cilantro
	5. Thyme
	6. Salt
2. Form circles 1 1/2 in diameter and 1/2 inch thick
3. Pour Oil to 2 - 3 inches in a Dutch Oven and heat to 350
4. Fry for about 5 minutes

Note: Good if air fried and add some milk
