---
tags:
  - untried
---
From: *Rice Diet* page 260
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 lb | Chicken |
| 1/4 teaspoon | Mustard (dry) |
| 1 Cup | Yogurt |
| 1/4 Cup | Lemon Juice |
| 3 Cloves | Garlic |
| 1/2 teaspoon | Cardamom |
| 1/4 teaspoon | Ginger |
| 1/4 teaspoon | Cumin |
| 1/4 teaspoon | Red Pepper Flakes |
| 1/2 teaspoon | Water |

1. Combine and create a paste
	1. Water
	2. Mustard
2. Add
	1. Yogurt
	2. Lemon Juice
	3. Garlic
	4. Cardamom
	5. Ginger
	6. Cumin
	7. Red Pepper
3. Coat Chicken with mix
4. Cover and refrigerate overnight
5. Roast for 1.5 hours at 375
6. Baste Chicken with marinade 
7. Place foil over chick parts that boast faster