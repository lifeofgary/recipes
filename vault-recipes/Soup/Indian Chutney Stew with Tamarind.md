---
tags:
  - untried
---
From: *The 30-Minute Vegan: Soups on!* page 52 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 5 Cup | Stock |
| 1 1/2 Cup | Tomato (chopped) |
| 1 Tablespoon | Tomato Paste |
| 1-3 teaspoon | Tamarind Paste |
| 1 Tablespoon | Curry Powder |
| 1 teaspoon | [[Garam Masala]] |
| 1/2 teaspoon | Coriander |
| 1/2 teaspoon | Cumin |
| 1 Cup | Onion (yellow, diced) |
| 3 Clove | Garlic |
| 1 Tablespoon | Ginger (fresh) |
| 3/4 Cup | Mushrooms |
| 1 1/2 Cup | Zucchini (Chopped) |
| 1 1/2 | Squash (yellow summer, chopped) |
| 1 Cup | Carrot (sliced) |
| 1/4 Cup | Raisons or Currants |
| 2 teaspoon | Salt |
| 1 1/2 Tablespoon | Maple Syrup |
| 1 1/2 Tablespoon | Tamari |
| 2 or 3 Tablespoon | Cilantro (fresh, chopped) |

1. Cook for 20 minutes
	1. Everything but Cilantro
2. Top with Cilantro