---
tags:
  - untried
---
From: *Unknown Library Book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Peanut Butter |
| 1/2 Cup | Water (boiling) |
| 3 Tablespoon | oil (sesame) |
| 3 Tablespoon | [[Date Syrup]] or Honey |
| 2 Tablespoon | Tamari |
| 2 Tablespoon | Vinegar (rice) |
| 3 clove | Garlic |
| 1 Tablespoon | Ginger (fresh) |
| 1/8 teaspoon | Red Pepper Flakes |

1. Mix
	1. Peanut Butter
	2. Water
2. Add
	1. Oil
	2. Date Syrup
	3. Tamari
	4. Vinegar
	5. Garlic
	6. Ginger
	7. Pepper Flakes