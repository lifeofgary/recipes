---
tags:
  - untried
---
From:  *Dairy-Free and Gluten Free Kitchen*  page 114
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Onion (red) |
| 2 Tablespoon | Stock |
| 2 Tablespoon | Vinegar (wine or balsamic) |
| 1/4 teaspoon | Salt |
| 1 Clove | Garlic |
| 1 lb. | Greens (kale, spinach) |
| 1/2 Cup | Water (divided) |

1. Sauté for 5 minutes
	1. Onion
	2. Salt
	3. Stock (1 Tablespoon)
2. Add Sauté for 10 minutes
	1. Garlic
	2. Stock (1 Tablespoon)
3. Add 
	1. Vinegar
	2. Greens
	3. Water (1/4 Cup)
4. Cook over medium heat for 3 minutes
5. Add
	1. Water (1/4 Cup)
6. Cook over medium heat for 3 minutes