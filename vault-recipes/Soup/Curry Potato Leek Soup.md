---
tags:
  - untried
  - people/nanci
---
From: [Randa Nutrition](https://randaderkson.com/red-curry-potato-leek-soup/#wprm-recipe-container-9490)
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoons | Oil (Avocado) |
| 4 | Potatoes |
| 2 | Leeks |
| 4 cloves | Garlic (Minced) | 
| 3 Tablespoon | Curry Paste (Red) |
| 14 oz can | Coconut Milk |
| 4 cups  | Broth |
| 1/2 Tablespoon | Thyme |
| 1/4 Teaspoon | Salt |

1. Sauté for 2 minutes
	1. Oil
	2. Leeks
2. Add and cook for 1 minute 
	1. Potatoes
	2. Curry Paste
	3. Garlic
3. Add and simmer for 20 minutes
4. Blend and serve
