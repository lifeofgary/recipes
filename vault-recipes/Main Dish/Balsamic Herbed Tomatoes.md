---
tags:
  - untried
  - air-frier
---
From: *The Vegan Air Fryer* page 68 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Vinegar (Balsamic) |
| 1/2 teaspoon | Salt |
| 1 Tablespoon | Oregano |
| 2 | Tomato (quartered)
| | Oil Spray |

1. Mix 
	1. Vinegar
	2. Salt
	3. Oregano
2. Dip each `Tomato` slice in Vinegar mix
3. Place in airfryer
4. Spray `Tomato` with oil
5. Sprinkle `Tomato` with rest of Vinegar mix
6. Airfry at 360F for 5-6 minutes