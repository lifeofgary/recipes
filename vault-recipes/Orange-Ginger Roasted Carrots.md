---
tags:
  - bowl
  - untried
---

From *Paleo Power Bowls* pg 305

| Amount     | Ingredient              |
| ---------- | ----------------------- |
| 1-2 pounds | Carrot                  |
| 2 Tbsp     | Oil (olive)             |
| 1 pinch    | Salt                    |
| 1 batch    | [[Orange-Ginger Sauce]] |
1. Roast at 425 for 20 minutes
	1. Carrots
	2. Oil
	3. Salt
2. Turn and Roast for 20 minutes
3. Add Sauce