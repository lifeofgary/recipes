---
tags:
  - untried
---
From: *Unknown library book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lb. | Ground Beef |
| 2 Tablespoon | Tamari |
| 1 teaspoon | Salt (smoked) |
| 1/2 teaspoon | Garlic Powder |
| 1/2 teaspoon | Red Pepper Flakes |

1. Mix
	1. Beef
	2. Tamari
	3. Salt
	4. Garlic Powder
	5. Red Pepper
2. Make cookie sized balls
3. Bake at 170 for 6 hours