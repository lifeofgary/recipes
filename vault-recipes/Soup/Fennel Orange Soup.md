---
tags:
  - untried
---
From: 30 minute vegan: Soup's on
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| 1 | Leek |
| 1 Bulb | Fennel |
| 1 | Potato | 
| | Citrus Zest |

1. Sauté for 25 minutes
	1. Oil
	2. Leek
	3. Fennel
	4. Potatoes
2. Add
	1. Broth
	2. Zest
