---
tags:
  - untried
---
From: *30-minute Vegan: Soup's On*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 1/2 Cup | Stock |
| 1 can | Coconut Milk |
| 1/2 Cup | Rice (basmati, uncooked) |
| 1 1/2 Cup | Onion (yellow, diced) |
| 1/2 Cup | Celery |
| 4 Cloves | Garlic |
| 1/2 teaspoon | Thyme (dried) |
| 1 Cup | Carrot (diced) |
| 1 Can or 1 1/2 Cup Cooked | Red Kidney Beans |
| 1 Cup | Peas or Potatoes |
| 2 Tablespoon | Parsley (fresh) |
| 2 teaspoon | Salt |
| 2 teaspoon | Tamari |
| 1/4 Cup + 2 Tablespoon | Toasted Coconut (garnish) |
| 6 leaves | Parsley (garnish) |

1. Cook for 10 minutes
	1. Stock
	2. Coconut Milk
	3. Onion
	4. Celery
	5. Garlic
	6. Thyme
	7. Potato (if used)
2. Add and cook for 5 minutes
	1. Carrot
	2. Red Beans
	3. Peas (if used)
	4. Salt
	5. Tamari
3. Garnish with
	1. Toasted Coconut
	2. Parsley