---
tags:
  - stars/4
  - hummas
---
From: *Yogurt Bible*
Rating: 4/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can | Chickpea |
| 1-2 Clover | Garlic |
| 1/4 - 2 1/2 Cup | Yogurt (drained) |
| 2-3 Tablespoon | Tahini or Peanut Butter |
| 3 Tablespoon | Lemon Juice |
| 1/2 teaspoon | Salt |
| 1/2 teaspoon | Cumin |
| 1/4 Cup | Parsley Leaves |

Blend everything