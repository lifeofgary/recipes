---
tags:
  - untried
  - sauce
---
From: *Unknown Library Book* page 99
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Sun Dried Tomatoes |
| 1 can or 15 oz | Beans (black) |
| 1/2 Cup | Water |
| 1/2 Cup | Cilantro (fresh) |
| 1/4 Cup | Lime Juice |
| 2 Tablespoon | Nutritional Yeast |
| 1 teaspoon |  Zest (lime) |
| 1 teaspoon | Chili Powder |
| 1/2 teaspoon | Cumin |

1. Blend Tomatoes for 2 minutes
2. Add and blend for 2 minutes
	1. Black Beans
	2. Water
	3. Cilantro
	4. Lime Juice
	5. Nutritional Yeast
	6. Zest
	7. Chili powder
	8. Cumin

4. Sauté for 10 minutes