---
tags:
  - untried
  - instapot
  - hummas
---
From: *Instantly Mediterranean* page 40 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/4 Cup | Chickpeas (dried) |
| 2 Teaspoon | Salt |
| 4 Cloves | Garlic |
| 1/2 Cup | Tahini |
| 2 | Lemon Juiced |
| 3 Tablespoon | Iced Water |
| | Parsley (garnish) | 

1. Pressure cook for 50 minutes, then manual release for 15 minutes
	1. Chickpeas
	2. Salt
	3. 4 Cups Cold Water
2. Reserve 1 Cup Water
3. Blend
	1. 1 teaspoon Garlic
	2. Tahini
	3. Lemon Juice
4. Add 3 Cup of Chickpeas to Blender.  If it is thick add reserve water 1 Tablespoon at a time
5. Add rest of ingredients