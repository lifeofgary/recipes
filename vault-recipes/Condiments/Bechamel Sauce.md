---
tags:
  - untried
---
From: *Unknown Library Book* page # 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 1/2 Tablesppon | Starch |
| 1 Tablespoon | Nutritional Yeast |
| 1 teaspoon | Onion Powder |
| 1 teaspoon | Garlic Powder |
| 1/2 teaspoon | Salt (sea) |
| 1/4 teaspoon | Nutmeg |
| 2 Cup | Milk (nondairy) |

1. Cook while stirring
	1. Starch
	2. Nutritional Yeast
	3. Onion Powder
	4. Garlic Powder
	5. Salt
	6. Nutmeg

Variation
1. Replace 1 Cup Milk with 1 Cup Broth
1. Replace 1 Cup Milk with 3/4 Cup Broth and 1/4 white whine
 