---
tags:
  - untried
  - instapot
---
From: *The Essential Vegan Instant Pot Cookbook* page 128 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Rice (long-grain jasmine or basmati) |
| 1 1/2 Cup | Rice (long-grain jasmine or basmati) |
| 1 Can (13 oz) | Coconut Milk |
| 1 Can (5 1/4 oz) | Coconut Cream |
| 1/2 Cup | Agave Nectar |
| 1/2 teaspoon | Cardamom |
| 1/4 teaspoon | Salt |
| 1/4 Cup | Currants |
| 1/4 Cup | Pistachios (chopped) |

1. In instapot cook
	1. Rice 
	2. Water
2. Blend
	1. Coconut Milk
	2. Coconut Cream
	3. Agave
	4. Cardamom
	5. Salt
3. Sauté until thickened 
	1. Milk blend
	2. Rice
4. Mix in
	1. Currants
5. Garnish 
	1. Pistachios