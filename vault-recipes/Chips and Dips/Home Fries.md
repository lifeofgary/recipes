---
tags:
  - untried
---
From: *Unknown Library Book* page 53 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Potato |
| 2 Cup | Rutabaga |
| 2 Cup | Sweet Potatoes |
| 1 1/2 Tablespoon | Broth |
| 1 1/2 Tablespoon | Oil (olive) |
|  3/4 teaspoon | Basil |
| 3/4 teaspoon | Thyme |
| 3/4 teaspoon | Garlic Powder |
| 1/2 teaspoon | Paprika (smoked) |
| 1/4 teaspoon | Salt |
| 2/3 Cup | Onion (red) |
| 1 1/3 Cup | Bell Pepper (red and green) |

1. Combine
	1. Potato
	2. Rutabaga
	3. Sweet Potatoes
	4. Broth
	5. Oil
	6. Basil
	7. Thyme
	8. Garlic Powder
	9. Paprika
	10. Salt
2. Bake at 425F for 20 minutes
3. Top with
	1. Onion
	2. Peppers
4. Bake at 425F for 20 minutes 