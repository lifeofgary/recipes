---
tags:
  - untried
---
From: `Spice: flavors of the eastern mediterranean`
## Cumin, Coriander and Cardamom
Page 3
### Cumin
Goes well with
- Lamb
- Beef
- Chickpeas
- Lentils
- Eggplant
- Cooked Tomatoes
- Mint
- Paprika
- Coriander
- Saffron
- Garlic
- Fennel
### Coriander 
Goes well with
- Citrus
- Zest
- Sugar
- Mushrooms
- Nuts
- Cumin
- Fennel
- Saffron
- Cinnamon
### Cardamom
Goes well with
- Gingerbread
- Dates
- Squash

## Saffron, Ginger and Vanilla
Page 39

### Saffron
`Coupe` saffron is pure stigma (good)
`Mancha` saffron has pieces of the flower, which makes it 20% cheaper

For North African flavors:
- Paprika
- Cumin
- Coriander
- Cinnamon
- Ginger
For Spanish flavor:
- Garlic
- Paprika
- Citrus
### Ginger
For North African flavors:
- Saffron
- Vanilla
- Coriander
- Turmeric
### Vanilla
- Candies
- Ice Cream
- Shell Fish
- Saffron
- Cilantro
- Nuts
## Sumac, Citrus and Fennel
Page 71

### Cumin
- Fish
- Chicken
- Avocado
- In [[Fish Spice]]
### Citrus
Enhance flavor, but should not taste it
- Garlic
- Cinnamon
- Paprika
- Olives
- Cilantro
## Fennel
- Broth
- Soup
- Fish
- Paprika
- Cumin
- Saffron
- Cinnamon
- Tomato sauce
- In pickles
## Allspice, Cinnamon and Nutmeg
Page 103
### Allspice
- Meat
- Rice
- Vegetables
- Broth
### Cinnamon
- Broth
- Chocolate
- Garlic
- Lamb
- Cumin
- Allspice
### Nutmeg
- Eggnog
- Bechamel
- White Sauce
- Tomato Sauce
- Spinach
- Cakes
- Pies
## Curry Powder, Turmeric and Fenugreek
Page 197
### Curry Powder
- Tuna
- Tomato based soup
- Mayonnaise
### Turmeric
- Mustard
- Pickles
- Relish
- Chutney
- Rice
### Fenugreek
- Mashed potatoes
- White Sauce

## Mint, Oregano and Zaatar
Page 229
### Mint
- Rice (with Oregano)
- Meat
### Oregano
- Mariandes
- Tomato based sauces
- Stews

### Zaatar
[[Zaatar]]

## Parsley, Mint, Basil
Page 250
### Parsley
Used for green color
Good with Oil oile
### Dill
- White sauce
- Chicken
- Fish
- Veggies
### Basil
- Pesto

### Oregano, Savory, Sage, Rosemary, Thyme
Page 275
### Oregano
- Basil
- Tomatoes
- Eggplant
- Zucchini
- Beef
- Lamb
- Garlic
### Savory
- Tomato Soup
- Beans
- Lentils
- Peas
### Sage
- Garlic
- Mushrooms
- Squash
- Eggplant
- Soup
- Onions
- Potatoes
- Beasn
- Peas
### Rosemary
- Meats
- Garlic
- Potatoes
### Thyme
- Chicken
- Baouquet Garni
- Soup
- Potatoes
- Beans