---
tags:
  - untried
---
From: 30 minute vegan: Soup's on
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| 2 Stocks | Celery |
| 1 1/2 lb | Jerusalem Artichoke |
| 32 oz | Broth |

1. Sauté for 10 minutes (until tender)
	1. Oil
	2. Celery
	3. Jerusalem Artichoke
2. Add Stock