---
tags:
  - bowl
  - "#people/nanci"
  - untried
---

From *Paleo Power Bowls* pg 297

| Amount  | Ingredient        |
| ------- | ----------------- |
| 1       | Cucumber          |
| 2 Cup   | Tomatoes (Cherry) |
| 1 Clove | Garlic            |
| 2 Tbsp  | Mint (fresh)      |
| 3 Tbsp  | Oil (avocado)     |
| 3 Tbsp  | Vinegar           |
| 1 tsp   | Oregano           |
| 1 tsp   | Parsley           |
| 1/4 tsp | Salt              |
1. Add
	1. Cucumber
	2. Tomatoes
	3. Garlic
	4. Mint
2. Whisk
	1. Oil
	2. Vinegar
	3. Oregano
	4. Parsley
	5. Salt
3. Pour dress on Cucumber