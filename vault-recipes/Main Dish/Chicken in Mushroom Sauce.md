---
tags:
  - untried
---
From: *The Dairy-Free and Gluten-Free Kitchen* page 84
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 lb. | Chicken (boneless) |
| 1/2 teaspoon | Salt |
| 1/4 Cup | Flour (gluten free) |
| 2 Tablespoon | Oil (olive) |
| 1 | Onion |
| 2 Clove | Garlic |
| 1 lb. | Mushrooms |
| 1/2 Cup | Wine (Marsala) |
| 1/4 Cup | Wine (sherry) |
| 1 Cup | Stock |
| 6 sprig | Thyme |
| 1/4 Cup | Parsley (flat leafed) |



1. Salt `Chicken`
2. Flour `Chicken`
3. Sauté for 5 minutes on each side
	1. Oil
	2. Chicken
4. Remove chicken to a plate
5. Sauté for 3 minutes
	1. Onion
	2. Stock (2 Tablespoon)
6. Add and Sauté for 1 minutes
	1. Garlic
7. Add and cook for 8 minutes
	1. Mushrooms
8. Add
	1. Wine
	2. Sherry
	3. Stock
9. Boil for 3 minutes
10. Add and simmer for 30 minutes
	1. Chicken