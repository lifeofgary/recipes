---
tags:
  - untried
---
From: *Unknown library book* page 78 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| 1 | Onion |
| 2 Cloves | Garlic |
| 2 inch | Ginger |
| 2 Stalks | Lemongrass |
| 1 Cup | Lentils |
| 1 lbs. | Pumpkin |
| 5 Cup | Stock |
| 1 Can (14 oz) | Coconut Milk |
| 1 Tablespoon | Tamarind |
| 2 Tablespoon | Cilantro |
| Dollop | Fish Sauce |
| Dollop | Tamari |
| 2 | Limes (juiced) |
| Pinch | Brown Sugar |

1. Sauté for 10 minutes
	1. Onion
2. Add Sauté for 2 minutes
	1. Garlic
	2. Ginger
	3. Lemongrass
3. Add
	1. Lentils
	2. Pumpkin
	3. Stock
4. Simmer until `Lentils` are soft
5. Stir in
	1. Coconut Milk
	2. Tamarind
	3. Cilantro
	4. Fish Sauce
	5. Tamari