---
tags:
  - untried
  - instapot
---
From: *The Ultimate Instant Pot Healthy Cookbook* Page 208
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 lb. | Asparagus |
| 1 | [[Vegan Aioli]] |
| 1 Cup | Water |

1. In instapot
	1. Wire Mesh
	2. Water
	3. Asparagus
2. Select `Steam` and set
	1. Time: 0
	2. Low Pressure
3. Steam Asparagus
4. Serve with [[Vegan Aioli]]