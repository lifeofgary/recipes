---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 43
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Caraway |
| 2 teaspoon | Coriander |
| 1 teaspoon | Cumin |
| 2 teaspoon | Garlic |
| 2 teaspoon | Paprika (smoked) |
| 1 1/2 teaspoon | Paprika (sweet) |
| 1/2 teaspoon | Cinnamon |
| 1 teaspoon | Cayemme |

North Africa all purpose.  Good on meat or nuts.