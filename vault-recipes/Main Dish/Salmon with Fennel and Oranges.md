---
tags:
  - untried
---
From: *RedBook*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Bulb | Fennel |
| 1/2 | Onion (wedged) |
| 1 | Orange |
| 3 Tablespoon | Oil (olive) |
| 4 | Salmon Fillets (6 oz) | 

1. Cook at 350F for 15 minutes
	1. Fennel
	2. Onion
	3. Orange
	4. Oil 
	5. Salmon
2. Remove
	1. Salmon
3. Broil for 2 minutes