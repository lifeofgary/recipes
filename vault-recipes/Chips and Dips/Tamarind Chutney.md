---
tags:
  - untried
  - to-try
  - sauce
---
From: *Vegan Cooking in your air Fryer* page 46 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Water |
| 1 Cup | Sugar |
| 2 Tablespoons | Tamarind (Concentrate) |

1. Mix 
	1. Water
	2. Sugar
	3. Tamarind
2. Bring to a boil
3. Simmer and reduce until thick and 1 Cup