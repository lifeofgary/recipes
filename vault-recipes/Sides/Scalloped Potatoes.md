---
tags:
  - untried
---
From: *Dairy-Free and Gluten Free Kitchen* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil (olive) |
| 2 | Onions (yellow) |
| 1 | Bay Leaf |
| 1/2 teaspoon | Thyme |
| 3 Clove | Garlic |
| 1 Tablespoon | Flour (gluten free) |
| 2 lbs. | Potato |
| 1 Cup | Milk (non-dairy) |
| 1 Cup | Yogurt |
| 1 teaspoon | Salt |
| 1/2 teaspoon | Nutmeg |
| 1/4 teaspoon | Paprika |

1. Sauté for 10 minutes
	1. Oil
	2. Onion
	3. Bay Leaf
	4. Thyme
2. Add and Sauté for 1 minute
	1. Garlic
3. Remove from heat
4. Discard 
	1. Bay Leaf
5. Add
	1. Flour
6. Set aside `Onion`
7. Slice Potato
8. Layer in Pan
	1. Potato
	2. Onion
	3. Potato
	4. Onion
	5. Potato
9. Set aside `Potato`
10. Mix
	1. Milk
	2. Yogurt
	3. Salt
	4. Nutmeg
11. Pour over `Potato`
12. Sprinkle
	1. Paprika
13. Bake at 400F for 30 minutes
14. Bake at 350F for 40 minutes 