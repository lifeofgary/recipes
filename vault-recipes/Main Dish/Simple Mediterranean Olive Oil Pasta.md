---
tags:
  - people/nanci
  - stars/4
---
From: [The Mediterranean Dish](https://www.themediterraneandish.com/mediterranean-olive-oil-pasta/#tasty-recipes-10891-jump-target)
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lb | Spaghetti |
| ½ cup | Oil (Olive)|
| 4 clove  | Garlic (crushed) |
| 1 cup | Parsley (fresh, chopped) |
| 12 oz |  Grape Tomatoes (halved) |
| 3 | Scallions (top trimmed, both whites and greens chopped) |
| 6 oz | Artichoke Hearts (Marinated) |
| ¼ cup | Olives (halved) |
| ¼ cup | Feta |
| 10-15 | Basil (Leaves) |
| 1 | lemon (Zest)|

1. Cook Pasta
2. Add and cook for 10 seconds
	1. Oil
	2. Garlic
	3. Pinch of Salt
3. Add  and cook for 30 seconds
	1. Parsley
	2. Tomatoes
	3. Scallion
4. Add remaining ingredients