---
tags:
  - bowl
---

From *Paleo Power Bowls* pg 26

1. Starchy Veg (1/2-1 Cup) Steam/Roast/Sautee
	- Potatoes
	- Carrot
	- Yucca
2. Low Carb Veg (1-2  Cup) Steam/Grill/Sautee/Roast
	- Zucchini
	- Onions
	- Radishes
	- Asparagus
	- Bell Pepper
3. Protein (6-8oz or palm sized)
4. Leafy Green (1-3 Cup) Fresh/Sautee
	- Spinach
	- Kale
	- Chard
	- Collard Greens
5. Rice (1/2-1 Cup)
6. Sauce or Dressing (2-4 Tbs)
7. Crunches (1-3 Tbs)
	- Nuts
	- Seeds
8
. Herbs (1 tsp -2 Tbs)
---

From *Great Bowls of Food*

1. Choose a Base (1 Cup)
- Brown Rice
- Oats
- Quinoa
2. Add Produce (1 Cup)
- Artichoke
- Asparagus
- Broccoli
- Carrots
- Eggplant
- Kale
- Mushrooms
- Spinach
- Tomatoes
3. Pick a Protein (1 Cup)
- Beef
- Black Beans
- Cannellini Beans
- Chicken
- Edamame
- Fava Bean 
- Kidney Bean
- Lentils
- Peas
- Salmon
- Tuna
- Turkey
4. Drizzle on Dressing (2 Tablespoons)
5. Add Topping (Optional)
- Almonds
- Avocado
- Hemp Seeds
- Peanuts
- Pomegranate Seeds
- Pumpkin Seeds
- Sesame Seeds
- Sunflower Seeds
- Walnuts

