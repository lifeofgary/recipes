---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 164 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 Cup | Rice |
| 4 Cup | Water |
| 1/3 teaspoon | Turmeric |
| 2 Cup | Onion (sweet) |
| 1/3 Can | Pineapple (chunks) |
| 4 Tablespoon | Oil |
| 1/3 Cup | Walnuts (crushed) |
| 2/3 Cup | Cucumber (chopped) |
| 1/3 Cup | Cilantro (chopped) |

1. Cook for 5 minutes in Instapot
	1. Water
	2. Rice
2. Mix in
	1. Turmeric
	2. Onion
	3. Pinaplle
	4. Oil
	5. Walnuts
	6. Cucumber
	7. Cilantro