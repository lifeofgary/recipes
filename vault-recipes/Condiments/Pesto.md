---
tags:
  - untried
---
From: *Clean Eating Bowls* page 144 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Pine Nuts |
| 3 Cup | Basil (fresh) |
| 3 Cloves | Garlic |
| 1 teaspoon | Salt |
| 1/2 Cup | Oil (olive)
| 1/3 Cup | [[Vegan Parmesan]]

1. Roast at 375F at 8 minutes on parchment paper
2. Blend
	1. Pine Nuts
	2. Basil
	3. Garlic
	4. Salt
3. Slowly add
	1. Oil
4. Add [[Vegan Parmesan]]