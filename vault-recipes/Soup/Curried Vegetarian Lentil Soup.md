---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (olive) |
| 1 | Onion |
| 2 | Carrot |
| 3 Cloves | Garlic |
| 1 teaspoon | Curry Powder |
| 1 Can (14.5 oz) | Tomato (diced) |
| 1 | Bay Leaf |
| 1 teaspoon | Thyme |
| 1 Cup | Lentils |
| 1/2 Cup | Wine (white) |
| 1/4 teaspoon | Salt |
| 4 1/2 Cup | Broth |
| 1 1/2 Cup | Water |

1. Sauté for 2 minutes
	1. Onion
	2. Carrot
2. Stir in and Sauté for 30 seconds
	1. Garlic
	2. Curry Powder
3. Stir in and Sauté for 30 seconds
	1. Bay Leaf
	2. Thyme
	3. Tomatoes
4. Reduce heat to medium-low
5. Stir in an cook for 10 minutes
	1. Lentils
	2. Salt
6. Stir in
	1. Wine
	2. Broth
	3. Water
7. Boil
8. Reduce heat
9. Cook for 30 minutes