---
tags:
  - untried
---
From: *Unknow library book* page 77
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| 1 | Onion |
| 2 Cloves | Garlic |
| 2 in | Ginger |
| 1 teaspoon | Curry Powder |
| 1 Cup | Lentils (red) |
| 1 Can (14 oz) | Tomatoes (chopped) |
| 5 Cups | Stock |
| 1 Tablespoon | Tamarind Paste |
| 2 Tablespoon | Cilantro (fresh) |
|  | Salt |
| 1-2 | Lime (juiced) |
| Pinch | Brown Sugar |
| 4 Tablespoon | Yogurt |

1. Sauté for 10 minutes
	1. Onion
2. Add and Sauté for 2 minutes
	1. Garlic
	2. Ginger
	3. Curry Powder
3. Simmer until soft
	1. Lentils
	2. Tomatoes
	3. Stock
	4. Tamarind
4. Add
	1. Cilantro
	2. Salt
	3. Lime
	4. Sugar
	5. Yogurt