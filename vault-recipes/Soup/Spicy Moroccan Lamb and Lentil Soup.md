---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lb. | Lamb |
| 1 Tablespoon | Oil (olive) |
| 1 | Onion |
| 1 teaspoon | Ginger |
| 1 teaspoon | Cumin |
| 1/2 teaspoon | Paprika |
| 1/4 teaspoon | Cinnamon
| 1/4 teaspoon | Cayenne |
| Pinch | Saffron |
| 1 Tablespoon | Flour (gluten free) |
| 10 Cup | Broth |
| 1 Cup | Lentils |
| 4 | Tomatoes (plum) |
| 1 Can (15 oz) | Chickpeas |
| 1/3 Cup | Cilantro |
| 1/4 Cup | [[Spice/Harissa]] |

1. Brown Lamb
2. Sauté for 7 minutes
	1. Onion
3. Stir in
	1. Ginger
	2. Cumin
	3. Paprika
	4. Cinnamon
	5. Cayenne
	6. Saffron
4. Sauté for 30 seconds
5. Stir in 
	1. Flour
6. Sauté for 1 minute
7. Add and simmer for 10 minutes
	1. Lamb
	2. Broth
8. Add
	1. Lentils
9. Simmer for 50 minutes
10. Add
	1. Tomatoes
	2. Chickpeas
11. Simmer for 10 minutes
12. Add
	1. Cilantro
	2. [[Spice/Harissa]]