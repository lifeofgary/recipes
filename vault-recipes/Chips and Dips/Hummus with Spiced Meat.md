---
tags:
  - untried
  - hummas
---
From: *Unknown Library Book*  
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil (olive) |
| 1 | Onion (yellow) |
| 2 cloves | Garlic |
| 1 1/2 teaspoon | Salt |
| 1 1/2 teaspoon | Cumin |
| 1 teaspoon | Cinnamon |
| 1 teaspoon | Coriander |
| 1/2 teaspoon | Allspice |
| 1 lbs | Beef (ground) |
| 1 | Lemon (juice) |
| 1 | Recipe of [[Smooth and Creamy Hummas]] |
| 1/2 Cup | Pine Nuts (garnish) |
| | Parsley (garnish) |

1. Sauté for 5 minutes
	1. Oil
	2. Onion
	3. Garlic
2. Add and Sauté for 5 minutes
	1. Salt
	2. Cumin
	3. Cinnamon
	4. Coriander
	5. Allspice
3. Add Meat Sauté for 8 minutes
4. Add 
	1. Lemon Juice
	2. Salt
5. Place Hummus in Bowl
6. Put meat in middle of bowl
7. Garnish with
	1. Pine nuts
	2. Parsley