---
tags:
  - untried
---
From: *Soups from around the world* Page 67
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lb. | Beef (ground) |
| 2 | Onions |
| 12 Cup | Broth (beef) |
| 1/4 Cup | Fish Sauce |
| 4 in | Ginger |
| 1 Stick | Cinnamon |
| 2 Tablespoon | Sugar |
| 6 | Star Anise |
| 6 | Cloves |
| | Salt |
| 1 lb | Steak (strips) |
| 16 oz | Noodles (rice) |
| 1/3 Cup | Cilantro |
| 3 | Scallions |
| | Bean Sprouts |
| | Basil |
| | Lime Wedges |
| | [[Hoisin Sauce]] |


1. Cover in `Water`
	1. Ground Beef
2. Boil for 2 minutes
3. Drain
4. Add
	1. Onions
	2. Broth
	3. Water (2 Cups)
	4. Fish Sauce
	5. Ginger
	6. Cinnamon
	7. Sugar
	8. Star Ansi
	9. Cloves
	10. Salt (2 teaspoon)
5. Boil for 45 minutes
6. Strain Broth
7. Soak for 15 minutes
	1. Rice Noodles
8. Boil Water
9. Cook for 60 seconds
	1. Rice Noodles
10. Cook and Slice
	1. Beef
11. Mix
	1. Beef
	2. Broth
	3. Noodles