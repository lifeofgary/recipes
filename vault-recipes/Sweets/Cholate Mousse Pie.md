---
tags:
  - stars/4
---
From: *Unknown Library Book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 12 oz | Tofu (silken) |
| 1/2 Cup | Coconut Cream (full fat, chilled) |
| 5 oz | Chocolate (Dark 70%) |
| 3 Tablespoon | Maple Syrup |
| 3 Tablespoon | Cacao Powder |
| 2 Tablespoon | Almond Butter |
| 1/2 teaspoon | Vanilla |

1. Blend
	1. Tofu
	2. Coconut Cream
	3. Chocolate
	4. Maple Syrup
	5. Cacao Powder
	6. Almond Butter
	7. Vanilla
2. Chill in freezer for 1 to 2 hours