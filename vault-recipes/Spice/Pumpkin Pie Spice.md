---
tags:
  - untried
---
From: *PlantPure Comfort food* by Kim Campbell page 67 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Cinnamon |
| 2 teaspoon | Dried Ginger |
| 2 teaspoon | Nutmeg |
| 1 1/2 teaspoon | Allspice |
| 1 1/2 teaspoon | Cloves |
| 1/4  teaspoon | Cardamom |teaspoon
