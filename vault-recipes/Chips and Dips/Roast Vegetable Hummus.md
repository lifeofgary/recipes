---
tags:
  - untried
  - hummas
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 can (14oz) | Chickpeas |
| 2 Clove | Garlic |
| 1 | Lemon (juiced) |
| 9 oz | Oil (olive) |
| | Vegetables (see below) |

1. Blend
	1. Chickpeas
	2. Garlic
	3. Lemon
	4. Oil
	5. Vegetables

## Red Pepper Hummus
| Amount | Ingredient |
|----------|-------------|
| 2  | Roasted Red Peppers |
| 1 teaspoon | Paprika |

1. Peal and seed `Red Peppers`

## Roast Pumpkin or Carrot Hummus
| Amount | Ingredient |
|----------|-------------|
| 1 lb.  | Pumpkin or Carrot |
| 2 Tablespoon | Oil (olive) |
| 1 teaspoon | Cumin Seeds

1. Coat `Pumpkin/Carrot` with
	1. Oil
	2. Cumin
2. Roast at 400F for 40 minutes

## Roasted Garlic

Roast `5 clove Garlic` with `Red Peppers` or `Pumpkin/Carrot`
