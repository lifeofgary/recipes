---
tags:
  - untried
---
From: *Unknown Library Book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (olive) |
| 1 | Onion (yellow) |
| 4 Cloves | Garlic |
| 2 teaspoon | Cumin |
| 1 teaspoon | Coriander |
| 1 teaspoon | Salt |
| 1 teaspoon | Paprika |
| 2 can | Fava Bean |
| 1 | Juice of 1 Lemon |
| 1/4 Cup | Parsley |
| 1 | Tomato |
| 3 | Scallions|

1. Sauté for 5 minutes
	1. Onion
2. Add and Sauté for 1 minute
	1. Garlic
	2. Cumin
	3. Coriander
	4. Salt
	5. Paprika
3. Add and Cook for 1 minute and manual release for 5 minutes
	1. Drained Beans
	2. 1 Cup Water
4. Mash Beans
5. Add Lemon Juice
6. Serve and top with
	1. Parsley
	2. Diced Tomato
	3. Scallions