---
tags:
  - untried
---
From: *Paleo Cooking from Elana's Pantry* page 105
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can (13 oz) | Coconut Milk |
| 1/3 Cup | Hemp Seeds |
| 1/3 Cup | Water |
| 1/4 Cup | Honey |
| 1 1/2 teaspoon | Peppermint Extract |
| 1/4 teaspoon | Stevia |
| 1/4 teaspoon | Vanilla |
| 1/4 Cup | Dark Chocolate Chips |


1. Puree until smooth
	1. Coconut Milk
	2. Hemp Seeds
	3. Water
2. Blend in
	1. Honey
	2. Peppermint
	3. Stevia
	4. Vanilla
3. Add Chocolate Chips
4. Put in Ice Cream maker