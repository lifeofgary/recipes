---
tags:
  - untried
---
From: Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Cup | Stock |
| 2 x 15 oz can | Coconut Milk |
| 1 Cup | Onion (Yellow)|
| 1/2 Cup | Celery |
| 4 Clove | Garlic |
| 4 Cup | Potatoes |
| 1 1/2 Cup | Tomato |
| 2 teaspoon | Salt |
| 1 Tablespoon | Tamari |
| 1 Tablespoon | [[Berbere]] |
| 3/4 Cup | Peanut Butter |
| 2 Tablespoon | Cilantro |
| 1/2 Cup | Peanuts (roasted) |

1. Cook for 15 minutes everything but
	1. Peanut Butter
	2. Cilantro
	3. Peanuts
2. Remove 1 Cup of liquid and mix in peanut butter
3. Return mixture 
4. Cook for 5 minutes
5. Add Cilantro
6. Garnish with Peanuts

Variation
1. Sauté Onion, Celery, Garlic in 1 Tables spoon of oil
2. Add 2 Cups of chopped Spinach, Kale or Chard