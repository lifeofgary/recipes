---
tags:
  - untried
---
From: *Nourish: The Paleo Healing Cookbook* page 136
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lb. | Parsnips |
| 1 Tablespoon | oil (coconut or avocado) |
| 1 Tablespoon | Lime Juice |
| 1 teaspoon | Salt |
| 1/2 teaspoon | Garlic Powder |
| 1 recipe | [[Garlic Dipping Sauce]] |

1. Clean and quarter
	1. Parsnips
2. Boil for 10 minutes (until soft)
3. Dry at 425F
4. Mix 
	1. Oil
	2. Lime Juice
	3. Salt
	4. Dry Parsnips
5. Roast for 30 minutes
6. Serve with [[Garlic Dipping Sauce]]