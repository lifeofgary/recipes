---
tags:
  - untried
  - sauce
---
From: *Nourish: The Paleo Healing Cookbook* page 136
Rating: ?/5

For [[Parsnip Wedges]]

| Amount | Ingredient |
|----------|-------------|
| 1 teaspoon | Oil (coconut or avocado) |
| 3 clove | Garlic |
| 1/2 Cup | Mayo |

1. Sauté for 1 minute
	1. Oil
	2. Garlic
2. Add 
	1. Mayo