---
tags:
  - untried
---
From: *Paleo Cooking From Elana's Pantry* page 70
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lb. | Chicken (boneless) |
| 1/3 Cup | Oil (olive) |
| 1/3 Cup | Lemon Juice |
| 1 1/2 teaspoon | Rosemary (fresh) |
| 1 teaspoon | Salt

1. Combine
	1. Oil
	2. Lemon Juice
	3. Rosemary
	4. Salt
2. Marinade for 6 hours
	1. Chicken
3. Cook Chicken