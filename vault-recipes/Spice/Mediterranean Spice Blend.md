---
tags:
  - untried
---
From: *Spice Diet* page 242
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Garlic Powder |
| 1 Tablespoon | Onion Powder |
| 1 Tablespoon | Parsley |
| 2 teaspoon | Oregano |
| 2 teaspoon | Salt |
| 1 teaspoon | Thyme |
| 1 teaspoon | Lemon Zest | 
| 1/2 teaspoon | Cinnamon |
