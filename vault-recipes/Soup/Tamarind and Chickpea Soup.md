---
tags:
  - untried
---
From: Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| 1 | Onion |
| 1 Clove | Garlic |
| 2 teaspoon | Curry (powder) |
| 2 teaspoon | Tamarind (paste) |
| 14 oz | Tomato |
| 7 oz | Broth |
| 580 g | Chickpeas |
| 2 Tablespoon | Lemon Juice |

1. Sauté for 10 minutes
	1. Onion
	2. Garlic
2. Add and cook for 1 minute
	1. Curry
3. Add and cook for 1 minute
	1. tamarind
4. Add and simmer for 20 minutes
	1. Broth
	2. Tomato
5. Add and Simmer for 20 minues
	1. Chickpeas
6. Add Lemon
