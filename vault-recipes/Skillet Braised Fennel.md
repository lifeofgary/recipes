---
tags:
  - untried
---
From: *Mastering Spices* page 50
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 | Fennel Bulbs |
| 2 Tablespoon | Oil (olive) |
| | Salt |
| 1 | Orange |
| 1/4 Cup | Raisins (golden) |
| 1/2 | Lemon Juiced |
| 1/4 Cup | Water |
| 1 1/2 Tablespoon | Vinegar (sherry) |
| 1 teaspoon | Fennel Seed |
| 1/2 teaspoon | Caraway |
| 1/2 teaspoon | Celery Seed |
| 1/2 teaspoon | Urla Pepper |

1. Cut in 8ths
	1. Fennel Bulb
	2. Orange
2. Place into layers over hot heat
	1. Fennel Bulb
	2. Fennel Seed
	3. Caraway
	4. Celery Seed
	5. Urla Pepper
3. Cook over medium heat for 3 minutes
4. Flip and cook for 5 minutes
5. Cook and flip until evenly browned 
6. Add
	1. Oil
	2. Orange
7. Cook for 2 minutes per side
8. Add
	1. Water
	2. Raisens
9. Cook for 15 minutes

## Variations

### 1)
After cooking add
1. 1/2 cup Green Olives
2. 1/3 cup Toasted Amonds
3. Drizzle with Olive Oil
### 2)
1. Replace vinegar with white wine
2. Replace raisins with cranberries 

| Amount | Ingredient |
|----------|-------------|
| 1/2 teaspoon | Caraway |
| 1 Tablespoon  | Basil |
| 2 teaspoon | Oregano |
### 3)
For spices use

| Amount | Ingredient |
|----------|-------------|
| 1 teaspoon | Turmeric |
| 1 teaspoon | Mint |
| 1/2 teaspoon | Lemongrass 
| 1/4 teaspoon Chipotle Chile ||

## 4) Aioli

Cook as above.

| Amount | Ingredient |
|----------|-------------|
| 1 clove | Garlic |
| 1 Tablespoon | Vinegar (apple) |
| 1 teaspoon | Mustard (Dijon) |
| | Oil (olive) |


1. Remove raisens and oranges
2. Blend Fennel
3. Add
	1. Garlic
	2. Vinegar
	3. Mustard
4. Add Olive Oil 1 Tablespoon at t time until it has the consistency of Aioli