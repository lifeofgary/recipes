---
tags:
  - untried
---
From: *Unknown library book*  
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2  lb. | Turkey (ground) |
| 2 Tablespoon | Sage (fresh) |
| 1 Tablespoon | Rosemary (fresh) |
| 1 Tablespoon | Honey |
| 1 1/2 teaspoon | Salt |
| 1 Tablespoon | Oil (olive) |

1. Mix
	1. Turkey
	2. Sage
	3. Rosemary
	4. Honey
	5. Salt
	6. Oil
