---
tags:
  - untried
---
From: *Spice: flavors of the eastern mediterranean* page 233
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Cumin |
| 2 Tablespoon | Oregano |
| 2 Tablespoon | Mint |
| 1 Tablespoon | Chilies (Aleppo) |
| 1 Tablespoon | Pepper |

1. Sauté for 10 minutes