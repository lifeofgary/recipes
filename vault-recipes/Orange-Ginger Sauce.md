---
tags:
  - bowl
  - sauce
  - untried
---


From *Paleo Power Bowls* pg 305

| Amount  | Ingredient       |
| ------- | ---------------- |
| 1 Tbsp  | Sunflower butter |
| 1/2 Cup | Oil (avocado)    |
| 1/4 Cup | Vinegar          |
| 1/4 Cup | Orange Juice     |
| 2 Tbsp  | Coconut Aminos   |
| 1 clove | Garlic           |
| 2 inch  | Ginger           |
| 1 tsp   | Honey            |
1. Mix