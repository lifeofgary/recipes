---
tags:
  - untried
---
From: *Unknown Library Book*  
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3/4 Cup | Milk (non-dairy, ~110F) |
| 1/2 teaspoon | [[Date Syrup]] |
| 1 Package (2 1/4 teaspoon) | Yeast |
| 1 1/2 Cup | Flour (gluten free) |
| 1 teaspoon | Xanthan Gum |
| 1/2 teaspoon | Oregano |
| 1/4 teaspoon | Thyme |
| 1/4 teaspoon | Garlic Powder |
| 1/4 teaspoon | Salt |
| 1 Tablespoon | Oil (olive) |
| 1 teaspoon | Vinegar |

1. Blend
	1. Warm Milk
	2. [[Date Syrup]]
2. Dissolve and let rest for 10 min
	1. Yeast
3. Mix, in a bowl
	1. Flour
	2. Xanthan Gum
	3. Oregano
	4. Thyme
	5. Garlic Powder
	6. Salt
4. Blend into yeast mixture
	1. Oil
	2. Vinegar
5. Add to Flour mixture
	1. Yeast Mixture
6. Spread on greased pan
7. Cover with damp towel
8. Let rest for 20 minutes
9. Bake for 15 minutes at 425