---
tags:
  - untried
---
From: *Spice Diet* page 235
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Nutmeg |
| 1 Tablespoon | Ginger |
| 1 Tablespoon | Orange Zest |
| 1 teaspoon | Lemon Zest |
| 1 teaspoon | Garlic |
| 1 teaspoon | Red Pepper Flakes |
| 1 teaspoon | Parsley |

