---
tags:
  - untried
---
From: *Spice Diet* page 225 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 Tablespoon | Olive Oil |
| 1 Tablespoon | Spice Blend |
| 1 Tablespoon | Garlic (minced) |
| 1/3 Cup | Milk (non-dairy) |
| 2 Cup | Wine (white) |
| 1 Tablespoon | Water |
| 1/2 teaspoon | Lemon Zest |
| 1 Tablespoon | Starch |

1. Sauté for 2 minutes
	1. Oil
	2. Spice Blend ([[Cajun Spice Blend]])
	3. Garlic
2. Whisk in
	1. Milk
	2. Wine
	3. Lemon Zest
3. Make a Slurry with
	1. Water
	2. Starch
4. Whisk Slurry into Sauce to Thicken it

Variations: 
- Use for Spice 
	1. [[Better than Salt]]
	2. [[Savory Soup Spice Blend]]
	3. [[Poultry Spice Blend]]
	4. [[Veggie Spice Blend]]
	5. [[Split Pea Soup]]
	6. [[Italian Spice Blend]]
	7. [[Seafood Spice Blend]]
	8. [[BBQ Spice Blend]]
	9. [[Chesapeake Bay Spice Blend]]
	10. [[Citrus Spice Blend]]
	11. [[Fiesta Spice Blend]]
	12. [[Japanese Spice Blend]]
	13. [[Masala Spice Blend]]
	14. [[Thai Spice Blend]]
	15. [[Spanish Spice Blend]]
	16. [[Moroccan Spice Blend]]
	17. [[Mediterranean Spice Blend]]
	18. [[Falafel Spice Blend]]