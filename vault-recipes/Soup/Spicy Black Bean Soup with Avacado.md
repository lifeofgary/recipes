---
tags:
  - untried
---
From: *Red Book Magazine*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Onion |
| 2 | Tomato (roma) |
| 2 Clove | Garlic |
| 1 | Jalapeno |
| 2 Tablespoon | Oil (olive) |
| 2 teaspoon | Cumin |
| 1 teaspoon | Oregano |
| 2 Can (15 oz) | Black Bean |
| 1 Quart | Broth |
| 1 | Lime (juiced) |
| 1 | Avocado |



1. Blend
	1. Onion
	2. Tomato
	3. Galic
	4. Jalapeno
2. Add
	1. Cumin
	2. Oregano
	3. Oil
3. Sauté for 10 minutes
4. Add
	1. Bean
	2. Broth
5. Boil
6. Simmer for 15 minutes
7. Add
	1. Lime Juice
8. Blend
9. Garnish
	1. Avocado pieces