---
tags:
  - untried
  - air-frier
---
From: *The Vegan Air Frier* page 44 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 large | Potato |
| 3 Cup | Water |
| 1 Tablespoon | Oil |
| 1 teaspoon | Dill |
| 1 teaspoon | Chives |
| 1 teaspoon | Parsley |
| 1 teaspoon | Cayenne |
| 2 Tablespoon | Flour (chickpea, soy, buckwheat or millet) |

1. Cut 
	1. Potato
2. Soak for 20 minutes in
	1. Water
3. Drain, Rinse and pat dry
4. Mix
	1. Oil
	2. Dill
	3. Chives
	4. Parsley
	5. Cayenne
	6. Flour
5. Air fry for 20 minutes at 390