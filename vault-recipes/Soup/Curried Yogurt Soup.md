---
tags:
  - untried
---
From: Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 Cups | Yogurt |
| 1/2 Cup | Chickpea (flour) |
| 1 teaspoon | Chili Powder |
| 1 teaspoon | Turmeric |
| 1/2 teaspoon | Jalapeno (Chopped) |
| 1 Cup | Stock |
| 1/2 Cup | Oil (Vegetable) |
| 1 | Chili (Dried) |
| 1 teaspoon | Cumin |
| 2 teaspoon | Curry (leaves) |
| 6 Cloves | Garlic |
| 2 inches | Ginger |
| 4 Tablespoon | Cilantro (Chopped) |
| 1 teaspoon | sugar |

1. Mix and simmer for 10 minutes
	1. Chickpea Flour
	2. Chili Powder
	3. Turmeric
	4. Jalapeno
	5. Stock
3. In another pan Sauté until leaves are black
	1. Cumin
	2. Garlic
	3. Ginger
	4. Curry leaves
4. Stir in
	1. Cilantro
	2. Sugar
	3. Yogurt
	4. Simmered mix
5. Let stand for 10 minutes