---
tags:
  - untried
---
From: *Unknown library book*  
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Flour (Coconut) |
| 1 1/2 teaspoon | Ginger (dry) |
| 1 teaspoon | Cinnamon |
| 1/2 teaspoon | Salt |
| 1/2 teaspoon | Baking Soda |
| 1/2 Cup | Oil (coconut) |
| 1/2 Tablespoon | Honey |
| 1/4 Cup | Molasses (blackstrap) |
| 1 1/2 teaspoon | Vanilla |
| 2 | Gelatin Egg Substitute |

1. Blend
	1. Flour
	2. Ginger
	3. Cinnamon
	4. Salt
	5. Baking Soda
2. In another bowl, mix
	1. Oil
	2. Honey
	3. Molasses
	4. Vanilla
	5. Egg
3. Mix in
	1. Flour mix
4. Place mix on parchment paper
5. Bake at 350F for 21 minutes

Frost with
| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Oil (coconut) |
| 3/4 | Honey |