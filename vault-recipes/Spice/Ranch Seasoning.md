---
tags:
  - untried
---
From: *PlantPure Comfort food by Kim Campbell* page 67 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Nutritional Yeast |
| 1 Tablespoon | Dried Chives |
| 2 teaspoon | Onion Powder |
| 1 1/2 teaspoon | Smoked Paprika |
| 1 teaspoon | Dill |
| 1 teaspoon | Garlic Powder |
| 1/2 teaspoon | Pepper |
| 1/4 teaspoon | Salt |
