---
tags:
  - untried
  - hummas
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 can (14 oz) | Chickpeas |
| 2 | Lemon (juiced) |
| 2 Clove | Garlic |
| 4-6 Tablespoon | Tahini |
| 2 Tablespoon | Oil (olive) |
| 1 teaspoon | Paprika |
| 1 Teaspoon | Parsley |

1. Blend
	1. Chickpeas
	2. Lemon
	3. Garlic
	4. Tahini
2. Mix in bowl
	1. Oil
	2. Paprika
3. Garnish Chickpeas with
	1. Oil
	2. Paprika

Variation
1. Replace `Tahini` with `Peanut Butter`

Garnish
1. [[Zaatar]]
2. `Pomegranate Seeds` and `Cilantro`
3. `Toasted Pine Nuts` and `Caramlized Onions`