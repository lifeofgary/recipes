---
tags:
  - untried
---
From: *Spice Diet* page 233
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Bay Leaves (ground) |
| 2 Tablespoon | Celery Salt |
| 1 Tablespoon | Mustard (dry) |
| 2 teaspoon | Pepper |
| 2 teaspoon | Ginger (ground) |
| 2 teaspoon | Paprika |
| 1 teaspoon | Nutmeg |
| 1 teaspoon | Cloves |
| 1 teaspoon | Allspice |
| 1/2 teaspoon | Red Pepper Flakes |
| 1/2 teaspoon | Mace |
| 1/2 teaspoon | Cardamon |
| 1/4 teaspoon | Cinnamon |
