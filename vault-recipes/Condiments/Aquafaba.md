---
tags:
  - untried
  - air-frier
  - egg
---
From: *Vegan Cooking in your Air Fryer* page 22 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Batch | Cooked Chickpeas |

1. Drain Chickpeas into a Pot
2. Simmer and reduce by a third