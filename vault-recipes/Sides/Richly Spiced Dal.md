---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil (olive or coconut) |
| 1 | Onion  |
| 1 teaspoon | Cumin (seeds) |
| 1/4 teaspoon | Cardamom |
| 4 Clove | Garlic |
| 4 Cup | Broth |
| 2 Cup | Lentils (red) |
| 1 (14.5 oz) Can  | Tomato (chopped) |
| 2 Tablespoon | Ginger (fresh) |
| 1 teaspoon | Turmeric  |
| 1 teaspoon | Salt |
| 1/3 Cup | Cilantro (fresh, chopped) |

1. Sauté for 5 minutes
	1. Onion
2. Add and Sauté for 2 minutes
	1. Cumin
	2. Cardamom
	3. Garlic
3. Add and boil
	1. Stock
	2. Tomatoes
	3. Lentils
	4. Ginger
	5. Turmeric
	6. Salt
4. Simmer for 15 minutes
5. Garnish with Cilantro