---
tags:
  - untried
  - instapot
---
From: *Instant Pot Cookbook* page 85
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 lbs. | Lamb |
| 4 Cloves | Garlic |
| 4 teaspoon | Oil (olive) |
| 1/3 Cup | Stock |
| 1/4 Cup | Lemon Juice |
| | Salt |

1. In Instapot Sauté for 10 minutes
	1. Oil
	2. Lamb
2. Add
	1. Garlic
	2. Oil
	3. Stock
	4. Lemon Juice
3. Cook on `Meat/Stew` for 35 minutes