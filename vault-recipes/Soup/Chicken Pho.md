---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 95 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 lb. | Chicken |
| 2 inch | Ginger |
| 4 | Green Onion |
| 1/2 Bunch | Cilantro |
| 8 Cup | Broth |
| 1 Tablespoon | Fish Sauce |
| 1 teaspoon | Agave Nectar |
| 8 oz | Rice Noodles |
| 1/2 Cup | Cilantro |
| 1/2 Cup | Green Onion |
| 2 Cup | Bean Sprouts |
| 4 Sprigs | Basil |


1. In Instapot steamer basket
	1. Chicken
	2. Ginger
	3. Green Onion
	4. Cilantro
	5. Broth
2. Select `Soup/Broth`
3. Natural Release for 20 minutes
4. Discard
	1. Ginger
	2. Green Onion
	3. Cilantro
5. Add
	1. Fish Sauce
	2. Agave
6. Cook noodles
7. Add
	1. Chicken
	2. Cilantro
	3. Green onion
	4. Shallot
	5. Broth
8. Garnish with
	1. Bean Sprouts
	2. Basil
	3. Lime Wedges