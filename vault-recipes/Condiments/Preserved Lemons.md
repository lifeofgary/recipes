---
tags:
  - untried
---
From: New Mediterranean Diet Cookbook
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 24-36 | whole lemons |
| 1-2 Cup | Salt (Pickling, Kosher, or Sea) |

1. Clean lemon
2. Sterilize  preserving jar with boiling water and wait 10 minutes
3. Quarter Lemon stopping 1/2 from the steam
4. Pack into Jar and fill with 1" to spare
5. Add 1/3 Cup salt
6. Add more lemon juice to cover all lemons
7. Put aside for 3 weeks