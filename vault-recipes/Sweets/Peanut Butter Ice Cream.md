From: *Paleo Cooking from Elana's Pantry* page 105
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can (13 oz) | Coconut Milk |
| 1 Tablespoon | Coconut Oil |
| 1/4 Cup | Honey |
| 1/4 teaspoon | Nut Butter (peanut, sunflower) |
| 1/4 teaspoon | Stevia |
| 1/4 teaspoon | Vanilla |
| 1/8 teaspoon | Salt |


1. Puree until smooth
	1. Coconut Milk
	2. Coconut Oil
2. Blend in
	1. Honey
	2. Nut Butter
	3. Stevia
	4. Vanilla
	5. Salt
3. Add Chocolate Chips
4. Put in Ice Cream maker