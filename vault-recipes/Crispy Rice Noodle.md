---
tags:
  - bowl
  - untried
---
From *Americas test kitchen - Bowls* pg 215

| Amount | Ingredient     |
| ------ | -------------- |
| 6 Cup  | Oil            |
| 2 oz   | Noodles (rice) |
1. Heat to 400
2. Cook for 10-30 seconds
	1. Noodles