---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 39
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon + 1 teaspoon | Ginger Powder |
| 1 Tablespoon + 1 teaspoon | Garlic Powder |
| 1 Tablespoon | Cumin |
| 1 Tablespoon | Paprika |
| 2 teaspoon | Fenugreek |
| 1 1/2 teaspoon | Cardamom |
| 1 1/2 teaspoon | Nutmeg |
| 1 teaspoon | Coriander |
| 1 teaspoon | Pepper |
| 1/2 teaspoon | Turmeric |
| 1/2 teaspoon | Cayenne |
| 1/2 teaspoon | Cinnamon |

