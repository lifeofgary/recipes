---
tags:
  - untried
---
From: *Unknown Library Book* page 111 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Onion |
| 6 cloves | Garlic |
| 2 Cup | Broth |
| 3 Tablespoon | Nutritional Yeast |
| 2 Tablespoon | Starch |
| 1 Tablespoon | Tamari |
| 1 teaspoon | Thyme |
| 1/2 teaspoon | Salt |

1. Make a boat with Aluminum foil and parchment paper
2. Bake at 375F for 20 minutes
	1. Onion
	2. Garlic
3. Cool for 5 minutes
4. Blend
5. Add
	1.  Broth
6. Blend for 2 minutes
7. Add
	1. Nutritional Yeast
	2. Starch
	3. Tamari
	4. Thyme
	5. Salt
8. Blend for 1 minute
9. Cook at a medium heat for 3-4 minutes, until thick