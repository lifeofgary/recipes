---
tags:
  - untried
---
From: *30 minute vegan: Soup's on*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 6 Cups | Stock |
| 1/4 Cup + 2 Tablespoon | Vinegar (rice) |
| 3 Tablespoon | Tamari |
| 1 Cup | Onion (yellow) |
| 1/2 Cup | Celery |
| 5 Cloves | Garlic |
| 1 Tablespoon | Chile Pepper (hot) |
| 2 Cups | Broccoli |
| 3/4 Cups | Carrot |
| 1/2 Cup | Bell Pepper (Red) |
| 1/2 Cup | Bamboo Shoots or Water Chestnuts |
| 1/2 teaspoon | Salt |
| 3/4 teaspoon | [[Five Spice]] |
| 1/4 teaspoon | Red Pepper Flakes |
| 1/4 teaspoon | Szechuan Pepper |
| 1/2 Cup | Peas (snow) |
| 1/4 Cup | Green Onion |

1. Cook for 15 minutes
	1. Stock
	2. Vinegar
	3. Tamari
	4. Onion
	5. Celery
	6. Garlic
	7. Chile Pepper
	8. Broccoli
	9. Carrot
	10. Bell Pepper
	11. Bamboo Shoots
	12. Salt
	13. Five Spice
	14. Red Pepper Flakes
	15. Szechuan Pepper
2. Add and cook for 5 minutes
	1. Snow Peas
3. Add Green Onions

Variations
1. Add 1/4 Cup water with Arrow root.  Add to soup when Vegetables are cooked then cook for 5 minutes
2. Add 1 Tablespoon Sesame oil with Green Onions
3. Add 1 Tablespoon Ginger with Garlic
4. Sauté Onion and Garlic in 1 Tablespoon Oil for 3 minutes

5. Sauté for 10 minutes