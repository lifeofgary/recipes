---
tags:
  - stars/5
---
From: *Incredible Vegan Ice Cream* page 13
Rating: 5/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can | Coconut Milk |
| 200 ml | Milk (Oat) |
| 1/3 Cup | Sugar |
| 1/4 Cup | Agave |
| 1 teaspoon | Vanilla |
| 1  | Vanilla Bean |
| 1 pinch | Black Salt |

1. In blend mix
	1. Coconut Milk
	2. Milk
	3. Sugar
	4. Agave
	5. Vanilla 
	6. Vanilla Bean
	7. Black Salt
2. Chill for at least 1 hour
3. Put in Ice Cream Maker