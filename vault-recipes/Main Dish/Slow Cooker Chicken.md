---
tags:
  - untried
---
From: [Slow-cooker: whole chicken in coconut milk](https://www.thekitchn.com/how-to-cook-a-whole-chicken-in-coconut-milk-in-the-slow-cooker-248235)
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Chicken |
| 1 Can (adjust) | Coconut Milk |
| (adjust) | Broth (chicken) |
| (adjust) | Ginger |
| (adjust) | Garlic |
| 2 Bunch | Basil |

1. Salt Chicken inside and out
2. Brown Chicken under Broiler
3. In Slow Cooker cook for 6 hours
	1. Chicken
	2. Broth
	3. Coconut Milk
	4. Ginger 
	5. Garlic
