---
tags:
  - untried
---
From:  *The Ultimate Instant Pot Healthy Cookbook* Page 208
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | [[Aquafaba]] |
| 1 Tablespoon | Chickpeas or White Beans |
| 1 Tablespoon | Lemon Juice |
| 1/2 teaspoon | Salt
| 1/8 teaspoon | Xanthan Gum |
| 1/2 Cup | Oil (Avocado) |
| 1 Clove | Garlic |

1. Blend for 1 minute
	1. [[Aquafaba]]
	2. Chickpeas
	3. Lemon Juice
	4. Salt
	5. Xanthan Gum
	6. Oil
	7. Garlic