---
tags:
  - untried
---
From: *Unknown library book* page
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Cup | Chickpeas |
| 1 teaspoon | Salt |
| 3 Tablespoon | Miso (white) |
| 1 Tablespoon | Oil (sesame seed) |
| 1 Tablespoon | Oil (avocado) |
| 3 Tablespoon | Lemon Juice |
| 1 teaspoon | Sesame Seed |

1. Blend for 3 minutes
	1. Chickpeas
	2. Salt
	3. Miso
	4. Oil
	5. Lemon Juice
2. Thin with water
3. Garnish with
	1. Sesame Seed