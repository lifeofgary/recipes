---
tags:
  - untried
---
From: *Unknown library book* page
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Carrot (large) |
| 3 | Shallots |
| 2 Clove | Garlic |
| 1 Inch | Ginger |
| 2 Tablespoon | Oil (coconut) |
| 1 Tablespoon | Turmeric |
| 1 Cup | Lentil (dry) |
| 4 Cup | Broth |
| 1 Can (15 oz) | Coconut Milk |
| 1 Tablespoon | Sugar (brown) |
| 1 Bunch | Spinach |
| 1/2 | Lime (juiced) |

1. Blend
	1. Carrot
	2. Shallots
	3. Garlic
	4. Ginger
2. Sauté for 7 minutes
	1. Carrot
	2. Turmeric
	3. Oil
3. Add
	1. Lentils
	2. Broth
4. Boil
5. Simmer for 30 minutes
6. Add
	1. Coconut Milk
	2. Sugar
	3. Spinach
7. Cook until `Spinach` is wilted
8. Add 
	1. Lime Juice
	2. Salt