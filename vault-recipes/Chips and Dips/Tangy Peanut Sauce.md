---
tags:
  - untried
  - sauce
---
From: *Paleo Cooking from Elana's Pantry* page 90
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Lime Juice |
| 1 Tablespoon | Ginger (fresh) |
| 1 teaspoon | Garlic |
| 1 teaspoon | Vinegar (ume plum) |
| 1/4 Cup | Almond Butter (roasted) |
| 1/2 Cup | Coconut Milk |

1. Blend
	1. Lime Juice
	2. Ginger
	3. Garlic
	4. Vinegar
	5. Almond Butter
	6. Coconut Milk