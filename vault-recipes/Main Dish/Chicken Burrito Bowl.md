---
tags:
  - untried
  - instapot
---
From: *Meal prep in an instant* page 45
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 lb. | Chicken (frozen) |
| 2/3 Cup | Rice (brown, uncooked) |
| 2/3 Cup | Black Beans |
| 2 x 15oz Can | Tomatoes (diced) |
| 10 Cloves | Garlic (minced) |
| 3 teaspoon | Crushed Red Peppers |
| 3 teaspoon | cayenne pepper |
| 5 teaspoon | chopped cilantro |
| 2 Cups | Chicken Stock |

1. put everything in insta pot and cook for 25 minutes