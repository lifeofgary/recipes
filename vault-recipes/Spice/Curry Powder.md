---
tags:
  - untried
---
From: [The Herb and Spice Companion](https://kcls.bibliocommons.com/v2/record/S82C605634) page 154
Rating: ?/5

## Hot

| Amount | Ingredient |
|----------|-------------|
| 1 teaspoon | Cloves |
| 2 teaspoon | Cardamom |
| 1 teaspoon | Mustard Seed |
| 1 teaspoon | Black Poppy Seed |
| 1 teaspoon | Chili Powder |
| 2 teaspoon | Cinnamon |
| 2 1/2 teaspoon | Cumin |
| 1/2 teaspoon | Fenugreek |
| 1 teaspoon | Nutmeg |
| 2 teaspoon | Pepper |
| 1 teaspoon | Curry Leaf (ground) |


## Medium Curry Paste

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 teaspoon | Cumin |
| 1 1/2 teaspoon | Coriander |
| 1 teaspoon | [[Garam Masala#Variation]] |
| 1 teaspoon | Garlic Powder |
| 1/2 teaspoon | Paprika |
| 1 teaspoon | Turmeric |
| 1 1/2 teaspoon | Chili Powder |
| Pinch | Salt |
| 1 teaspoon | Mint |
| 1 Tablespoon | Water |
| 2 Tablespoon | Lemon Juice |
| 1 Tablespoon | Wine Vinegar |
| 1 Tablespoon | Oil (Vegetable) |

1. Mix everything together
2. Cook for 10 minutes


## Mild Curry Paste

| Amount | Ingredient |
|----------|-------------|
| 1 teaspoon | Cardamom |
| 1 teaspoon | Chili Powder |
| 1 teaspoon | Pepper |
| 1 teaspoon | Cumin |
| 4 teaspoon | Coriander |
| 1 1/2 teaspoon | Turmeric |
| 1 teaspoon | Fenugreek |

## Kashmiri

From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 28

| Amount | Ingredient |
|----------|-------------|
| 1/2 teaspoon | Cloves |
| 1 Tablespoon | Cumin |
| 1 Tablespoon | Coriander |
| 1 Tablespoon | Fennel |
| 2 teaspoon | Cardamom |
| 1 1/2 teaspoon | Cinnamon |
| 1 teaspoon | Turmeric |

From `Spice: flavors of the eastern mediterranean` page 2-0

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Coriander |
| 1/4 Cup | Turmeric |
| 1/4 Cup | Ginger (dried) |
| 3 Tablespoon | Pepper |
| 2 Tablespoon | Cardamon |
| 2 Tablespoon | Cinnamon |


