---
tags:
  - untried
---
From: *Soups from around the world* page 55
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 lbs. | Chicken Breast (boneless) |
| 1 Tablespoon | Oil |
| 2 Tablespoon | Margarine |
| 2 1/2 teaspoon | [[Garam Masala]] |
| 1 1/2 teaspoon | Cumin |
| 1 1/2 teaspoon | Coriander |
| 1 teaspoon | Turmeric |
| 2 | Onion |
| 2 | Carrot |
| 1 Rib | Celery |
| 1/2 Cup | Coconut (sweetened, shredded) |
| 4 Clove | Garlic |
| 4 teaspoon | Ginger (fresh) |
| 1/4 Cup | Flour |
| 1 teaspoon | Tomato Paste |
| 7 Cup | Broth |
| 1/2 Cup | Lentils |
| 2 Tablespoon | Cilantro |
| 1 Cup | Yogurt |

1. Brown both sides, about 5 minutes
	1. Chicken
2. Set aside
3. Sauté for 30 minutes
	1. [[Garam Masala]]
	2. Cumin
	3. Coriander
	4. Turmeric
4. Add and Sauté for 7 minutes
	1. Onion
	2. Carrot
	3. Celery
	4. Coconut
5. Add and Sauté for 30 minutes
	1. Garlic
	2. Ginger
6. Add and cook for 1 minutes
	1. Flour
	2. Tomato Paste
7. Stir in
	1. Broth
8. Boil
9. Add and simmer for 20 minutes
	1. Chicken
10. Puree
11. Add and simmer for 45 minutes
	1. Lentils
12. Add
	1. Cilantro
	2. Yogurt