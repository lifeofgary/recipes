---
tags:
  - untried
---
From: *Mastering Spices* page 142
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lbs. | Salmon |
| 3 Tablespoon | Salt |
| 1 Tablespoon | Sugar |
| 6 sprig | Dill |
| 1 1/2 teaspoon | Dill Seed |
| 3/4 teaspoon | Mustard Seed (yellow) |
| 1/2 teaspoon | Caraway |

1. Mix
	1. Salt
	2. Sugar
	3. Dill Seed
	4. Mustard
	5. Caraway
2. Place in Pan
	1. Dill
	2. 1/2 Sugar Mixture
3. Put on Top
	1. Salmon
4. Sprinkle the rest of
	1. Sugar Mixture
5. Cover and Refrigerate for 18 hours
6. Rinse

## 1.

Use 

| Amount | Ingredient |
|----------|-------------|
| 1 lbs. | Salmon |
| 3 Tablespoon | Salt |
| 1 Tablespoon | Sugar |
| 6 sprig | Cilantro |
| 1 1/2 teaspoon | Fennel Seed |
| 3/4 teaspoon | Cumin |

2.

Use 

| Amount | Ingredient |
|----------|-------------|
| 1 lbs. | Salmon |
| 3 Tablespoon | Salt |
| 1 Tablespoon | Sugar |
| 6 sprig | Rosemary |
| 1 1/2 teaspoon | Coriander |
| 2 Tablespoon | Rosemary |
| 1 1/2 teaspoon | Juniper Berry |
| 3/4 teaspoon | Orange Zest |
