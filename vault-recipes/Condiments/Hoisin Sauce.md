---
tags:
  - untried
---
From: *PlantPure Comfort food* by Kim Campbell page 134 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Tamari |
| 2 Tablespoon | Peanut Butter |
| 2 Tablespoon | Maple Syrup |
| 1 Tablespoon | Molasses |
| 1 Tablespoon | Rice Vinegar |
| 1 teaspoon | Sriracha |
| 1/2 teaspoon | Garlic Powder |
| 1/2 teaspoon | Onion Powder |
| 1/4 teaspoon | [[Five Spice]] powder

Blend Everything|