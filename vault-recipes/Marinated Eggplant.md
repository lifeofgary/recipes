---
tags:
  - bowl
  - untried
---
From *Americas test kitchen - Bowls* pg 206

| Amount  | Ingredient            |
| ------- | --------------------- |
| 12 oz   | Eggplant (1in rounds) |
| 1/2 tsp | Salt                  |
| 2 tsp   | Oil                   |
| 2 tsp   | Red Wine Vinegar      |
| 1 tsp   | Capers                |
| 1 Clove | Garlic                |
| 1/4 tsp | Lemon Zest            |
| 1/4 tsp | oregano               |
| 1 Tbsp  | Mint (fresh)          |
1. Broil until brown (4-8 min)
	1. Eggplant
	2. Oil
2. Whisk
	1. Oil (1 1/2 Tbsp )
	2. Vinegar
	3. Capers
	4. Garlic
	5. Zest
	6. Oregano
3. Add 
	1. Eggplant
	2. Mint
4. Cool Eggplant (1 hour)
	1. 