From: *The Green Barbecue* page 153
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Avocado (unripe, 1/4 inch cubes) |
| 1 | Grapefruit (1/4 inch cubes) |
| 1/2 | Pineapple (1/4 inch cubes) |
| 1 | Red Chili |
| 2 | Limes (juice) |
| 3/4 oz | Mint |
| 1 teaspoon | Salt |



