---
tags:
  - untried
---
From: *Unknown library book* page 118
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Flax Meal |
| 1/2 Cup | Sugar (coconut) |
| 1 Cup | Flour (almond) |
| 1 teaspoon | Baking Soda |
| 2 teaspoon | Cinnamon |
| 2 teaspoon | Ginger |
| 1/4 teaspoon | Allspice |
| 1/8 teaspoon | Vanilla |
| 1 teaspoon | Stevia |
| 1/2 Cup | Apple Sauce |
| 1/4 Cup | Coconut Oil |

1. Mix
	1. Flax Meal
	2. Sugar
	3. Flour
	4. Baking Soda
	5. Cinnamon
	6. Ginger
	7. Allspice
	8. Vanilla
	9. Stevia
	10. Apple Sauce
	11. Coconut Oil (melted)

Variation
	1. Bake at 350 for 18 minutes
	