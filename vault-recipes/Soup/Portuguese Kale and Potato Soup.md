---
tags:
  - untried
---
From: *30 minute vegan: Soup's on*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 6 Cups | Stock |
| 1 1/4 Cup | Onion (yellow) |
| 1/2 Cup | Celery |
| 6 Cloves | Garlic |
| 1 1/2 Cup | Potato |
| 3/4 Cup | Carrot |
| 1/2 Cup | Mushrooms (shiitake) |
| 1/2 Cup | Bell Pepper (Green) |
| 2 Tablespoon | Nutritional Yeast |
| 1 1/2 Tablespoon | Paprika | 
| 2 teaspoon | Salt |
| 1/2 teaspoon | Liquid Smoke |
| 2 Tablespoon | Lemon Juice |
| 3 Cups | Kale (tightly packed) |

1. Cook for 15 minutes
	1. Stock
	2. Onion
	3. Celery
	4. Garlic
	5. Potato
	6. Carrot
	7. Mushrooms
	8. Bell Pepper
	9. Nutritional Yeast
	10. Paprika
	11. Salt
	12. Liquid Smoke
2. Add and cook for 8 minutes
	1. Lemon Juice
	2. Kale

Variation
1. Add 8 oz of sausage
2. Sauté Onion and Garlic in 1 Tablespoon oil for 3 minutes 

3. Sauté for 10 minutes