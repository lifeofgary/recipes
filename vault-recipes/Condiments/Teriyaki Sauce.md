---
tags:
  - untried
---
From: **Great Bowls of Food** page 75 
Rating: ?/5

Use by [[Teriyaki Chicken Bowl]]

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Tamari |
| 1/4 Cup | Honey |
| 2 Tablespoons | Orange juice |
| 1 Tablespoon | Vinegar (rice) |
| 1 1/2 teaspoon | Oil (sesame) |
| 1/4 teaspoon | Ginger (ground) |
| 1 Clove | Garlic |


1. Boil for 2 minutes
	1. Tamari
	2. Honey
	3. Orange Juice
	4. Vinegar
	5. Oil
	6. Ginger
	7. Garlic