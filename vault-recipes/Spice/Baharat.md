---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 25
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 teaspoon | Cloves |
| 1 Tablespoon | Allspice |
| 1 Tablespoon | Cumin |
| 1 Tablespoon | Pepper |
| 1 teaspoon | Coriander |
| 1 teaspoon | Nutmeg |
| 1 teaspoon | Cinnamon |
| 1 teaspoon | Paprika |
| 1 teaspoon | Cardamom |


From middle eastern cuisine.  Food on meat dishes or [[Sweet Onion Jam]]