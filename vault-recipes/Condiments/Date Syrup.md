---
tags:
  - untried
---
From: *The Dairy Free and Gluten Free Kitchen*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Water |
| 1 Cup | Dates (medjool) |
| 1 teaspoon | Lemon Juice |

1. Blend for 2 minutes
	1. Water
	2. Dates
	3. Lemon Juice
2. Store

Variation
1. Replace water with orange juice	