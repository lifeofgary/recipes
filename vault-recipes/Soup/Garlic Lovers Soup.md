---
tags:
  - untried
---
From: *30 minute vegan: Soup's on*  133
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 or 5 Heads (1 Cup) | Garlic |
| Some | Oil (Olive) |
| 2 teaspoon | Salt |
| 3/4 Cup | Brazil Nuts |
| 5 Cup | Stock |
| 1 1/4 Cup | Onion (Yellow) |
| 1/2 Cup | Celery |
| 1 Cup | Carrot |
| 1 Cup | Mushrooms (shiitake) |
| 1 1/2 Cup | Potatoes |
| 2 Tablespoon | Parsley (fresh) | 
| 2 Tablespoon | Basil (fresh) |
| 1 Tablespoon | Lemon Juice |
| 3 Cloves | Garlic (pressed) |

1. Cook Garlic
	1. Heat Oven to 450
	2. Slice off top of garlic heads
	3. Baste in Olive Oil then salt
	4. Put in Oven with a small bowl of water
	5. Roast for 25 minutes till golden brown
2. Roast for 7 Minutes then blend
	1. Brazil Nuts
3. Cook for 10 minutes
	1. Stock
	2. Onion
	3. Celery
	4. Carrot
	5. Mushroom
	6. Potatoes
4. Mix and Serve
	1. Garlic
	2. Nuts
	3. Stock mix
	4. Parsley
	5. Basil
	6. Lemon Juice
	7. Raw Garlic