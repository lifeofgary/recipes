---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Lentil |
| 4 Cup | Broth |
| 2 Tablespoon | Oil | 
| 1 | Onion |
| 3 Clove | Garlic |
| 1 Tablespoon | Ginger |
| 1 Tablespoon | Curry Powder |
| 1 | Tomato |
| 2 Tablespoon | Cilantro |
| 1/2 Cup | Yogurt |

1. Microwave for 8 minutes
	1. Lentils
	2. Broth (2 Cup)
2. Sauté for 30 seconds
	1. Oil
	2. Garlic
	3. Ginger
	4. Curry powder
3. Add
	1. Lentils
	2. Tomato
	3. Broth (2 Cup)
4. Simmer for 15 minutes
5. Garnish with
	1. Cilantro
	2. Yogurt