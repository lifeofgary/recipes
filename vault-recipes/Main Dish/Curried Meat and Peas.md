---
tags:
  - untried
---
From: Unknown from Library
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 | Onion |
| 3/4 Cup | Oil |
| 1 teaspoon | [[Garam Masala]] |
| 1/2 Cup | Water |
| 1 inch | Ginger |
| 4 Cloves | Garlic |
| 2 Tablespoon | Tomato Paste |
| 1 teaspoon | Turmeric |
| 2 teaspoon | Coriander |
| 2 lbs | Beef or Lamb (ground) |
| 3/4 Cup | Peas | 
| 2 teaspoon | [[Garam Masala]] |
| 1 Tablespoon | Lemon Juice |
| | Mint (garnish) |

1. Sauté for 5 minutes
	1. Oil
	2. Garam masala
	3. Onions
2. Add and cook for 5 minutes
	1. Ginger
	2. Garlic
	3. Tomato Paste
	4. Turmeric
	5. Coriander
3. Add and simmer for 20 minutes
	1. Meat
4. Add
	1. Peas
	2. [[Garam Masala]]
	3. Lemon Juice
	4. 