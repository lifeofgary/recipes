---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 147
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil |
| 3 lbs. | Chicken (boneless, cubed) |
| 1/3 teaspoon | Salt |
| 1/3 Cup | Green Onion |
| 3 | Carrot | 
| 1/3 Cup | Prunes |
| 1/3 Cup | Wine (red) |
| 3/4 Cup | Broth |

1. Sauté for 5 minutes
	1. Oil
	2. Chicken
2. Add
	1. Salt
	2. Green Onion
	3. Carrot
	4. Prunes
	5. Wine
	6. Broth
3. Cook for 15 minutes on `Poultry`