---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 71
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/3 Cup | Sugar |
| 1/3 Cup | Sugar (brown) |
| 1 Tablespoon | [[Baharat]] |
| 1 1/2 teaspoon | Salt |
| 1 | Egg white |
| 4 Cup | Nuts |

1. Mix
	1. Sugar
	2. Brown Sugar
	3. [[Baharat]]
	4. Salt
	5. Nuts
2. Bake at 250F for 45 minutes 