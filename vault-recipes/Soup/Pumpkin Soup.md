---
tags:
  - untried
---
From: *Unknown library book* page
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil |
| 1 | Onion |
| 1 1/2 lbs | Pumpkin |
| 1 lbs | Potatoes |
| 2 1/2 Cups | Stock |
| pinch | Nutmeg |
| 1 teaspoon | Tarragon |
| 2 1/2 Cup | Milk (Oat) |
| 1 - 2 teaspoon | Lemon Juice |


1. Sauté for  5 minutes
	1. Onion
2. Add and cook on a low heat for 10 minutes
	1. Pumpkin
	2. Potatoes
3. Add and simmer for 10 minutes
	1. Stock
	2. Nutmeg
	3. Tarragon
4. Puree then Add
	1. Milk
	2. Lemon Juice