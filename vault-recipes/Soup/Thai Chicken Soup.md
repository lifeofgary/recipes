---
tags:
  - untried
---
From: *Nourish: The Paleo Healing Cookbook* page 48
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (avocado or coconut) |
| 3 Tablespoon | Thai Green Curry Paste |
| 4 Cup | Broth |
| 1 lb. | Chicken |
| 2 Cup | Coconut Milk |
| 1 Tablespoon | Fish Sauce |
| 2 Tablespoon | Lime Juice |
| 4 Cup | Spinach |
| 4 | Green Onion |
| 2 Tablespoon | Cilantro |
| 2 Tablespoon | Basil |

1. Sauté for 2 minutes
	1. Curry Paste
	2. Oil
2. Add and boil
	1. Broth
3. Add
	3. Chicken
4. Simmer for 5 minutes
5. Add
	1. Coconut Milk
	2. Fish Sauce
6. Reduce Heat
7. Add Lime Juice
8. Wilt
	1. Spinach
9. Add
	1. Green Onion (whites)
10. Garnish with
	1. Green Onion (greens)
	2. Basil