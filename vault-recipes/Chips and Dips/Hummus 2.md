---
tags:
  - untried
  - hummas
---
From: The Hummus Blog
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Chickpeas |
| 1/2 Cup | Tahini |
| 1 | Lemon (juiced) |
| 2 Cloves | Garlic |
| 1/2 teaspoon | Salt |
| 1 teaspoon | Cumin |
| | Water |

1. Blend
	1. Chickpeas
2. Add and blend
	1. Tahini
	2. Lemon Juice
	3. Garlic
	4. Salt
	5. Cumin
3. Thin with water to desired thickness