---
tags:
  - untried
---
From: 30 minute vegan: Soup's on
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 17 oz | Broth |
| 14 oz | Black Beans |
| | Salsa |
| | Cilantro |

1. Simmer for 5 minutes
	1. Salsa
	2. Beans
	3. Broth
2. Garnish with 
	1. Cilantro
	2. Mayo
	3. Zest
