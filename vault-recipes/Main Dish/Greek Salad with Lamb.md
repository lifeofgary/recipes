---
tags:
  - untried
---
From: *Primal Blueprint Quick & Easy Meals* page 81`
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 pound | lamb |
| 1/2 Cup | herbs (dill, mint, oregano, parsley) |
| 2 heart  | Lettuce (romaine , finely chopped)
| 1-2 | tomatoes (chopped)|
| 1 | cucumber (large, chopped) |
| 1 Cup | Olives (Kalamata) |
| 1/4 Cup | Lemon Juice |
| 1/2 Cup | Oil (olive) |

1. Sauté for 10 minutes
	1. Lamb
	2. Herbs
2. When cooked add
	1. Lettuce
	2. Tomato
	3. Cucumber
	4. Olives
3. Whisk and add to Salad
	1. Lemon Juice
	2. Olive Oil