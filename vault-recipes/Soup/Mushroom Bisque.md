---
tags:
  - untried
---
From: *Dairy Free and Gluten Free Kitchen* page 62
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 oz | Mushroom (dried porcini) |
| 3/4 Cup | Hot Water |
| 1 lbs. | Mushrooms |
| 1 Cup | Wine (white, divided) |
| 1 | Onion |
| 4 Cup | Stock |
| 2 | Potato |
| 1/4 Cup | Parsley (fresh) |
| 1/2 teaspoon | Salt |
| 1/4 teaspoon | Nutmeg |
| Pinch | Cardamom |
| 1 Cup | Milk (non-dairy) |

1. Soak for 20 minutes
	1. Mushrooms (dried)
	2. Water
2. Drain and reserve water
3.  Sauté for 12 minutes
	1. Mushrooms (dried)
	2. Mushrooms
	3. Wine (1/2 Cup)
4. Add and  Sauté for 5 minutes
	1. Wine (1/2 Cup)
	2. Onion
5. Add and Boil
	1. Mushroom water reserve
	2. Potato
	3. Parsley
	4. Salt
	5. Nutmeg
	6. Cardamom
6. Simmer for 25 minutes