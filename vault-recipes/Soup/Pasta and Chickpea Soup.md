---
tags:
  - untried
---
From: Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 Tablespoon | Oil (Olive) |
| 1 | Onion |
| 2 | Carrot |
| 2 Sticks | Celery | 
| 14 oz | Chickpeas (cooked) |
| 7 oz | Cannellini Beans |
| 2/3 Cup | Tomatoes (crushed) |
| 1 /2 Cup | Water |
| 6 1/4 Cup | Stock |
| 2 Cups | Pasta |
| 1 sprig | Rosemary (fresh) |

1. Sauté for 7 minutes
	1. Onion
	2. Carrot
	3. Celery
2. Add and cook for 5 minutes
	1. Chickpeas
	2. Beans
3. Add and cook for 3 minutes
	1. Water
	2. Pasta
4. Add and simmer for a hour
	1. Stock
	2. Rosemary
