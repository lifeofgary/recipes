---
tags:
  - untried
---
From: *Unknown library book* page 76
Rating: ?/5

Used by [[Roasted Pesto Mushrooms]]

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Almonds (blanched) |
| 2 Tablespoon | Nutritional Yeast |
| 1 teaspoon | Salt |
| 1 teaspoon | Garlic Powder |
| 1 teaspoon | Onion Powder |

1. Blend
	1. Almonds
	2. Nutritional Yeast
	3. Salt
	4. Garlic Powder
	5. Onion Powder