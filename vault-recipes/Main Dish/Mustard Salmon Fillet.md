---
tags:
  - untried
---
From: *Unknown library book* page 75
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 lb. | Salmon Fillet |
| 1 Cup | Dijon Mustard |

1. Dry Salmon
2. Place skin side down
3. Coat with
	1. Mustard
4. Bake for 15-20 minutes