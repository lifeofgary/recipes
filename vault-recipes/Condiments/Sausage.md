---
tags:
  - untried
---
From: *Unknown Library Book* page 134
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 lbs | Meat (lamb or chicken) |
| 1 lbs | Oil (Olive or Coconut) |
| 2 1/2 Tablespoon | Salt |
| 2 Tablespoon | Garlic |
| 1 Cup | Red Wine |

1. Mix and refrigerate for 2 - 24 hours
	1. Meat
	2. Fat
	3. Salt
	4. Garlic
2. Add Wine
3. Fry bite sized pieces at 150F for 10 minutes

Variations
1. Italian: Add 
	1. 2 Tablespoon Fennel
	2. 3 Tablespoon Paprika
	3. 1/4 Cup Oregano
2. Sweet Italian: Add
	1. 2 Tablespoon Sugar
	2. 2 Tablespoon Fennel
	3. 3 Tablespoon Paprika
	4. 1/4 Cup Oregano
3. Breakfast: Add
	1. 1/4 Cup Ginger
	2. 1/4 Cup Sage
4. Lamb
	1. 2 Tablespoon Zest
	2. 3 Tablespoon Coriander
	3. 1/4 Cup Lemon Juice
	4. 1/4 Cup Orange Juice
	5. 1/4 Cup Mint
	6. 1 1/2 Cup Olives

6. Sauté for 10 minutes