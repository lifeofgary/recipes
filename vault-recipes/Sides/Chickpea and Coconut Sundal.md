---
tags:
  - untried
---
From: *Unknown library book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil |
| 1 tsp | Mustard (seed, black) |
| 1 | Green Chili |
| 1 teaspoon | Ginger (fresh) |
| 5 | Curry Leaves (optional) |
| 1/2 teaspoon | Salt |
| 1 Tablespoon | Lentils |
| 1 Can (14 oz) | Chickpeas |
| 4 Tablespoon | Coconut (grated) |

1. Sauté
	1. Oil
	2. Mustard
2. Add and Sauté
	1. Green Chili
	2. Ginger
	3. Curry Leaves
	4. Salt
	5. Dal
3. Add
	1. Coconut
	2. Cilantro


