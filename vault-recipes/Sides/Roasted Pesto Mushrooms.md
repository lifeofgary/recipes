---
tags:
  - untried
---
From: *Unknown library book* page 140 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 20  | Mushrooms (chestnut or cremini) |
| 2 Tablespoon | Oil (olive) |
| 1 Tablespoon | Vinegar (balsamic) |
| 20 teaspoon | [[Pesto for Mushrooms]]

1. Coat Mushrooms with
	1. Oil
	2. Vinegar
2. Bake on parchment paper at 350F for 10 minutes
3. Drain Mushrooms
4. Fill with [[Pesto for Mushrooms]]
5. Roast for 5-10 minutes
6. Top with [[Vegan Parmesan]]