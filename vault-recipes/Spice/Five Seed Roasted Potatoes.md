---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 118
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 lbs. | Potato |
| 2 Tablespoon | Oil (olive) |
| 1 teaspoon | Mustard seed (brown) |
| 1 teaspoon | Nigella |
| 1 teaspoon | Cumin |
| 1/2 teaspoon | Ajwain Seed (or fennel) |
| 1/2 teaspoon | Fennel |
| 1 teaspoon | Salt (divided) |


1. Sauté for 1 minutes
	1. Oil
	2. Mustard seed
	3. Nigella
	4. Cumin
	5. Ajwain
	6. Fennel
2. Cook for 20 minutes
	1. Pototo
	2. Salt