---
tags:
  - untried
  - egg
---
From: *Vegan Air Fryer* page 16 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Flaxseed (ground) |
| 3 Tablespoon | Water (warm) |

1. Combine
	1. Flaxseed
	2. Water
2. Let set for 10 minutes