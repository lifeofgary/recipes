---
tags:
  - instapot
  - stars/5
---
From: *Unknown library book* page
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 or 2 | shallot |
| 1/4 Cup or 2 inch | Ginger |
| 1/4 Cup or 2 inch | Galangal |
| 3 Tablespoon | Lemon grass |
| 8 | Kefir lime leaves |
| 2 Cups | Broth |
| 1 lb. | Chicken |
| 8 oz | Mushrooms |
| | Veggies (bell pepper, snow peas...ect) |
| 3 Tablespoon | Fish Sauce |
| 1 1/2 teaspoon | Salt |
| 2 can (14 oz) | Coconut Milk |
| 3 Tablespoon | Lime Juice |
| 2 teaspoon | Sugar (coconut)

1. Place in Instapot
	1. Shallot
	2. Ginger
	3. Galangal
	4. Lemon grass
	5. Lime leaves
	6. Broth
	7. Chicken
	8. Mushrooms
	9. Veggies
	10. Fish Sauce
	11. Salt
2. On high pressure cook for
	1. 7 Minutes (chicken breast)
	2. 10 Minutes (chicken thigh)
3. Naturally release for 10-15 minutes
4. Add
	1. Coconut Milk
	2. Lime Juice
	3. Sugar