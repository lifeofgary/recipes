---
tags:
  - untried
---
From: *The Primal Kitchen* page 129
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| amount | ingredient |
| 1 Cup | Olives (green) |
| 1 Cup | Olives (black) |
| 1/2 Cup | Olives (Kalamata) |
| 1/2 Cup | Oil (Avocado) |
| 1 Cup | Parsley |
| 1 Cup | Cilantro |
| 1 Clove | Garlic |
| 2 Tablespoon | Lime Juice |
| 1/4 teaspoon | Salt |

1. Blend everything together and slowly add oil
