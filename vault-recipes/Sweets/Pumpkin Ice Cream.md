---
tags:
  - untried
---
From: *Unknown library book* page 52
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can | Coconut Milk |
| 1/2 Cup (about 8) | Dates | 
| 1 Cup | Milk (almond, cashew, hemp) |
| 1 Cup | Pumpkin |
| 1 teaspoon | Vanilla |
| 2 1/2 teaspoon | Cinnamon |
| 1/2 teaspoon | Ginger |
| 1/2 teaspoon | Nutmeg |
| 1/8 teaspoon | Salt |

1. Puree
	1. Coconut Milk
	2. Dates
2. Add 
	1. Milk 
	2. Pumpkin
	3. Vanilla
	4. Cinnamon
	5. Ginger
	6. Nutmeg
	7. Salt
3. Chill
4. Put in Ice Cream Maker