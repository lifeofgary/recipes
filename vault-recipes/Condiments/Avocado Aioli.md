---
tags:
  - untried
---
From: *Yogurt Bible* page 170 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 | Avocado |
| 1 Tablespoon | Lemon Juice |
| 2 Clove | Garlic |
| 1 1/2 C | Yogurt (drained for 10 min)

Blend everything