---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 61
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil (divided) |
| 6 | Chicken Legs |
| 1 1/2 teaspoon | Salt |
| 1 | Onion (red) |
| 6 Cloves | Garlic |
| 1 inch | Ginger |
| 1/2 teaspoon | Turmeric |
| 1 Teaspoon | Honey |
| 1 Tablespoon | Vinegar |
| 1/4 Cup | Lemon Juice |
| 3/4 Cup | Broth |
| 1/2 Cup | Green Olives |
| 1/2 Cup | [[Preserved Lemons]] |

1. In Instapot Sauté for 2 1/2  minutes per side
	1. Oil (2 Tablespoon)
	2. Chicken
	3. Salt (1/2 teaspoon)
2. Set `Chicken` aside
3. Sauté for 7  minutes
	1. Oil (1 Tablespoon)
	2. Onion
	3. Garlic
	4. Ginger
	5. Turmeric
	6. Salt (1 Teaspoon)
4. Add
	1. Honey
	2. Vinegar
	3. Lemon Juice
	4. Broth
	5. Olives
	6. [[Preserved Lemons]]
	7. Chicken
5. Cook on `Poultry` for 15 minutes
