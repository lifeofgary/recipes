---
tags:
  - untried
  - instapot
  - hummas
---
From: *Unknown Library Book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/4 Cup | Chickpeas (dried) |
| 1 teaspoon | Baking Soda |
| 1/4 Cup | Tahini |
| 2 | Lemons (juiced) |
| 6 cloves | Garlic |
| 2 Tablespoon | Oil (olive) |
| 1 teaspoon | Salt | 
| 1/4 teaspoon | Cumin |
| 4 Cup | Ice Water |
| | Paprika (Garnish) |

1. Pressure cook for 50 minutes with manual release of 15 minutes
	1. Chickpeas
	2. Water
	3. Baking Soda
2. Drain 
3. Blend
	1. Chickpeas
	2. Tahini
	3. Lemon Juice
	4. Garlic
	5. Oil
	6. Salt
	7. Cumin