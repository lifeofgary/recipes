---
tags:
  - untried
---
From: *Unknown library book* page 28
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can | Coconut Milk |
| 1/2 Cup | Dates |
| 1 1/2 Cup | Milk (almond, cashew or hemp) |
| 1 Tablespoon | Vanilla |

1. Blend
	1. Coconut Milk
	2. Dates
2. Add
	1. Milk
	2. Vanilla
3. Cool in freeze
4. Put in Ice Cream Maker