---
tags:
  - untried
---
From: *Spice Diet* page 232
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Brown Sugar |
| 1/2 Cup | Paprika |
| 1 Tablespoon | Pepper |
| 1 Tablespoon | Chili Powder |
| 1 Tablespoon | Garlic Powder |
| 1 Tablespoon | Onion Powder |
| 2 teaspoon | Mustard Powder |
| 1 teaspoon | Cayenne |

1. Sauté for 10 minutes