---
tags:
  - bowl
  - sauce
  - untried
---
From *Paleo Power Bowls* pg 328

| Amount  | Ingredient    |
| ------- | ------------- |
| 1/2 Cup | Oil (avocado) |
| 1/4 Cup | Vinegar       |
| 2 Tbsp  | Lemon Juice   |
| 1 Tbsp  | Oregano       |
| 1/2 tsp | Coriander     |
| 1/2 tsp | Marjoram      |
| 1/2 tsp | Salt          |
1. Mix