---
tags:
  - untried
---
From: Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| 2 | Shallots | 
| 2 g | Ginger |
| 1 handful | Curry leaves |
| 14 oz | Coconut Milk |
| 570 g | Lentils (red, cooked) |

1. Sauté 
	1. Shallots
2. Add and cook for 1 min
	1. Ginger
	2. Curry Leaves