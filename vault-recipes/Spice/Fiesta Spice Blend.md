---
tags:
  - untried
---
From: *Spice Diet* page 236
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Chili Powder |
| 2 Tablespoon | Paprika |
| 1 Tablespoon | Onion Powder |
| 1 Tablespoon | Garlic Powder |
| 1 teaspoon | Red Pepper Flakes |
| 1 Tablespoon | Cumin |
| 1 teaspoon | Lime Zest |
