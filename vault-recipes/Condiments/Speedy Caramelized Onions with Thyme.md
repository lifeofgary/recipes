---
tags:
  - untried
---
From: *Nourish: The Paleo Healing Cookbook*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 6 Cup | Onion |
| 2 Tablespoon | Oil (avocado, coconut) |
| 6 Sprigs | Thyme |
| 1 teaspoon | Salt |
| 1/2 Cup | Broth (divided) |
| 1 Tablespoon | Vinegar (balsamic) |

1. Cook at medium high heat for 15 minutes
	1. Onion
	2. Oil
	3. Thyme
2. Add and cook for 5 minutes
	1. Broth (1/4 Cup)
3. Add and cook for 5 minutes
	1. Broth (1/4 Cup)
4. Add and cook for 5 minutes
	1. Vinegar
5. Remove
	1. Thyme sprigs