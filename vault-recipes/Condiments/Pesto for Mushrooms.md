---
tags:
  - untried
---
From: *Unknown library book* page 140 
Rating: ?/5

Used by [[Roasted Pesto Mushrooms]]

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Basil (fresh) |
| 1/2 Cup | Pine Nuts (roasted) |
| 3 Tablespoon | Nutritional Yeast |
| 2 Tablespoon | Oil (Olive) |
| 1/4 Cup | Water |
| 1/4 teaspoon | Garlic Powder |
| 1/2 teaspoon | Salt |

1. Combine
	1. Basil
	2. Pine Nuts
	3. Nutritional Yeast
	4. Oil
	5. Water
	6. Garlic Powder
	7. Salt
2. Blend