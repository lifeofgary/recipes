---
tags:
  - untried
---
From: *Unknown Library Book* page 105
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil (olive) |
| 1 | Avocado |
| 1/2 Cup | Flour (almond) |
| 1/4 teaspoon | Cayenne |
| 1/4 teaspoon | Paprika (smoked) |
| pinch | Salt |
| 3/4 Tablespoon | Milk (almond) |
| 1 teaspoon | Lime Juice |

1. Slice into 10 pieces
	1. Avocado 
2. Combine
	1. Flour
	2. Cayenne
	3. Paprika
	4. Salt
3. Coat Avocado with
	1. Flour mix
	2. Milk
	3. Flour mix
4. Oil tray an put in
	1. Avocado
5. Bake a 400F for 5 minutes and flip
7. Bake a 400F for 10 minutes and flip
8. Bake a 400F for 5 minutes and flip
9. Sprinkle with
	1. Lime Juice
10. Bake a 400F for 5 minutes
