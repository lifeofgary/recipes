---
tags:
  - untried
---
From: *The Vegan Air Fryer* page 42 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Potato (large) |
| 4 Cups | Water (cold) |
| 1 teaspoon | Paprika |
| 1/2 teaspoon | Garlic Powder |
| 1/4 teaspoon | Sugar |
| 1/4 teaspoon | Onion Powder |
| 1/4 teaspoon | Chili Powder |
| 1/8 teaspoon | Salt |
| 1/8 teaspoon | Mustard (ground) |
| 1/8 teaspoon | Cayenne |
| 1 teaspoon | oil (canola) |
| 1/8 teaspoon | Liquid Smoke | 

1. Slice 
	1. Potato
2. Soak for 20 minutes in
	1. Water
3. Mix in bowl
	1. Garlic Powder
	2. Sugar
	3. Onion Powder
	4. Chili Powder
	5. Salt
	6. Mustard
	7. Cayenne
4. Rinse, drain and pat dry
	1. Potato
5. Add
	1. Potato
	2. Oil
	3. Liquid Smoke
	4. Spice Mix
6. Air fry for 20 minutes at 390F