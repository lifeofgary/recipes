---
tags:
  - untried
---
From: *Dairy-Free and Gluten Free Kitchen*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Coconut Milk |
| 1/3 Cup | Lime Juice |
| 3 Tablespoon | Vinegar (rice) |
| 1 Tablespoon | Turmeric |
| 1 Tablespoon | Curry Powder |
| 1 teaspoon | Salt |
| 4 | Chicken Breasts (halved, boneless) |
| 1 | [[Spice Peanut Sauce]]

1. Mix
	1. Coconut Milk
	2. Lime Juice
	3. Vinegar
	4. Turmeric
	5. Curry Powder
	6. Salt
2. Marinade
	1. Chicken
3. Cook Chicken
4. Serve with [[Spice Peanut Sauce]]