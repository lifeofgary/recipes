---
tags:
  - bowl
  - untried
---
From *Americas test kitchen - Bowls* pg 217

| Amount  | Ingredient            |
| ------- | --------------------- |
| 3/4 Cup | Seasoned Rice Vinegar |
| 1/4 Cup | Water                 |
| 1 inch  | Orange Zest           |
| 1 Clove | Garlic                |
| 1/4 tsp | Fennel Seed           |
| 1/8 tsp | mustard seed          |
| 1 bulb  | Fennel (sliced)       |
1. Mix in 4 cup measuring cup
	1. Vinegar
	2. Water
	3. Zest
	4. Garlic
	5. Fennel seed
	6. Mustard seed
2. Microwave until boiling (3 min)
3. Stir in Fennel
4. Soak for 30 minutes
5. Drain