---
tags:
  - untried
---
From: Unknown
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Basil (fresh) |
| 1/3 Cup | Nuts (pine or walnut) |
| 1/4 Cup | Oil (olive) |
| 3 Cloves | Garlic |
| 1/2 teaspoon | Salt |
| 1 teaspoon | Lemon Juice |

1. Blend for 3 minutes
	1. Basil
	2. Nuts
	3. Oil
	4. Garlic
	5. Salt
	6. Lemon Juice