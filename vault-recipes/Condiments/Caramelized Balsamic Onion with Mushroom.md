---
tags:
  - untried
---
From: *The Dairy-Free and Gluten-Free Kitchen* page 132
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 | Onion (yellow) |
| 2 | Onion (red) |
| 1/4 lb. | Mushroom |
| 1/4 Cup | Stock |
| 1/2 teaspoon | [[Date Syrup]]
| 5 Sprig | Thyme |
| 1/2 teaspoon | Salt |
| 3 Tablespoon | Vinegar (balsamic) |
| 2 Clove | Garlic |

1. Cook over medium low heat for 20 minutes
	1. Onion
	2. Mushrooms
	3. Stock
	4. [[Date Syrup]]
	5. Thyme
	6. Salt
2. Add cook for 55 minutes
	1. Garlic
	2. Vinegar
3. Remove
	1. Thyme