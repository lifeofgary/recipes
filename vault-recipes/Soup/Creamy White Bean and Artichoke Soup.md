---
tags:
  - untried
---
From: *30-minute Vegan: Soup's on!* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 5 Cup | Stock |
| 1 Cup | Onion |
| 1/4 Cup | Fennel Bulb |
| 1/2 Cup | Celery |
| 5 Clove | Garlic |
| 1 teaspoon | rosemary (fresh) |
| 1/4 teaspoon | thyme (dried) |
| 2 x 15 oz cans or 3 Cups | Cannellini Beans |
| 1 Cup | Artichoke Hearts |
| 1 Cup | Bell Pepper (Red) |
| 1 1/2 Tablespoon | Oil (Olive) |
| 2 1/2 Teaspoon | Salt | 
| 2 teaspoon |  Vinegar (Balsamic) |
| 1 Tablespoon | Nutritional Yeast |
| 2 Tablespoon | Basil (Fresh) |
| 2 teaspoon | marjoram (Fresh) |

1. Cook for 30 minutes
	1. Onion
	2. Fennel
	3. Celery
	4. Garlic
	5. Rosemary
	6. Thyme
	7. Cannellini Beans
2.  Broil for 10 minutes
	1. Oil
	2. Artichoke Hearts
	3. Bell Peppers
	4. 1/4 teaspoon Salt
3. Mix everything and blend