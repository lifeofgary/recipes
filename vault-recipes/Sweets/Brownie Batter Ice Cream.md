---
tags:
  - untried
---
From: *Unknown library book* page 100
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Batch | [[Chocolate Ice Cream]] |
| 1 Batch | [[Brownie Batter]] |

1. Fold into [[Chocolate Ice Cream]]
	1. [[Brownie Batter]]
2. Freeze
