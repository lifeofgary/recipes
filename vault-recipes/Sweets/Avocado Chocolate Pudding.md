---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 | Avocado |
| 1/4 Cup | Coconut Milk |
| 4 Tablespoon | Cocoa Powder |
| 1 teaspoon | Stevia |
| 1/4 Cup | Dark Chocolate |
| 2 teaspoon | Vanilla |
| 1 teaspoon | Cinnamon |
| Pinch | Salt |

1. Blend
	1. Avocado
	2. Coconut Milk
	3. Cocoa Powder
	4. Stevia
	5. Dark Chocolate
	6. Vanilla
	7. Cinnamon
	8. Salt
2. Refrigerate for 8 hours

Variations:
- 2 teaspoon `Lemon Juice`
- 2 teaspoon `Lemon Zest`
- Handful of `Mint Leaves`