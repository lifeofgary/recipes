---
tags:
  - untried
---
From: *Spice Diet* page 238
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/3 teaspoon | Fenugreek |
| 1 teaspoon | Coriander |
| 3-7 teaspoon | Paprika (smoked) |
| 1 teaspoon | Cumin |
| 1/2 teaspoon | Cloves |
| 1/2 teaspoon | Cardamon |
| 1/2 teaspoon | Cinnamon |
| 1 teaspoon | Turmeric |
| 1 1/2 teaspoon | Ginger |
| 3/4 teaspoon | Cayenne |
| 1 teaspoon | Garam Masala |
