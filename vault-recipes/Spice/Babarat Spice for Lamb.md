---
tags:
  - untried
---
From: *Spice: flavors of the eastern mediterranean* page 232
Rating : ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Cinnamon |
| 1 Tablespoon | Nutmeg |
| 1 Tablespoon | Cumin |
| 1 Tablespoon | Coriander |
| 2 Tablespoon | Mint (dried) |
| 2 Tablespoon | Oregano |
| 2 Tablespoon | Pepper |
| 4 | Bay Leaves |
| 1 teaspoon | Fennel |
| 1 teaspoon | Allspice |
| 1 teaspoon | Cloves |
| 1 Tablespoon | Mustard Seed |

1. Sauté for 10 minutes