---
tags:
  - untried
---
From: *Spice Diet* page 232
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Dill |
| 3 Tablespoon | Lemon Pepper |
| 1 teaspoon | Garlic Powder |

