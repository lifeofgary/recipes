---
tags:
  - untried
  - hummas
---
From: *Unknown Library Book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Chickpeas |
| 1/2 Cup | Tahini |
| 1 | Lime (juiced) |
| 1/2 Tablespoon | Salt |
| 3 teaspoon | Onion Powder |
| 1/2 teaspoon | Paprika |
| 2/3 teaspoon | Celery Seed |
| 3 Clove | Garlic |
| 4 Tablespoon | Oil (olive)

1. Blend
	1. Chickpeas
2. Add and blend
	1. Tahini
	2. Lime Juice
	3. Salt
	4. Onion Powder
	5. Paprika
	6. Celery Seed
	7. Garlic
	8. Oil
3. Thin with water to desired thickness