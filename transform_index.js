const path = require("path");
let   fs      = require("fs");
const { execSync } = require("child_process");

SITE_NAME = `lifeofgary.gitlab.io/recipes/`

function copy_file(source, destination) {
    fs.copyFile(source, destination, (err) => {
        if (err) {
            console.error(err)
            throw err;
        }
      });
    
}

function change_file(filename, callback) {
    let txt = fs.readFileSync(filename).toString();
    txt = callback(txt);
    fs.writeFileSync(filename, txt, function(err) {
        if(err) {
            console.log(err);
            process.exit()
        }
    });
}

function loop_through_folders(folder, callback, directories_to_skip = []) {
    for (let file of fs.readdirSync(folder)) {
        let filename = path.join(folder, file);
        let  stat = fs.lstatSync(filename);
        
        if (stat.isDirectory()) {
            if (directories_to_skip.includes(file))
                continue;
            loop_through_folders(filename, callback)
        } else {
            callback(filename)
        }
    } 
}

function main() {
    // TODO verify file exists
    let filename = path.join("site", "index.html");

    change_file(filename, txt=>{
        let include_js = (to_replace, replace_with) => {
            txt = txt.replace(to_replace, `${to_replace}\n  ${replace_with}`)
        }
    
        include_js(`<script src="lib/scripts/webpage.js"></script>`, `<script src="lib/scripts/lunr.js"></script>`)
        include_js(`<script src="lib/scripts/webpage.js"></script>`, `<script src="lib/scripts/lunr_index.js"></script>`)
        include_js(`<script src="lib/scripts/webpage.js"></script>`, `<script src="lib/scripts/index.js"></script>`)
        include_js(`</form>`, `<div id="search_results"></div>`)
        include_js(`</form>`, `<div id="result_count"></div>`)
        return txt
    })

    // Copy files for search
    copy_file("index.js", "site/lib/scripts/index.js")
    copy_file("lunr.js", "site/lib/scripts/lunr.js")

    // Remove the _template directory from being published
    fs.rmSync("site/_templates/", { recursive: true, force: true });

    // Hide the expand twisty for _templates in all generated html files
    loop_through_folders("site", filename =>{
        change_file(filename, txt=>{

            txt =  txt.replace(
                `<div class="tree-item mod-tree-folder mod-collapsible is-collapsed" data-depth="1">`,
                `<div class="tree-item mod-tree-folder mod-collapsible is-collapsed" data-depth="1" style="visibility: collapse;">`
            )
            let include_script = (to_replace, replace_with) => {
                txt = txt.replace(to_replace, `${to_replace}\n  ${replace_with}`)
            }
            let insert_before = (to_replace, replace_with) => {
                txt = txt.replace(to_replace, `${replace_with}\n  ${to_replace}`)
            }
            include_script(`<script src="lib/scripts/webpage.js"></script>`, `<script src="lib/scripts/cactus.js"></script>`)
            include_script(`<script src="lib/scripts/webpage.js"></script>`, `<link rel="stylesheet" href="lib/styles/cactus.css">`)

            include_script(`<script src="lib/scripts/webpage.js"></script>`, `<script>
            document.addEventListener("DOMContentLoaded", () => {
                console.log("Test");
                initComments({
                  node: document.getElementById("comment-section"),
                  defaultHomeserverUrl: "https://matrix.cactus.chat:8448",
                  serverName: "cactus.chat",
                  siteName: "lifeofgary.gitlab.io-recipes",
                  commentSectionId: "${filename.replace("/", '-').replace(".", "-").replace(" ", "-")}"
                })
            })
            </script>
            `)
            insert_before(`<div class="mod-footer"></div>`, `<div id="comment-section"></div>`)
            return txt
        })
    })

    // Copy Cactus Comment Files
    copy_file("cactus.js", "site/lib/scripts/cactus.js")
    copy_file("cactus.css", "site/lib/styles/cactus.css")
}

main();
