---
tags:
  - untried
---
From: *Magazine  Marlene's Sound Outlook* Nov 8 
Rating: ?/5

Used by [[Roasted Vegetable Pot Pie]]

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Margarine |
| 2 | Portobello Mushroom |
| 1 Cup | Wine (red) |
| 2 Cup | Broth |
| 1/4 Cup | Flour (gluten free) |

1. Sauté for 10 minutes