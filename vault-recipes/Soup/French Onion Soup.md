---
tags:
  - untried
  - instapot
---
From: *Unknown library book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 5 Cup | Onion |
| 1/4 teaspoon | Baking Soda |
| 1 Cup | Stock |
| 1 Tablespoon | Sherry |
| 1 Sprig | Thyme |
| to taste | Salt |
| to taste | Sugar |

1. Mix
	1. Onion
	2. Baking Soda
	3. Salt
	4. Sugar
2. Divide into 3 canning jars, with loose lid (quarter turn)
3. Place on Instapot mesh with 1 inch water
4. Cook for 40 minutes on high pressure
5. Release naturally
6. Cool under tepid water
7. Simmer for 12 minutes
8. Add
	1. Stock
	2. Sherry
	3. Thyme
9. Boil