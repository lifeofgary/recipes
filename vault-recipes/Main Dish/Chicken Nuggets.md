---
tags:
  - untried
---
From: *Unknown library book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 | Chicken (breast, boneless, skinless) |
| 2 Cup | Flour (almond) |
| 2 Tablespoon | Arrowroot |
| 2 teaspoon | Salt |
| 1 teaspoon | Paprika |
| 1 teaspoon | Onion Powder |
| 1/2 teaspoon | Mustard (dry) |
| 3/4 Cup | Oil (olive) |


1. Cut in 1x1 inch chunks
	1. Chicken
2. Mix in bowl
	1. Flour
	2. Arrowroot
	3. Salt
	4. Paprika
	5. Onion Powder
	6. Mustard
3. Put in bowl
	1. Oil
4. Dip `Chicken` in
	1. Oil
	2. Flour
5. Bake at 400F for 20 minutes