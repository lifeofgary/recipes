---
tags:
  - untried
---
From: *Mastering Sauces* page 20
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 1/2 lbs | Chicken Bones |
|1 | Onion |
| 1 | Carrot |
| 1 | Celery Stick |
| 2 Tablespoon | Oil |
| optional | herbs | 

1. Preheat oven to 450
2. Cook bones for 30 minutes
3. Turn bones and cook until a rich mohogany brown (about 20 minutes)
4. Sauté veggies in oil until brown (about 12 minutes)
5. Add water and boil for several hours
