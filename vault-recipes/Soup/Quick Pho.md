---
tags:
  - untried
---
From: *Unknown Library Book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 lbs | Beef (top sirloin) |
| 1 | Onion (yellow) |
| 3 inch | Ginger | 
| 2 pods | Star Anise |
| 3 inch | Cinnamon (Stick) |
| 1 teaspoon | Coriander (seeds) |
| 1/2 teaspoon | Fennel (seeds) |
| 2 | cloves (whole) |
| 8 Cups | Stock (Beef) |
| 1/4 Cup | Fish Sause | 
| 8 oz | Noodles (rice, dried) |
| 4 | Scallions |
| 1 Bunch | Cilantro |
| 1 Bunch | Basil (Thia) |
| 1 Bunch | Mint |
| 2 | Limes (Quartered) |
| 1/4 Cup | [[Hoisin Sauce]]
| 1 Package | Bean Sprouts |

1. Cook at medium high heat
	1. Onion
	2. Ginger
	3. Star Anise
	4. Cinnamon
	5. Coriander
	6. Fennel
	7. Cloves
2. Cover with Boiling Water and set aside for 10 minutes
	1. Noodles
3. Arrange on Platter
	1. Green Onions
	2. Cilantro
	3. Basil
	4. Mint
	5. Bean Sprouts
	6. Limes
4. Drain Noodles and add to Broth
5. Slice Beef and serve