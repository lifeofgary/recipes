---
tags:
  - untried
---
From: *Spice Diet* page 231
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oregano |
| 3 Tablespoon | Basil |
| 1 1/2 teaspoon | Garlic Powder |
| 1/4 teaspoon | Red Pepper Flakes |

