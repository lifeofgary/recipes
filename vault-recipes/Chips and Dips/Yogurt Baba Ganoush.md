---
tags:
  - untried
  - sauce
---
From:  *Yogurt Bible* page 170 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Eggplant |
| 2 clove | Garlic |
| 1/2 Cup | Parsley (fresh) |
| 1 Tablespoon or 1/4 Cup | Lemon Juice |
| 1/2 Cup | Yogurt (drained for 8 hours) |

1. Seed and bake for 30-40 minutes
	1. Eggplant
2. Mix in
	1. Garlic
	2. Parsley
	3. Lemon Juice
	4. Yogurt
