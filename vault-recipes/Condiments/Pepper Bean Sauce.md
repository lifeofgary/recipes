---
tags:
  - untried
---
From: *Dairy-Free and Gluten-Free Kitchen* page 120 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil (olive) |
| 1 | Onion (large, yellow) |
| 3 cloves | Garlic |
| 2 Cup | Broth |
| 1 | Bell Pepper (red, quartered) |
| 2 Cup | Chickpeas (drained) |
| 1/4 Cup | White Wine |
| 1 1/2 Tablespoon | Curry Powder (yellow) |
| 1/4 teaspoon | Salt |
| 1 teaspoon | Seasoned Rice Vinegar |

1. Sauté for 3 minutes
	1. Onion
2. Add and Sauté for 1 minute
	1. Garlic
3. Stir in
	1. Stock
	2. Red Peppers
	3. Beans
	4. Wine
	5. Curry Powder
	6. Salt
4. Boil
5. Simmer for 30 minutes
6. Blend for 2 minutes
7. Stir in
	1. Vinegar