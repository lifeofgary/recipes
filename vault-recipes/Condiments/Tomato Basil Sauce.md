---
tags:
  - untried
  - instapot
---
From: *Instant Pot Cookbook* page 58 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 1/2 lb. | Tomato (Roma) |
| 1 | Onion | 
| 4 clove | Garlic |
| 2 Tablespoon | Oil (olive) |
| 1 teaspoon | Garlic Powder |
| 1 | Bay Leaf |
| 1 teaspoon | Salt |
| 2 teaspoon | [[Italian Spice Blend]] |
| 1/3 Cup | Basil (fresh) |

1. Sauté for 10 minutes
	1. Oil
	2. Garlic
	3. Onion
2. Add
	1. Tomato
	2. Garlic Powder
	3. Bay Leaf
	4. Salt
	5. [[Italian Spice Blend]]
3. Select "Steam" for 3 minutes
4. Release naturally
5. Mix in Basil