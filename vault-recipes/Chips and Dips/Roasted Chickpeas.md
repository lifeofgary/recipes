---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 can (14 oz) | Chickpeas |
| 2 Tablespoon | Oil |
| 1 teaspoon | Salt |
| 2 teaspoon | Spice Mix |

1. Drain and dry
	1. Chickpeas
2. Mix with
	1. Oil
3. Roast at 450F for 40 minutes, shaking every 10 minutes
4. In Bowl Add
	1. Salt
	2. Spice Mix

Use any spice blend or
- 1 teaspoon `paprika`, 1/2 teaspoon `thyme`
- 1/2 teaspoon `cilantro`, 1 1/2 teaspoon `cumin`
- 1 Tablespoon `toasted sesame seed`,  1 teaspoon `thyme`, 1 teaspoon `sumac`
