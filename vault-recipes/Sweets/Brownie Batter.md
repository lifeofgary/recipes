---
tags:
  - untried
---
From: *Unknown library book* page 100
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Water |
| 1 Cup | Nut Butter (almond, sunflower) |
| 1/2 Cup | Cocoa Powder |
| 1/4 Cup | Sugar (coconut) |
| 1/8 teaspoon | Vanilla |
| 1/8 teaspoon | Stevia |
| 1/2 teaspoon | Salt |

1. Mix together
	1. Water
	2. Nut Butter
	3. Cocoa Powder
	4. Sugar
	5. Vanilla
	6. Stevia
	7. Salt