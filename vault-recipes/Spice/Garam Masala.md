---
tags:
  - untried
---
From: *Unknown Library Book* page 247 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Cardamom |
| 1 stick or 1 teaspoon | Cinnamon |
| 1 teaspoon | Pepper |
| 1 whole or 3/4 teaspoon ground | Cloves  |
| 1 whole or 3/4 teaspoon ground | Cumin |
| 1 whole or 3/4 teaspoon ground |Fennel |
| 1/2 teaspoon | Nutmeg |
| 1 | Bay Leaf |

## Variation 

From: [The Herb and Spice Companion](https://kcls.bibliocommons.com/v2/record/S82C605634) page 154

| Amount | Ingredient |
|----------|-------------|
| 2 teaspoon | Cinnamon |
| 2 1/2 teaspoon | Cloves |
| 2 teaspoon | Bay Leaf (ground) |
| 2 1/2 teaspoon | Cumin |
| 1 Tablespoon | Pepper |
| 1 1/2 teaspoon | mace |
| 1 1/2 teaspoon | Cardamom |

