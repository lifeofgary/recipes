---
tags:
  - untried
---
From: *Rice Diet*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 teaspoon | oil (canola) |
| 1 | onion |
| 15 oz can | black beans |
| 15 oz can | kidney beans |
| 1 Cup | rice (cooked) |
| 1/2 Cup | Salsa |

1. Sauté for 10 minutes
	1. Oil
	2. Onion
2. Add
	1. black beans
	2. kidney beans
	3. rice
	4. salsa
3. Blend