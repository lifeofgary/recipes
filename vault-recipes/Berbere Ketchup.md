---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 207
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (olive) |
| 1 | Onion |
| 4 cloves | Garlic |
| 2 teaspoon | [[Berbere]]
| 1 can (28 oz) | Tomato (crushed) |
| 1/2 Cup | Vinegar |
| 1/4 Cup | Brown Sugar |
| 2 Tablespoon | Tomato paste |
| 1 Tablespoon | Lemon Juice |
| 2 teaspoon | Salt |

1. Sauté for 2 minutes
	1. Oil
	2. Onion
	3. Garic
	4. [[Baharat]]
2. Add
	1. Tomatoes
	2. Vinegar
	3. Brown Sugar
	4. Tomato paste
	5. Lemon Juice
	6. Salt
3. Cook until thickened