---
tags:
  - untried
---
From: *30-minute Vegan: Soup's On* page 107 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 5 Cup | Stock |
| 1/2 teaspoon | Saffron threads |
| 2 Tablespoon | Arame (seaweed, optional) |
| 1 1/4 | Onion (yellow) |
| 4 Cloves | Garlic | 
| 1/2 Cup | Mushrooms (shitake) |
| 1/2 Cup | Rice (uncooked basmati) |
| 1 Tablespoon |  Paprika | 
| 1/4 teaspoon | liquid smoke |
| 1 can or 1 1/2 Cup | Tomatoes (fire-roasted or fresh) |
| 1 Can or 1 1/2 Cup | Chickpeas |
| 1 1/2 teaspoon | Salt |
| 3 Tablespoon | Lemon Juice |
| 2 teaspoon | Tamari |
| 1/2 Cup | Artichoke Hearts |
| 3 Tablespoon | Parsley (fresh) |


1. Add and cook for 15 minutes
	1. Stock
	2. Saffron
	3. Arame
	4. Onion
	5. Garlic
	6. Mushrooms
	7. Rice
	8. Paprika
	9. Liquid Smoke
	10. Tomatoes
	11. Chickpeas
	12. Salt
	13. Lemon Juice
	14. Tamari
2. Add and cook for 5 minutes
	1. Artichoke Hearts 
3. Add Parsley

Variations
1. Sauté Onion and Garlic in 1 1/2 Tablespoon of Oil for 3 minutes
2. Add 1/2 Cup of diced Carrot or Parsnip with Rice
3. Add 1 1/2 Cup of chopped Spinach or Kale with Artichoke Hearts
4. Replace Rice with Quinoa