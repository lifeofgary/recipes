---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 51
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Sesame Seed (white) |
| 1 Tablespoon | Oregano |
| 1 teaspoon | Dill |
| 1 teaspoon | Thyme |
| 1/2 teaspoon | Salt |
| 1 Tablespoon | Sumac |

1. Sauté for 10 minutes