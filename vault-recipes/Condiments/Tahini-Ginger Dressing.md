---
tags:
  - untried
---
From: *Great Bowls of Food* page 143
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup  | Tahini |
| 1/4 Cup | Oil (olive) |
| 1/4 Cup | Water |
| 2 Tablespoon| Tamari
| 1 1/2 Tablespoon | Lemon Juice |
| 1 1/2 teaspoon | Vinegar (red wine) |
| 1 1/2 teaspoon | Vinagar (white wine) |
| 2 teaspoon | Ginger (fresh) |
| 3 Cloves | Garlic |

1. Blend
	1. Tahini
	2. Oil
	3. Water
	4. Tamari
	5. Lemon Juice
	6. Vinegars
	7. Ginger
	8. Garlic