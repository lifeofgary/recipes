---
tags:
  - untried
---
From: *Unknown library book* page
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 5 Tablespoon | Oil (olive) |
| 1 1/2 teaspoon | Paprika |
| 4 Clove | Garlic |
| 2 teaspoon | Cumin |
| 1/4 teaspoon | Cayenne |
| 1/8 teaspoon | Salt |

1. Combine
	1. Oil
	2. Paprika
	3. Garlic
	4. Cumin
	5. Cayenne
	6. Salt
2. Microwave for 15-30 seconds
3. It should be bubbling and fragrent