---
tags:
  - untried
---
From: From: *Dairy-Free and Gluten Free Kitchen**
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Onion |
| 4 | Carrot |
| 2 Stalk | Celery |
| 1/4 Cup | Tomato (sun dried) |
| 2 Clove | Garlic |
| 4 Cup | Water |
| 4 Cup | Stock |
| 1 1/4 Cup | Lentils |
| 3 Cup | Spinach |

1. Sauté for 3 minutes
	1. Onion
	2. Water (2 Tablespoon)
2. Add and Sauté for 1 minutes
	1. Garlic
3. Add
	1. Carrot
	2. Celery
	3. Tomatoes
4. Bring to a Boil
5. Simmer for 35 minutes
6. Add
	1. Spinach
7. Cook for 3 minutes