---
tags:
  - bowl
  - sauce
  - untried
---
From *Paleo Power Bowls* pg 342

| Amount  | Ingredient    |
| ------- | ------------- |
| 1 Cup   | Mango         |
| 1 inch  | Ginger        |
| 1/2 Cup | Coconut Milk  |
| 1/4 Cup | Oil (olive)   |
| 1/4 Cup | Vinegar       |
| 2 tsp   | Maple Syrup   |
| 1/2 Cup | Basil (fresh) |
1. Mix