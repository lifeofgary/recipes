---
tags:
  - stars/5
---
From *The weeknight Dinner Cookbook*
Rating: *5/5*

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 lbs | Potatoes (baby) |
| 1 lbs | Tomatoes (cherry) |
| 2 | Onions (Yellow) |
| 1/4 C | Olive Oil |
| 2 TableSpoon | Vinegar (balsamic) |
| 4 cloves | Garlic |
| 1 tsp | Basil (dried) |
| 1 tsp | Oregano (dried) |
| 1 tsp | Thyme (dried) |
| 1 1/2 tsp | Salt (dried) |
| * 2/3 Cup | Nutritional Yeast |
| * 8 | Basil (Leaves) |

1. Preheat over to 400F
2. On a baking sheet place
3. 
	1. Potatoes
	2. Tomatoes
	3. Onions
4. In a measuring cup mix
	1. Oil
	2. Vinegar
	3. Garlic
	4. Basil
	5. Oregano
	6. Thyme
	7. Salt
5. Drizzle mixture over Potatoes
6. Cook for 35 min, until potatoes are brown and tender
7. Sprinking with yeast
8. Top with basil leaves
