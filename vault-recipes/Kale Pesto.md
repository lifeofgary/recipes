---
tags:
  - bowl
  - untried
  - sauce
---
From *Paleo Power Bowls* pg 323

| Amount  | Ingredient            |
| ------- | --------------------- |
| 1 Cup   | Kale (packed)         |
| 2 Cup   | Basil (fresh, packed) |
| 1/2 Cup | Walnuts               |
| 2 Tbsp  | Nutritional Yeast     |
| 1 Tbsp  | Vinegar               |
| 1/4 tsp | Salt                  |
| 1/2 Cup | Oil (avocado)         |