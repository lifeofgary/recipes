---
tags:
  - untried
---
From: *Unknown library book* page
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Macadamia Nuts (raw, not dry roasted) |
| 1/4 Cup | Pine Nuts (raw) |
| 1/2 Cup | Water |
| 1 Tablespoon | Lemon Juice |
| 2 teaspoon | Vinegar (apple) |
| 1/2 teaspoon | Onion Powder |
| 1/2 teaspoon | Salt (sea) |

1. Blend
	1. Macadamia Nuts
	2. Pine Nuts
	3. Water
	4. Lemon Juice
	5. Vinegar
	6. Onion Powder
	7. Salt