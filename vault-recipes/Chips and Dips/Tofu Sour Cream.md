---
tags:
  - untried
  - sauce
---
From: *Unknown Library Book* page # 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 12 oz | Silken Tofu (firm or extra-firm) |
| 1/4 Cup | Milk (nondairy) |
| 3 Tablespoon | Lemon Juice |
| 1 Tablespoon | Nutritional Yeast |
| 1 Tablespoon | Syrup (brown Rice) |
| 1/2 teaspoon | Salt |

1. Blend
	1. Tofu
	2. Milk
	3. Lemon Juice
	4. Nutritional Yeast
	5. Syrup
	6. Salt
2. Chill for 30 minutes 