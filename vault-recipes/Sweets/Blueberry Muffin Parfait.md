---
tags:
  - stars/5
---
From: *Clean Sweets* page 33 
Rating: 5/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Coconut Flour |
| 1 Tablespoon | Almond Flour |
| 1 Tablespoon | Oat Flour |
| 2 Tablespoon | Sugar |
| 1/4 teaspoon | Cinnamon |
| 1/2 teaspoon | Baking Powder |
| 1 | Flax Egg |
| 1 Tablespoon | Oil or Pumpkin |
| 1 Tablespoon | Milk (non-dairy) |
| 3 Tablespoon | Blueberry |
| 1/4 Cup | Yogurt |

1. Mix
	1. Coconut Flour
	2. Almond Flour
	3. Oat Flour
	4. Sugar
	5. Cinnamon
	6. Baking Powder
	7. Flax Egg
	8. Oil
	9. Milk
	10. Blueberry
2. Microwave for 3 minutes
3. Add Yogurt
