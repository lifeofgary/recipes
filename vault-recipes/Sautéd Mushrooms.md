---
tags:
  - stars/5
---
From: [The Herb and Spice Companion](https://kcls.bibliocommons.com/v2/record/S82C605634) page 73
Rating: 5/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 Tablespoon | Oil (olive) |
| 2 Cloves | Garlic |
| 2 1/4 Cup | Mushrooms |
| 2 Tablespoon | Wine (white) |
| 1 teaspoon | Tomato paste |
| 1/2 teaspoon | thyme |
| | Salt |

1. Sauté for 1 minutes
	1. Oil
	2. Garlic
2. Add
	1. Mushrooms
	2. Wine
	3. Tomato paste
3. Cook for 15-20 minutes
 