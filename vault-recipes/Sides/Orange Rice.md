---
tags:
  - untried
  - to-try
---
From: Rice Diet* page 359
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Brown Rice (uncooked) |
| 1 Cup | Orange Juice |
| 1 Cup | Water |
| 1/2 Cup | Raisins |
| 2 Tablespoons | Orange Zest | 

1. Sauté for 4 minutes
	1. Rice
2. Add
	1. Water
	2. Orange Juice
	3. Raisins
3. Simmer for 30 45 minutes
4. Add Zest
5. Cook for 5 minutes