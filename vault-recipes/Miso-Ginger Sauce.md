---
tags:
  - bowl
  - untried
  - sauce
---
From *Buddha Bowls - 100 nourishing one bowl meals*

| Amount     | Ingredient     |
| ---------- | -------------- |
| 1/4 Cup    | Milk (nut)     |
| 1/4 Cup    | Vinegar (Rice) |
| 2 Tbsp     | Miso (White)   |
| 2 Tbsp     | Water          |
| 1 Tbsp     | Ginger         |
| 1 1/4 Tbsp | Oil (Sesame)   |
| 1 Clove    | Garlic         |
1. Mix