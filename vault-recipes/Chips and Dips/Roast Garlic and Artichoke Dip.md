---
tags:
  - untried
---
From: *Yogurt Bible* page 170 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Head | Garlic |
| 1 Can | Artichoke |
| 1 1/3 Cup | Yogurt (drained for 8 hour) |
| 1 Tablespoon | Rosemary |
| 2 Tablespoon | Oil |
Blend everything