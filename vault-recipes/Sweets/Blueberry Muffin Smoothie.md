---
tags:
  - stars/4
---
From: Mind Diet for Beginners
Rating: 4/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 Cup | Milk (Oat) |
| 2 cup | Blueberry (frozen) |
| 1  | Banana (frozen, sliced) |
| 1/2 Cup | Oat Meal (uncooked) |
| 2 teaspoon | Vanilla |
| 3 packets | Stevia |
| 2 or 3 Tablespoons | Peanut Protein |

Mix in a blender