---
tags:
  - untried
---
From: *Spice Diet* page 226
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Basil |
| 2 Tablespoon | Parsley |
| 2 Tablespoon | Chives |
| 2 Tablespoon | Savory |
| 2 teaspoon | Rosemary |
| 2 teaspoon | Paprika | 
| 2 teaspoon | Onion (powder) |
