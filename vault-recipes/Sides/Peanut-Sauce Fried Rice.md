---
tags:
  - stars/5
---
From: *Vegan Richa's Everyday Kitchen* page 7 
Rating: 5/5

| Amount | Ingredient |
|----------|-------------|
| 2 teaspoon | Oil |
| 1/2 | Onion (Red) |
| 4 Cloves | Garlic |
| 3/4 Cup | Carrot |
| 1 | Bell Pepper (Red) |
| 7 oz | Protein (Tofu)  |
| 1 Cup | Broccoli |
| 1/2 Recipe | [[Peanut Butter Sauce]] |
| 3 Cup | Rice (Cooked) |
| 1 teaspoon | Lemon Juice |

1. Cook for 2-3 minutes
	1. Oil
	2. Onion
	3. Pinch of Salt
2. Add and cook for 2 minutes
	1. Garlic
	2. Carrot
	3. Bell Pepper
3. Add and cook for 2-3 minutes
	1. Protein
	2. Broccoli
4. Add and cook on medium for 3 minutes
	1. Peanut Butter Sauce
5. Add and cook for 2 minutes
	1. Rice
	2. Salt
6. Add and serve
	1. Lime Juice

Blend everything