---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 48
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Rose Petals |
| 1 Tablespoon | Pepper |
| 1/2 teaspoon | Cloves |
| 1/2 stick | Cinnamon |
| 1 Tablespoon + 1 teaspoon | Cumin |
| 1 Tablespoon + 1 teaspoon | Coriander |
| 1 Tablespoon | Allspice |
| 1 Tablespoon | Nutmeg |
| 1 Tablespoon | Paprika |
| 2 teaspoon | Grains of Paradise |
| 1 1/2 teaspoon | Turmeric |
| 1 teaspoon | Lavender Flowers |
| 1 teaspoon | Chamomile Flowers |
| 1 teaspoon | Nigella Seed |
| 1 teaspoon | Mustard Seed (brown) |
| 1 teaspoon | Ginger |
| 1 teaspoon | Caraway |
| 1/2 teaspoon | Fenugreek |
| 1/2 teaspoon | Cardamom |
| 1/2 teaspoon | Ajwain |
| 1/2 teaspoon | Anise |

North African

### Moroccan
Page 16 of `Spice: flavors of the eastern mediterranean`
| Amount | Ingredient |
| 1/4 Cup | Cumin |
| 3/4 teaspoon | Saffron |
| 1 1/2  teaspoon | Cinnamon |
| 1 Tablespoon | Turmeric |
| 1 teaspoon | Ginger |
| 1 Tablespoon | Pepper |
| 1/2 Cup | Paprika |


