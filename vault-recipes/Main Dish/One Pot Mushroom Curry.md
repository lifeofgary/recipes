---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 140
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon  | Oil (olive) |
| 2 Cloves | Garlic |
| 2  | Shallots |
| 3 Tablespoons | Curry Paste |
| 3/4 Cup | Mushrooms |
| 3 Cup | Coconut Milk |
| 3 Cup | Stock |

1. Sauté for 10 minutes
	1. Oil
	2. Garlic
	3. Shallots
2. Add
	1. Curry Paste
	2. Mushrooms
	3. Coconut Milk
	4. Stock
3. Cook on Manual for 6 minutes