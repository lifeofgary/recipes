---
tags:
  - untried
---
From:  *Dairy-Free and Gluten Free Kitchen*  page 96
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | [[Condiments/Pesto]] |
| 2 | Tomatoes (sliced) |
| 1 | [[Crispy Pizza Crust]] |
| | Oil (olive) |
| 1 Cup | Cheese (Vegan) |
| 1/4 teaspoon | Salt |

1. Place on `Crust`
	1. Pesto
2. Top with
	1. Tomatoes
3. Bake at 425 Ffor 20 minutes
4. Top with
	1. Cheese
5. Bake at 425F for 5 minutes
6. Sprinkle
	1. Salt