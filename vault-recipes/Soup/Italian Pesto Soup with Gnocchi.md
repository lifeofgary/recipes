---
tags:
  - untried
---
From: 30 minute vegan: Soup's on
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Potatoes |
| 3 Tablespoon | Flour | 
| 1/4 teaspoon | Salt |
| 1 Pinch | Nutmeg |

1. Mix all ingredents
2. Cook for 10 minutes
## Pesto Soup
| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (Olive) |
| 1 1/4 Cup | Onion (Yellow) |
| 6 Clove | Garlic |
| 3/4 Cup | Leeks |
| 1/2 - 3/4 | Pine or Macadamia Nuts |
| 1 1/2 Cup | Basil (Fresh) |
| 5 Cup | Stock |
| 3 Tablespoon | Lemon Juice |
| 2 teaspoon | Tamari |
| 1 teaspoon | Salt |
| 3 Tablespoon | Nutritional Yeast |
| 1 1/2 Cup | Tomato (Chopped) |
| 2 Tablespoon | Parsley (chopped) |

1. Sauté 
	1. Onion
	2. Garlic
	3. Leek
	4. Nuts
2. Add
	1. Stock
	2. Lemon Juice
	3. Tamari
	4. Salt
	5. Nutritional Yeast
	6. Basil
	7. Tomato
	8. Parsley
