---
tags:
  - untried
---
From: *Unknown Library Book* page 11 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 6 Cups | Broth |
| 3 lbs | Chicken (cooked) |
| 1/2 | Onion |
| 1 | Carrot |
| 1 bunch | Cilantro |
| 1 Cup | Dried Beans (Black or any) |
| 1 teaspoon | Salt |
| 1/2 Tablespoon | Butter |

1. On "Bean" cook in Instapot
	1. Broth
	2. Beans
	3. Salt
	4. Chicken Meat
2. Add and cook on Sauté until slightly thick
	1. Onion 
	2. Carrot
3. Add
	1. Cilantro
	2. Butter
