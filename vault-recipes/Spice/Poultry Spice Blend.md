---
tags:
  - untried
---
From: *Spice Diet* page 228
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 teaspoon | Mace |
| 1/2 teaspoon | Sage |
| 1 1/2 teaspoon | Thyme |
| 1 teaspoon | Marjoram |
| 3/4 teaspoon | Rosemary |
| 1/2 teaspoon | Nutmeg |
| 1/2 teaspoon | Pepper |
