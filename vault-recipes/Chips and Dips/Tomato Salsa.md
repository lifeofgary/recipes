---
tags:
  - untried
---
From: *Mastering Sauces* page 117
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| amount | ingredient |
| 3 (~1 lb.) | Tomatoes | 
| 1 | jalapeno |
| 1/2 | White Onion |
| 2 Tablespoon | Cilantro (chopped) |
| 1/2 | Lime Juice |
| 1 Clove | Garlic |
| 1/2 teaspoon | Salt |

1. Deseed tomatoes by cutting in half and squeezing them out
2. Remove seeds and ribs from pepper
3. Mix everything together
