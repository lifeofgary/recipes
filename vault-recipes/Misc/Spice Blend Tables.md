From: *Spice Diet* page 243

## Protein Match
From: Spice Diet page 243

| |Chicken | Beef | Lamb | Fish | Veggies |
|-|:-:|:-:|:-:|:-:|:-:|
| [[Better than Salt]] | X | X | X | X | X |
| [[Savory Soup Spice Blend]] | X | X | X | X | X |
| [[Poultry Spice Blend]] | X |  |  | X | X |
| [[Veggie Spice Blend]] |  |  |  |  | X |
| [[Italian Spice Blend]] | X |  | X | X |  |
| [[Seafood Spice Blend]] | X |  |  | X | X |
| [[BBQ Spice Blend]] | X | X |  | X | X |
| [[Chesapeake Bay Spice Blend]] | X |  |  | X | X |
| [[Citrus Spice Blend]] | X |  |  | X | X |
| [[Cajun Spice Blend]] | X | X | | X | X |
| [[Fiesta Spice Blend]] | X | X |  | X | X |
| [[Japanese Spice Blend]] |  |  |  | X | X |
| [[Masala Spice Blend]] | X | X |  | X | X |
| [[Dragon Marinade]] | X | X | X | X | X |
| [[Thai Spice Blend]] | X | X |  | X | X |
| [[Spanish Spice Blend]] | X | X | | X | X |
| [[Moroccan Spice Blend]] | X | X | X | X | X |
| [[Mediterranean Spice Blend]] | X |  | X | X | X |
| [[Falafel Spice Blend]] | X | X | X | | X |

## Spice Compatibility
From: Spice Diet page 246 and 25

| Spice | Type | Blends With | Good With |
|:-: |:-:|:-:|
| Allspice | Sweet | Cardamom, Cinnamon, Ginger, Nutmeg | Nuts, Onions, Poultry, Seafood, Root Veggies |
| Aniseed | Sweet | |
| Basil | Strong |  Garlic, Parsley, Lemongrass, Chili, Oregano, Rosemary, Coriander, Mint, Thyme  | Tomato, Olive oil, Onion, Chicken, Seafood, Strawberries, Spinach, Mushrooms, Olives, Poultry, Raspberries |
| Bay Leaf | Pungent | |
| Caraway Seed | Pungent | |
| Cardamom | Pungent | | 
| Cayenne/Chili | Hot | Cilantro, Cinnamon, Cumin, Basil, Garlic, Ginger, Oregano | Bananas, Beans, Grains, Citrus, Chocolate, Beef, Potatoes, Poultry, Seafood, Tropical Fruits |
| Celery Seed | Pungent | |
| Chervil | Mild | |
| Chives | Medium | | 
| Cilantro | Amalgamating | | 
| Cinnamon | Sweet | Allspice, Cardamom, Chiles, Cloves, Curry, Ginger, Nutmeg | Bananas, Beans, Chocolate, Cranberry, Dates, Grains |
| Cloves | Pungent | | 
| Coriander | Amalgamating | Cilantro, Cumin, Mint, Parsley | Bananas, Beans, Curry, Poultry, Root Vegetables, Seafood, Tomatoes |
| Cumin | Pungent | Cilantro, Garlic, Mint, Parsley | Avocado, Bean, Beef, Citrus, Cucumber, Gains, Onion, Poultry, Sausages, Seafood, Tomatoes |
| Dill | Strong | Aniseed, Caraway, Chives, Fennel, Mint, Oregano, Parsley, Tarragon | Carrot, Chicken soup, Potatoes, Seafood, Tomatoes, Veal |
| Fennel | Amalgamating | |
| Fenugreek | Pungent, Strong | |
| Garlic | Pungent | Basil, Chile, Coriander, Dill, Ginger, Marjoram, Oregano, Parsley, Rosemary, Sage, Thyme, Turmeric | Beans, Beef, Chicken, Lamb, Mushrooms, Onion, Pasta, Potatoes, Spinach, Seafood |
| Ginger | Pungent | Allspice, Aniseed, Chiles, Chives, Cinnamon, Cloves, Coriander, Cumin, Fennel, Garlic, Nutmeg, Pepper | Asparagus, Bananas, Carrots, Chocolate, Citrus, Coconut, Cranberry, Curry, Dates, Onions, Poultry, Raisins, Root Veggies, Seafood |
| Horseradish | Hot | |
| Lemongrass | Strong | | 
| Mace | Pungent | | 
| Marjoram | Pungent | | 
| Mint | Strong | | 
| Mustard | Hot | | 
| Nutmeg | Sweet | Allspice, Cinnamon, Cloves, Cumin, Ginger | Asparagus, Carrots, Cranberries, Green Beans, Pasta, Pumpkin, Potato |
| Oregano | Pungent |  Basil, Cinnamon, Cumin, Fennel, Garlic, Parsley, Thyme | Artichokes, Beans, Beef, Mushrooms, Nuts, Pasta, Poultry, Seafood, Tomatoes |
| Paprika | Amalgamating | Allspice, Basil, Caraway, Cardamom, Chile, Cinnamon, Cloves, Coriander, Cumin, Fennel, Garlic, Ginger, Oregano, Parsley, Rosemary, Sage, Thyme, Turmeric | Chicken, Beef, Sauces |
| Parsley | Mild | Basil, Bay Leaf, Chervil, Chives, Dill, Garlic, Oregano, Rosemary, Thyme | Asparagus, Beans, Beef, Citrus, Cranberry, Grains, Mushrooms, Nuts, Onions, Potatoes, Poultry, Seafood, Tomatoes |  
| Pepper | Pungent to Hot | |
| Poppy Seeds | Amalgamating | |
| Rosemary | Pungent | Basil, Garlic, Fennel, Oregano, Parsley, Sage, Thyme | Asparagus, Beans, Beef, Citrus, Cranberry, Gains, Mushrooms, Nuts, Onions, Potatoes, Poultry, Seafood, Tomatoes |
| Saffron | Pungent | | 
| Sage | Pungent | Garlic, Parsley, Rosemary Thyme | Anchovy, Capers, Cranberry, Beef, Green Beans, Mushrooms, Nuts, Pasta, Poultry, Seafood |  
| Sesame  Seeds | Amalgamating | |
| Star Anise | Amalgamating | |
| Savory | Pungent | |
| Tarragon |Strong | |
| Thyme Bay Leaf, Chervil, Dill, Mint, Oregano, Parsley, Sage | Artichoke, Banana, Beans, Carrots, Citrus, Cranberry, Dates, Mushrooms, Nuts, Onion, Potatoes, Poultry, Seafood, Tomatoes |
| Turmeric | Amalgamating | Allspice, Cardamom, Chile, Fennel, Garlic, Ginger, Lemongrass, Paprika, Parsley | Chicken, Vegetables, Seafood, Curries, Tagines, Soup, Roasted Veggies |
| Thyme| Pungent | |
## Pairings
[[Rice Spices]]
[[Bean Spices]]

From: Spice Diet page 248

| Vegetable | Spices |
|-|-|
| Artichoke | Bay Leaf, Parsley, Oregano, Red Pepper Flakes | Thyme |
| Asparagus | Chervil, Chives, Curry, Dill, Garlic, Lemon, Mustard, Onion, Sesame Seeds, Tarragon |
| Broccoli | Caraway, Curry, Dill, Ginger, Mint, Oregano, Red Pepper Flakes, Savory, Turmeric |
| Carrot | [[Carrot Spices]] Allspice, Basil, Bay Leaf, Caraway, Chervil, Cinnamon, Cloves, Coriander, Fennel, Ginger, Mace, Mint, Parsley, Sage, Star Anise, Tarragon, Thyme |
| Celery | Basil, Chervil, Curry, Dill, Paprika, Parsley |
| Cucumber | Allspice, Basil, Coriander, Dill, Mint, Mustard, Parsley, Tarragon |
| Eggplant | Basil, Cumin, Curry, Marjoram, Oregano, Parsley, Red Pepper Flakes, Rosemary, Savory, Thyme |
| Fennel Bulb | Basil, Caraway, Coriander, Nutmeg, Parsley, Paprika, Rosemary, Thyme |
| Green Beans | Bail, Chives, Dill, Onion, Oregano, Rosemary, Savory |
| Kale | Allspice, Caraway, Chili, Coriander, Dill, Marjoram, Mustard, Nutmeg, Tarragon, Thyme |
| Leeks | Caraway, Dill, Mustard, Paprika, Nutmeg |
| Mushrooms | [[Mushrooms Spices]], Marjoram, Nutmeg, Parsley, Oregano, Sage, Tarragon, Thyme |
| Onions | Aniseed, Basil, Bay Leaf, Caraway, Cloves, Curry, Mustard Seed, Nutmeg, Oregano, Paprika, Parsley, Thyme |
| Peas | [[Peas Spices]], Chervil, Chives, Curry, Dill, Mint, Nutmeg, Parsley, Rosemary, Tarragon, Thyme, Turmeric |
| Bell Peppers | Basil, Curry, Ginger, Mustard, Oregano, Paprika, Parsley, Rosemary, Thyme |
| Potatoes | [[Potato Spices]], Caraway, Chervil, Chives, Dill, Mace, Marjoram, Paprika, Parsley, Rosemary, Sage, Thyme, Turmeric |
| Pumpkin | Celery Leaves, Chives, Curry, ginger, Onions, Sage, Thyme |
| Spinach | Allspice, Basil, Chives, Dill, Nutmeg, Sesame Seeds, Thyme |
| Tomatoes | Allspice, Basil, Cilantro, Cumin, Curry, Dill, Fennel, Garlic, Oregano, Parsley, Paprika, Red Pepper Flakes, Rosemary, Tarragon, Thyme |
| Bananas | Cardamom, Cilantro, Cinnamon, Cloves, Coriander, Ginger, Parsley, Thyme |
| Berries | Aniseed, Basil, Cardamom, Cinnamon, Cloves, Coriander, Ginger, Mace, Mint, Nutmeg, Pepper, Star Anise |
| Blueberries | Ginger, Lemon Grass, Lemon Zest, Lime Zest, Orange Zest |
| Cherries | Basil, Mint, Lemon Zest, Orange Zest, Thyme, Vanilla |
| Dates | Cinnamon, Ginger, Orange Zest, Vanilla |
| Figs | Aniseed, Cardamom, Fennel, Ginger, Mace, Mint, Rosemary, Thyme |
| Lemon | Black Pepper, Cinnamon, Cumin, Dill, Ginger, Red Pepper Flakes, Vanilla |
| Lime | Basil, Cilantro, Lemon Grass |
| Oranges | Cardamom, Fennel, Mint, Thyme |
| Pineapple | Cloves, Ginger, Lemongrass, Mint, Rosemary |
| Pumpkin | Allspice, Black Sesame Seed, Cardamom, Cinnamon, Clove, Ginger, Nutmeg, Sage, Thyme, Vanilla, Honey |
| Raspberries | Chives, Cinnamon, Mint, Star Anise |

## Creating your own blend
From: Spice Diet page 251
- Sweet spices can balance more powerful spices
- Since pungent spice are strong, use them sparingly
- Tangy spices add an important dimension
- Use hot spices in small amounts
- Amalgamating spices tie other spices together
- Use Paprika (Amalgamating) to bring the blend to balance

A good balance
- 2% Hot - Chile, Horseradish, Mustard, Pepper
- 4% Pungent - Bay Leaf, Caraway, Cardamom, Celery Seed, Cloves, Cumin, Dill Seed, Fenugreek, Garlic, Ginger, Mace, Oregano, Rosemary, Sage, Savory, Star Anise, Thyme
- 12% Tangy/Strong - Basil, Cilantro, Dill, Fenugreek, Lemongrass, Marjoram, Mint, Tarragon
- 22% Sweet/Medium - Allspice, Aniseed, Cinnamon, Chives, Nutmeg
- 60% Amalgamating/Mild - Chervil, Coriander, Fennel, Paprika, Parsley, Poppy Seed, Sesame Seed, Turmeric

