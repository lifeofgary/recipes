---
tags:
  - untried
---
From: Unknown library book 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 cans (13 oz) | Coconut Milk |
| 1/2 teaspoon | Vanilla |
| 1/2 Cup | Honey |
| 1/2 Cup | Avocado (mashed) |
| 1/2 Cup | Carob (powder) |
| 1/2 teaspoon | Cinnamon |

1. Boil for 10 minutes
	1. Coconut Milk
	2. Vanilla
2. Remove from burner
3. Blend
	1. Honey
	2. Avocado
	3. Carob
	4. Cinnamon
4. Chill for 4 hours
5. Put into an ice cream maker