---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 83
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil (olive) |
| 2 Cloves | Garlic |
| 1 | Onion |
| 2 | Carrot |
| 1/2 teaspoon | Salt |
| 1 teaspoon | [[Italian Spice Blend]] |
| 1 Cup | Lentils |
| 4 Cup | Broth |
| 1 Can (14 oz) | Tomato |
| 3 Cup | Spinach |
| 1 Tablespoon | Vinegar (balsamic) |

1. In Instapot Sauté for 2 minutes
	1. Oil
	2. Garlic
2. Add and Sauté for 4 minutes
	1. Onion
	2. Carrot
	3. Salt
3. Add 
	1. Lentil
	2. Broth
	3. Tomato
4. Cook on `Manual` at `High Pressure` for 5 minutes
5. Vent Naturally for 15 minutes
6. Add
	1. Spinach
	2. Vinegar