---
tags:
  - untried
---
From: [The Herb and Spice Companion](https://kcls.bibliocommons.com/v2/record/S82C605634) page 152
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
|  | Parsley |
| | Thyme |
| | Rosemary |
| | Bay leaf |
| | Celery leaf |
| | Leak |

1. Tie together 
2. Discard after cooking