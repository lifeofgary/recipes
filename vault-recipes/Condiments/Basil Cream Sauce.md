---
tags:
  - untried
---
From: *Paleo Cooking from Elana's Pantry* page 86 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Coconut Cream (full fat) |
| 1 Tablespoon | Coconut Butter |
| 1 Tablespoon | Vinegar (ume plum) |
| 1 Tablespoon | Lime Juice |
| 1/2 Cup | Basil (fresh) |
| 1 teaspoon | Ginger (fresh) | 

1. Blend
	1. Coconut Cream
	2. Coconut Butter
	3. Vinegar
	4. Lime Juice
	5. Basil
	6. Ginger