---
tags:
  - untried
---
From: *The Diary-Free and Gluten-Free Kitchen* Page 124
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Milk (non-dairy) |
| 1 Tablespoon | Flour (gluten free) |
| 1/2 teaspoon | Salt |
| 3/4 lb. | mushrooms |
| 1 Tablespoon | Sherry |

1. Blend for 30 seconds
	1. Milk
	2. Flour
	3. Salt
2. Cook at medium heat for 4 minutes (starts to thicken)
3. Add 
	1. Mushroom
4. Simmer for 7 minutes