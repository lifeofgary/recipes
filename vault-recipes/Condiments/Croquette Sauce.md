Use with [[Chickpea Flour Croquettes]]

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil (canola) |
| 1 teaspoon | Mustard Seed |
| 1 Tablespoon | Lentils (black) |
| 8 oz | Tomatoes |
| 2 Tablespoon | Cilantro (fresh) |
| 1 teaspoon | Salt |

1. Sauté for 30 seconds
	1. Oil
	2. Mustard Seed
2. Add and fry for 20 seconds
	1. Lentils
3. Add and cook for 5 minutes
	1. Tomatoes
	2. Cilantro
	3. Salt