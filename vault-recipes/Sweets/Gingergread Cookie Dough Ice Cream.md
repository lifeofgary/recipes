---
tags:
  - untried
---
From: *Unknown library book* page 120
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 batch | [[Vanilla Ice Cream]] |
| 1 batch | [[Gingerbread Cookie Dough]] |

1. Fold into [[Vanilla Ice Cream]]
	1. [[Gingerbread Cookie Dough]]
2. Freeze
