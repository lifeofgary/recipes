---
tags:
  - bowl
  - untried
---
From *Americas test kitchen - Bowls* pg 214

| Amount   | Ingredient |
| -------- | ---------- |
| 3 Tbsp   | Oil        |
| 8 Cloves | Garlic     |
1. Cook over medium heat for 3-5 minutes
	1. Oil
	2. Garlic