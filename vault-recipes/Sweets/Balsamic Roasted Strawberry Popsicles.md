---
tags:
  - untried
---
From: *Nourish: The Paleo Healing Cookbook* page 184
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lb. | Strawberries |
| 1/4 Cup | Vinegar (balsamic) |
| 2 Tablespoon | Honey |
| 2 Tablespoon | Basil (chopped) |
| 1 1/2 Cup | Coconut Milk |

1. Coat `Strawberries` with
	1. Vinegar
	2. Honey
2. Roast at 400C for 30 minutes
3. Blend
	1. Strawberries
4. Put into molds
	1. 50% Strawberry
	2. 50 Coconut Milk
5. Freeze for 30 minutes
6. Insert Popsicle Stick
7. Freeze for 4 hours