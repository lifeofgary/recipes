---
tags:
  - untried
  - instapot
"":
---
From: *Unknown Library Book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Cup (adjust) | Rice (jasmine) |
| 1 Tablespoon (adjust) | Peanut Butter |
| 2 Tablespoon (adjust) | Hemp Seed |
| 1 teaspoon (adjust) | Pumpkin seeds |
| 2/3 Cup | Peanuts (minced) |
| 1/3 Cup | Dates (Chopped) |
| 1/3 teaspoon | Vanilla |
| 9 Cup | Water |

1. Put in Instapot
	1. Rice
	2. Peanut Butter
	3. Hemp Seed
	4. Pumpkin Seed
	5. Peanuts
	6. Dates
	7. Vanilla
	8. Water
2. Cook on "Rice" mode