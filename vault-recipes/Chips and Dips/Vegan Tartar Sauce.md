---
tags:
  - untried
  - sauce
---
From: *Vegan Cooking in your Air Fryer* page 126
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Yogurt (coconut or almond) |
| 1/4 Cup | Relish (dill pickle) |
| 2 Tablespoon | Lemon Juice |
| 1 teaspoon | Tarragon |

1. Mix
	1. Yogurt
	2. Relish
	3. Lemon Juice
	4. Tarragon