---
tags:
  - untried
  - people/nanci
---
From: Sun Baskets
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| | Turkey (ground) |
| 1 | [[Lemongrass Paste]]
| 1 | Cucumber |
| | Scallions |
| | Basil (fresh) |
| | Mint (fresh) |
| 1 | [[Thai Dressing]] |
| | Sombai Oelek |
| | Lettuce (butter) |
| | Lime |
| | Carrot (shredded) |
| | Roasted Cashews |

1. Fry for 7 minutes
	1. Oil
	2. Turkey
2. Add and cook for 1 minute
	1. [[Lemongrass Paste]]
3. Set aside
4. Peal and cut into 1/4 inch slices
	1. Cucumber
5. Clean and dice
	1. Scalions
6. Strip leaves from stems and  chop leaves
	1. Basil
	2. Mint
7. Mix
	1. Turkey
	2. Cucumber
	3. Scallions
	4. Basil
	5. Mint
	6. [[Thai Dressing]]
8. Trim
	1. Lettuce
9. Put Turkey mix in Lettuce
10. Garnish with 
	1. Lime wedges
	2. Carrots
	3. Cashews