---
tags:
  - stars/4
---
From: *PlantPure Comfort food* by Kim Campbell page 85 
Rating: *4/5*

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Quinoa |
| 1 1/4 Cup | Water |
| 2 Tablespoon | Nutritional Yeast |
| 1 teaspoon | Onion powder |
| 1/2 teaspoon | Garlic powder |
| 1/2 teaspoon | Baking powder |
| 1/2 teaspoon | Salt |
| 2 Tablespoon | [[Bagel Seasoning]] |

1. Preheat Overn to 425 F.  
2. Combine everying but Bagel Seasoning in a bowl and blend
	1. Quinoa
	2. Water
	3. Nutritional Yeast
	4. Onion powder
	5. Garlic powder
	6. Baking powder
3. Line a rimmed baking sheet with parchment paper and make 6 inches circles of batter
4. Bake for 20-25 minutes until golden around edges. 