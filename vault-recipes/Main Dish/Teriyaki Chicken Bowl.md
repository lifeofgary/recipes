---
tags:
  - untried
---
From: *Grain Bowl* page 75 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Brown Rice |
| 1 Tablespoon | Oil (olive) |
| 1 lb. | Chicken (boneless) |
| 2 | Carrots |
| 1 Cup | Broccoli |
| 1 | [[Teriyaki Sauce]] |
| 1 Cup | Edamame (frozen, thawed) |
| 4 | Scallions |
| | Sesame seeds |

1. Sauté for 3 minutes on each side
	1. Chicken
2. Set heat to medium and add
	1. Carrots
	2. Broccoli
3. Add and cook for 5 minutes
	1. [[Teriyaki Sauce]]
4. Garnish with
	1. Edamame
	2. Scallions
	3. Sesame seeds