---
tags:
  - untried
---
From: *Dairy-Free and Gluten Free Kitchen* page 140
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 Cup | Flour (gluten free) |
| 2 teaspoon | Baking Powder |
| 1/4 teaspoon | Xanthan Gum |
| 1/4 teaspoon | Thyme |
| 1/4 teaspoon | Oregano |
| 1/4 teaspoon | Garlic Powder |
| 1/4 teaspoon | Salt |
| 1 Cup | Water |

1. Mix
	1. Flour
	2. Baking Powder
	3. Xanthan Gum
	4. Thyme
	5. Oregano
	6. Garlic Powder
	7. Salt
2. Add Water
3. Pour on parchment papper
4. Smooth til less than 1/2 inch thick
5. Prebake for 15 minutes

Variation
- Crackers - Bake for 5-8 minutes longer