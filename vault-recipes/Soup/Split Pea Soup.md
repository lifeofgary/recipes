---
tags:
  - untried
---
From: *Soups and Stews*? page 113 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 8 Cup | Stock |
| 1 3/4 | Onion (yellow, diced) |
| 3/4 Cup | Celery |
| 3/4 Cup | Split Peas |
| 3/4 teaspoon | Rosemary (fresh) |
| 1/2 teaspoon | Thyme |
| 1 1/2 Cup | Carrot |
| 1 Cup | Potatoe |
| 1/2 teaspoon | Liquid Smoke |
| 5 oz | Bacon |
| 1 Tablespoon | Marjaoram |
| 2 3/4  teaspoon | Salt |
| 1 Tablespoon + 2 teaspoon | Tamari |

1. Cook for 40 minutes
	1. Onion
	2. Celery
	3. Split Peas
	4. Bay Leave
	5. Rosemary
	6. Thyme
2. Add and cook until thickened (about 15 min)
	1. Carrot
	2. Potato
	3. Liquid Smoke
3. Remove
	1. Bay Leave
4. Add
	1. Marjoram
	2. Salt
	3. Tamari
	4. Bacon

Variation
1. Add 1/2 Cup mushrooms with onions

Blend everything