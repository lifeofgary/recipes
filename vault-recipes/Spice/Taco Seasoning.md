---
tags:
  - untried
---
From: *PlantPure Comfort food by Kim Campbell* page 66 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Chili Powder |
| 1 1/2 teaspoon | Smoked paprika |
| 1 1/2 teaspoon | Cumin |
| 1 teaspoon | Coriander |
| 1 teaspoon | Onion Powder |
| 1 teaspoon | Garlic Powder |
| 1/2 teaspoon | Oregano |
| 1/2 teaspoon | Salt |
| 1/2 teaspoon | Pepper |
| 1/2 teaspoon | Cayenne |
