---
tags:
  - untried
  - air-frier
---
From: *Vegan Cooking in your Air Fryer* Page 50
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 large or 3 small | Onions |
| 1/2 teaspoon | Salt |

1. Put in Air-Frier in bowl and parchment paper
2. Cook for 25 minutes at 400
3. Check and cook for 20 minutes at 400
3. Peel off skin
4. Cut in half
5. Blend and salt