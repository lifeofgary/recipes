---
tags:
  - untried
---
From: *Spice Diet* page 236
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 teaspoon | Garlic Powder |
| 2 teaspoon | Onion Powder |
| 2 teaspoon | Cayenne |
| 2 teaspoon | Paprika |
| 1 Tablespoon | Thyme |
| 2 teaspoon | Pepper |
| 1/2 teaspoon | Mustard Powder |
