---
tags:
  - untried
---
From: *Great Bowls of Food* page 135
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 Tablespoon | Chia Seeds (divided) |
| 1 Cup | Milk (almond, divided) |
| 2 Tablespoon | Cacao Powder |
| 2 Tablespoons | Maple Syrup (divided) |
| 1 teaspoon | Vanilla (divided) |
| 1/4 teaspoon | Salt |
| 1 Tablespoon | Peanut Butter |
| 1 | Banana (sliced) |
| 1 Tablespoon | Dark Chocolate Chips |
| Dash | Cinnamon |

1. Mix
	1. Chia Seeds (2 Tablespoons)
	2. Milk (1/2 Cup)
	3. Cacao Powder
	4. Maple Syrup (1 Tablespoon)
	5. Vanilla (1/2 teaspoon)
	6. Salt
2. Mix in another bowl
	1. Chia Seeds (2 Tablespoons)
	2. Milk (1/2 Cup)
	3. Peanut Butter
	4. Maple Syrup (1 Tablespoon)
	5. Vanilla (1/2 teaspoon)
3. Chill for 3 hours, both bowls
4. Mix the two bowls
5. Garnish with
	1. Banana
	2. Cinnamon
	3. Chocolate Chips