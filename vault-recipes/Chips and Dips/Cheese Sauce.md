---
tags:
  - untried
  - sauce
---
From: *Unknown library book* page 218
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3/4 Cup | Broth |
| 1/3 Cup | Nutritional Yeast |
| 1/4 Cup | Sesame Seeds |
| 1/4 Cup | Oil (coconut) |
| 1 Tablespoon + 1 teaspoon | Lemon Juice |
| 1 1/2 teaspoon | Ground Mustard |
| 3/4 teaspoon | Onion Powder |
| 1/4 teaspoon | Garlic Powder |
| 1/4 teaspoon | Salt |

1. Soak for 1 hour
	1. Sesame Seeds
2. Drain and dry
3. Blend
	1. Broth
	2. Nutritional Yeast
	3. Sesame Seeds
	4. Oil
	5. Lemon Juice
4. Boil
5. Add
	1. Ground Mustard
	2. Onion Powder
	3. Garlic Powder
	4. Salt
6. Reduce heat and cook for 5 minutes or until thick