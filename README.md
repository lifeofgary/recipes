
1. Generate Website
    1. Open Obsidian
    2. Select `Export to Html`
    3. Select `site`
2. Generate Search Data
    node build_index.js
    node transform_index.js
3. Cleanup Templates
    rm -Rf site/templates/
    ./remove_template.bash    
