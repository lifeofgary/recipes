---
tags:
  - untried
---
From: *Spice Diet* page 227
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Parsley |
| 1 Tablespoon | Thyme |
| 1 Tablespoon | Garlic (powder) |
| 1 1/2 teaspoon | Chervil |
| 1 1/2 teaspoon | Basil |
| 1 1/2 teaspoon | Marjoram |
| 1 1/2 teaspoon | Celery Seed |
| 1 teaspoon | Savory |
| 1 teaspoon | Rosemary |
| 1 teaspoon | Zest (lemon) |
