---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 84
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil (olive) |
| 3 Cloves | Garlic |
| 1 | Onion |
| 2 | Carrot |
| 2 Stalks | Celery |
| 1 teaspoon | Salt |
| 1 teaspoon | Oregano |
| 1 Cup | Lentil |
| 4 Cup | Broth |
| 2 Tablespoon | Tomato Paste |
| 2 Tablespoon | Vinegar (red wine) |
| 2 Tablespoon | Parsley |

1. Sauté in Instapot for 2 minutes
	1. Garlic
	2. Oil (2 Tablespoon)
2. Add and Sauté for 3 minutes
	1. Onion
	2. Carrot
	3. Celery
	4. Salt
3. Add and Sauté for 1 minutes
	1. Oregano
4. Add
	1. Lentils
	2. Broth
	3. Tomato Paste
5. Cook on `Manual` for 25 minutes
6. Naturally release for 15 minutes
7. Add
	1. Oil (1 Tablespoon)
	2. Vinegar
	3. Parsley

Variation:
	Add Bell Peppers or Parsnip
