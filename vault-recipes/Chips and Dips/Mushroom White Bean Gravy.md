---
tags:
  - untried
  - air-frier
---
From: *The Vegan Air Fryer* page 94
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Margarine |
| 3 Cloves | Garlic |
| 1/2 Cup | Onion (yellow) |
| 1 Cup | Mushrooms (shitake) |
| 1/8 teaspoon | Sage |
| 1/8 teaspoon | Rosemary |
| 1 1/4 Cup | Broth |
| 1/4 Cup | Tamari |
| 1 can (15 oz) | White Beans |
| 1/8-1/4 Cup | Nutritional Yeast |


1. Sauté for 10 minutes
	1. Oil
	2. Garlic
	3. Onion
2. Add
	1. Mushrooms
	2. Sage
	3. Rosemary
	4. Broth
	5. Tamari
3. Boil
4. Add
	1. Beans
5. Blend for 30 seconds
6. Reduce heat and cook for 5 minutes
7. Add
	1. Nutritional Yeast
8. Cook for 5 minutes