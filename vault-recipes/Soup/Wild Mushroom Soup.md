---
tags:
  - untried
---
From: *Unknown Library Book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cups | Mushrooms (dried porcini) |
| 1 Cup | Water |
| 3 Tablespoons | Oil (Olive) |
| 2 | Leeks |
| 2 | Shallots |
| 1 Clove | Garlic | 
| 8 oz | Mushrooms (wild) |
| 5 Cups | Stock |
| 1/2 teaspoon | Thyme (dried) |
| 2/3 Cup | Coconut Milk |

1. Soak for 30 minutes then save liquid
	1. Dried Mushrooms
2. Add and Sauté for 5 minutes
	1. Oil
	2. Leeks
	3. Shallots
	4. Garlic
3. Chopped and add
	1. Fresh Mushrooms
4. Cook for a few minutes
5. Add Stock and bring to a Boil
	1. Stock
	2. Dried Mushrooms
	3. Soaking liquid
	4. Thyme
6. Cook for 30 minutes
7. Blend and add Milk