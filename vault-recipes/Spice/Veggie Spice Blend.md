---
tags:
  - untried
---
From: *Spice Diet* page 228
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 5 Tablespoon | Parsley |
| 1 Tablespoon | Oregano |
| 2 1/2 Tablespoon | Paprika |
| 2 Tablespoon | Celery Seed |
| 2 teaspoon | Cayenne Pepper |
| 1 Tablespoon | Mustard (ground) |
| 1 Tablespoon | Marjoram |
| 1 Tablespoon | Garlic Powder |
| 1 1/2 teaspoon | Savory |
| 1 teaspoon | Thyme |
| 1 teaspoon | Chili Powder |
