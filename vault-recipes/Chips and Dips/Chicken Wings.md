---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 214
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 lbs. | Chicken Wings |
| 2 Tablespoon | Oil |
| 3/4 Cup | Soy Sauce |
| | Salt |
| 1 teaspoon | Rosemary |
| 1/2 teaspoon | Marjoram |
| 1 teaspoon | Cayenne |
| 1/2 Cup | Milk (rice) |
| 2 Tablespoon | Honey |

1. In Instapot Sauté until browned
	1. Chicken
	2. Oil
2. Add
	1. Soy Sauce
	2. Salt
	3. Rosemary
	4. Marjoram
	5. Cayenne
	6. Milk
	7. Honey
3. Cook for 10 minutes with `High` pressure