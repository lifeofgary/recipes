---
tags:
  - bowl
  - sauce
  - untried
---
From *Buddha Bowls - 100 nourishing one bowl meals*

| Amount  | Ingredient                           |
| ------- | ------------------------------------ |
| 1       | Avocado                              |
| 1/2 Cup | Water                                |
| 1       | Lemon/Lime (Juiced)                  |
| 1 Clove | Garlic                               |
| 1/4 tsp | Salt                                 |
| 1/4 Cup | Herbs  (cilantro, basil, mint, dill) |
1. Mix