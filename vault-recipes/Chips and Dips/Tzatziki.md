---
tags:
  - untried
  - sauce
---
From: *The Ultimate instant plot healthy cookbook* page 264
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Yogurt (Greek) |
| 1 | Cucumber |
| 1 Tablespoon | Mint (fresh) |
| 1 Clove | Garlic |
| 1/2 teaspoon | Salt | 

1. Mix