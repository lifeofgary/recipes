---
tags:
  - untried
---
From: *Spice Diet* page 238
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Sheet | Nori (seaweed) |
| 1 teaspoon | Red Pepper Flakes |
| 2 teaspoon | Sesame Seed |
| 1/2 teaspoon | Ginger |
| 1 teaspoon | Garlic Powder |
