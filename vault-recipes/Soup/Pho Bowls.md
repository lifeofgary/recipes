---
tags:
  - untried
---
From: *30 minute vegan: Soup's on*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 6 Cups |Stock |
| 3 Tablespoon | Tamari |
| 3 | Star Anise |
| 1 | Cinnamon Stick |
| 1/2 Cup | Shallots |
| 1 Tablespoon | Ginger |
| 1 Cup | Mushrooms (shiitake) |
| 3/4 Cup | Carrot |
| 1 teaspoon | Salt |
| 1 Tablespoon | Mirin |
| 8 oz | Beef |
| 3 oz | Noodles (rice, uncooked) |
| 1 1/2 teaspoon | Lime Juice |
| 1/4 Cup | Scallions |
| 3 Tablespoon | Cilantro |

1. Cook for 15 minutes
	1. Stock
	2. Tamari
	3. Star Anise
	4. Cinnamon Stick
	5. Shallots
	6. Ginger
	7. Mushrooms
	8. Carrot
	9. Salt
	10. Mirin
	11. Beef
2. Add and cook for 7 minutes
	1. Noodles
3. Remove
	1. Star Anise
	2. Cinnamon Stick
4. Add 
	1. Lime Juice
	2. Scallions
	3. Cilantro

Variations
1. Add 1 Cup Broccoli or Peas
2. Add 2 Tablespoons Thai Basil
3. Add 2 Tablespoons of Miso Paste before serving