---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil |
| 2 | Onions |
| 4 Clove | Garlic |
| 2 teaspoon | Chili Powder 
| 2 Can (14 oz) | Chickpeas |
| 4 Cups | Stock |
| Pinch | Salt |
| 1 | Lemon (juiced) |
| 12 | Mint Leaves ||

1. Sauté for 10 minutes
	1. Oil
	2. Onion
2. Add and Sauté for 10 minutes
	1. Garlic
	2. Chili Powder
3. Add  and Simmer for 10 minutes
	1. Chickpeas
	2. Stock
	3. Salt
4. Add 
	1. Lemon Juice
	2. Mint
