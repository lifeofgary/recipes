---
tags:
  - untried
  - instapot
---
From: *Ultimate Instant Pot Healthy Cookbook* page 168
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Greek Yogurt |
| 1 teaspoon | Salt |
| 2 teaspoons | Paprika |
| 1 teaspoons | Cumin |
| 1 teaspoons | Coriander |
| 1 teaspoons | Cinnamon |
| 1/2 teaspoons | Ginger (dry) |
| 1/2 teaspoons | Turmeric |
| 1/2 teaspoons | Garlic Powder |
| 1/8 teaspoons | Nutmeg |
| 2 lb. | Chicken (boneless, skinless) |
| 1 Tablespoon | Oil (avocado) | 
| 2 Clove | Garlic |
| 1/2 inch | Ginger |
| 1/2 Cup | Water |

1. Mix Marinade
	1. Yogurt
	2. Salt
	3. Paprika
	4. Cumin
	5. Coriander
	6. Cinnamon
	7. Ginger (dry)
	8. Turmeric
	9. Garlic Powder
	10. Nutmeg
2. Coat `Chicken` with `Marinade` and refrigerator for 1+ hours
3. Sauté for 3 minutes
	1. Oil
	2. Garlic
	3. Ginger
4. Stir in
	1. Chicken
	2. Marinade
	3. Water
5. Cook for 15 minutes on High Pressure
6. Vent for 20 minutes