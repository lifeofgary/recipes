---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 180
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 10 oz | Great Norther Beans (dried, soaked) |
| 5 Cup | Water |
| 3 Cloves | Garlic |
| 3 Tablespoon | Oil (olive) |
| 1/2 teaspoon | Salt |
| 2/3 | Juice of Lemon |

1. Put in instapot
	1. Beans
	2. Water
	3. Garlic
	4. Olive oil
	5. Salt
2. Select "Manual" and cook on "High" for 30 minutes
3. Naturally release for 15 minutes
4. Drain Beans reserving 1 Cup of liquid
5. Puree Beans
6. Add Reserve 1 Tablespoon at a time