---
tags:
  - untried
---
From: [It Doesn't Taste Like Chicken](https://itdoesnttastelikechicken.com/vegan-egg-yolk/#recipe)
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Water |
| 1 Tablespoon + 1 teaspoon | Starch |
| 2 Tablespoons | Oil | 
| 2 teaspoons | Nutritional Yeast |
| 3/4 teaspoon | Black Salt (kala namak) |
| 1/4 teaspoon | Turmeric |

1. Add
	1. Water
	2. Starch
	3. Oil
	4. Nutritional Yeast
	5. Black Salt
	6. Turmeric
2. Cook at medium heat for 5 minutes
