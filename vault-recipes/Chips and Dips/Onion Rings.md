---
tags:
  - untried
---
From: *The Vegan Air Fryer* page 80 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Onion (large) |
| 1 Cup | Flour (gluten free) |
| 1/4 Cup | Flour (Chickpea) |
| 1 teaspoon | Baking Powder |
| 1 teaspoon | Salt |
| 1/2 Cup | [[Aquafaba]] or Flax Egg |
| 1 Cup | Milk (dairy free) |
| 3/4 Cup | Panko Bread Crumbs |

1. Slice in 1/4 thick slices
	1. Onion
2. Mix
	1. Flour
	2. Chickpea Flour
	3. Baking Powder
	4. Salt
4. Whisk liquids
	1. [[Aquafaba]]
	2. Milk
5. Spread on plate
	1. Panko
6. Dredge Onion in each
	1. Flour
	2. Liquids
	3. Panko
7. Cook rings for 7 minutes at 360