---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 232
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/3 Cup | Tapioca Pearls |
| 1 Cup | Milk (non-dairy) |
| 1 Cup | Water |
| 4 Tablespoon | Cocoa Powder |
| 2 | [[Vegan Egg Yolk]] |
| 1/2 Cup | Sugar |
| 1 teaspoon | Orange Zest |
| 1 teaspoon | Vanilla |

1. Add to Instapot
	1. Water (1 Cup)
	2. Metal Rack
	3. Bowl
2. In Bowl 
	1. Milk
	2. Water (1 Cup)
	3. Cocoa Powder
	4. [[Vegan Egg Yolk]]
	5. Sugar
	6. Zest
	7. Vanilla
3. Cook on `Manual` for 8 minutes on `High` pressure