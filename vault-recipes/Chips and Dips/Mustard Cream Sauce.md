---
tags:
  - untried
---
From: *The Diary-Free and Gluten-Free Kitchen* Page 124
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3/4 Cup | Stock |
| 1/3 cup | Milk (non-dairy) |
| 1/4 Cup | Whine (white) |
| 2 Tablespoons | Dijon Mustard |
| 1 Tablespoon | [[Date Syrup]] or Honey |
| 1 Tablespoon | Flour (gluten free) |
| 1 teaspoon | Vinegar |
| 1/4 teaspoon | tarragon |

1. Blend for 30 seconds
	1. Stock
	2. Milk
	3. Win
	4. Mustard
	5. Syrup
	6. Flour
	7. Vinegar
	8. Tarragon
2. Cook over medium high heat for 6 minutes, until starts to thicken
