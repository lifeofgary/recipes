---
tags:
  - untried
---
From: *Prep in an Instant* page 112 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| amount | ingredient |
| 2 cups | white beans (unsoaked ) |
| 4 | Jalapeno |
| 2 | Bell Peppers |
| 2 | Onions |
| 8 Cloves | Garlic |
| 1 Tablespoon | Cumin |
| 1 Tablespoon | Chili Powder |
| 4 Cups | Broth |
| 2 Cups | Almond Milk |
| 2 lbs | Chicken (Frozen) |
| 1 1/2 Cups | Yogurt |

1. Put everything but yogurt in instapot, set to high for 45 minutes
2. Add yogurt
