---
tags:
  - untried
---
From: *Spice: flavors of the eastern mediterranean* page 75
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 6 (3) | Limes (Oranges) Zest |
| 1/4 Cup | Sumac |
| 1/4 Cup | Fennel |
| 1 teaspoon | Aleppo Chilies |

Blend and mix