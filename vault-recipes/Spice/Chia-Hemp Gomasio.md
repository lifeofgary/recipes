---
tags:
  - untried
---
From: *Great Bowls of Food* page 12
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Sesame Seed |
| 2 Tablespoons | Chia Seed |
| 2 Tablespoons | Hemp Seed |
| 1 Tablespoon | Salt |

1. Sauté until brown
	1. Sesame Seed
	2. Chia Seed
	3. Hemp Seed
	4. Salt
2. Grind in Coffee Grinder

Can be added to Rice or veggies or pizza