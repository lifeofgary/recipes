---
tags:
  - untried
---
From: *PlantPure Comfort* food by Kim Campbell page 119 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3  | Red Peppers (or 16 ounce canned) |
| 1 Cup | toasted Walnuts |
| 1 clove | garlic |
| 1/2 Cup | bread crumbs |
| 2 Tablespoon | Pomegranate molasses |
| 1 Tablespoon | Tomato paste |
| 1 Tablespoon | Lemon juice |
| 1 teaspoon | Smoked paprika |
| 1/2 teaspoon | Red Pepper Flakes |
| 1/2 teaspoon | Cumin |
| 1/2 teaspoon | Salt |

Blend everything