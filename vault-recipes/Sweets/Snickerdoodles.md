---
tags:
  - untried
---
From: *The primal kitchen*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| amount | ingredient |
| 1 Cup | Flour (almond) |
| 1/2 Cup | Sugar (coconut) |
| 1/2 teaspoon |  Baking Soda |
| 1/8 teaspoon |  Salt |
| 1/3 Cup | Mayo |
| 1/2 teaspoon | Vanilla |
| 1/2 teaspoon | Cinnamon |