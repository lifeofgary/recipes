---
tags:
  - untried
---
From: *Unknown Library Book* page 93
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can | Coconut Milk |
| 1 Tablespoon | Honey |
| 1 teaspoon | Vanilla |
| 5 drops | Stevia (vanilla cream) |
| 1 pinch | Salt |

1. Chill Coconut Milk for 24 hours
2. Chill a metal bowl for 15 minutes
3. Scoop cream out of can and store the water
4. Blend for 1 minute
	1. Coconut Cream
5. Whip in
	1. Honey
	2. Vanilla
	3. Stevia
	4. Salt