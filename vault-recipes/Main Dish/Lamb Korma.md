---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 16
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
|  | Oil |
| 2.5 lb. | Lamb |
| 2 | Onion |
| 3 Pod | Cardamom |
| 1 teaspoon | Turmeric |
| 5 | Cloves |
| 1 teaspoon | Cumin |
| 1 teaspoon | Coriander |
| 1 teaspoon | Chili |
| 2 teaspoon | Paprika |
| 1/2 teaspoon | Cinnamon |
| 1 Tablespoon | Ginger (fresh) |
| 6 Cloves | Garlic |
| 1 Cup | Yogurt |
| 1 Can | Tomato (chopped) |


1. In Instapot Sauté for 10 minutes
	1. Oil
	2. Onion
	3. Cardamom
	4. Turmeric
	5. Cloves
	6. Cumin
	7. Coriander
	8. Chili
	9. Paprika
	10. Cinnamon
2. Sear for 2 minutes
	1. Lamb
3. Add
	1. Tomato
	2. Water (1 1/2 Cup)
	3. Salt
4. Cook on `Meat/Stew` for 45 minutes
5. Add
	1. Yogurt
