---
tags:
  - untried
---
From: https://thehiddenveggies.com/how-to-make-vegan-cheese-provolone/
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can | Coconut Milk |
| 1/2 Cup | Water (hot) |
| 1 teaspoon | Salt |
| 2 Tablespoon | Nutritional Yeast |
| 2 Tablespoon | Agar Agar | 
| 1/2 teaspoon | Lemon Juice |
| 1/4 teaspoon | Garlic Powder | 

1. Combine
	1. Coconut Milk
	2. Salt
	3. Nutritional Yeast
	4. Agar Agar
	5. Lemon Juice
	6. Garlic Powder
2. Pour Hot Water in Coconut Milk Can
3. Add Mixture
4. Stir and Heat on medium 
5. Slow boil for 6 minutes while stirring
6. Pour into a container
7. Cool