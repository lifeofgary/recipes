---
tags:
  - untried
---
From: *PlantPure Comfort food& by Kim Campbell page 68 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 teaspoon | Cinnamon |
| 1 teaspoon | Cloves |
| 1 teaspoon | Fennel seed |
| 1 teaspoon | Star Anise |
| 1/2 teaspoon | Pepper |
