---
tags:
  - untried
---
From: *Unknown Library Book* page 94
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 8 or 1/2 Cup | Dates |
| 5 Tablespoon | Water |

1. Combine and soak for 30 minutes
	1. Dates
	2. Water
2. Blend for 1 minute
3. Scape down sides
4. Repeat 3 times

Variation
1. Replace water with Juice (Orange or Pineapple) 
2. Replace Dates with Apricots or Raisins 

