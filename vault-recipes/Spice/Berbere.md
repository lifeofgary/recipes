---
tags:
  - untried
---
From: Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Paprika |
| 2 teaspoon | Ginger (dry) |
| 1 teaspoon | Coriander | 
| 1/2 teaspoon | Allspice |
| 1/2 teaspoon | Cinnamon | 
| 1/2 teaspoon | Fenugreek |
| 1/4 teaspoon | Nutmeg |
| 1/4 teaspoon | Cloves |

Blend everything

## Variation
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 28

| Amount | Ingredient |
|----------|-------------|
| 1 teaspoon | cloves |
| 1 teaspoon | Coriander |
| 2 teaspoon | Fenugreek |
| 1 teaspoon | Pepper |
| 1 teaspoon | Ajwain |
| 1 teaspoon | Allspice |
| 1 teaspoon | Cardamom |
| 2 teaspoon | Paprika |
| 2 teaspoon | Garlic Powder |
| 1 1/2 teaspoon | Ginger Powder |
| 1/2 teaspoon | Cinnamon |
| 1 teaspoon | Turmeric |
| 1 teaspoon | Nutmeg |


