---
tags:
  - untried
---
From: 30 minute vegan: Soup's on 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Slice | Bacon |
| 1 | Onion |
| 2 oz | Hazelnut | 
| 2 Teaspoon | Thyme |
| 14 oz | Chickpea |
| 25 oz | Broth |

1. Cook Bacon
2. Sauté Onion
3. Add and Cook for 1 minute
	1. Nuts
	2. Thyme
4. Add and simmer for 15 minutes
	1. Chickpea
	2. Broth
5. Garnish with
	1. Mayo
	2. Zest
	
Blend everything