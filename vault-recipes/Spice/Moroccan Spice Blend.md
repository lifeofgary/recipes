---
tags:
  - untried
---
From: *Spice Diet* page 241
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Cumin |
| 1 Tablespoon | Paprika |
| 1 Tablespoon | Coriander |
| 1 teaspoon | Cinnamon |
| 1 teaspoon | Allspice |
| 1/4 teaspoon | Cloves |
