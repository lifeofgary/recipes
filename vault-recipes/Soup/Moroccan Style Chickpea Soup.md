---
tags:
  - untried
---
From: *All-Time Best Soup* page 154
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil |
| 1 | Onion |
| 1 teaspoon | Sugar |
| 1/2 teaspoon | Salt |
| 3 Cloves | Garlic |
| 1/2 teaspoon | Paprika |
| 1/4 teaspoon | Saffron |
| 1/4 teaspoon | Ginger (dry) |
| 1/4 teaspoon | Cumin |
| 2 Can (15 oz) | Chickpea |
| 1 lb. | Potato |
| 1 Can (14 oz) | Tomato |
| 1 | Zucchini |
| 3 1/2 Cup | Broth |
| 1/2 Cup | Parsley or Mint |

1. Sauté for 5 minutes
	1. Oil
	2. Onion
	3. Sugar
	4. Salt
2. Sauté for 30 minutes
	1. Garlic
	2. Paprika
	3. Saffron
	4. Ginger
	5. Cumin
3. Stir in and cook for 30 minutes
	1. Chickpeas
	2. Potato
	3. Tomato
	4. Zucchini
	5. Broth
4. Add
	1. Parsley or Mint