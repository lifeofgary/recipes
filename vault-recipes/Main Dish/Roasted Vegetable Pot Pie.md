---
tags:
  - untried
---
From: *Magazine  Marlene's Sound Outlook* Nov 8 
Rating: ?/5

Used by [[Mushroom Gravy]]

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (olive) |
| 6 Clove | Garlic |
| 2 Tablespoon | Herb Seasoning Blend |
| 3 | Parsnips |
| 3 | Turnips |
| 4 | Carrots (large) |
| 1 | Rutabaga |
| 3 | Potato |
| 1 | [[Mushroom Gravy]]
| 1 | Pie Crust |

1. Mix
	1. Oil
	2. Garlic
	3. Herb Seasoning Blend
2. Coat with Oil Mix
	1. Parsnip
	2. Turnip
	3. Carrot
	4. Rutabaga
	5. Potato
3. Cook at 450F for 50 minutes
	1. Crust
	2. Vegetables
	3. [[Mushroom Gravy]]