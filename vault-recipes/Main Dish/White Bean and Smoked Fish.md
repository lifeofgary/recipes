---
tags:
  - untried
---
From: Unknown Library Book 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil |
| 1 | Onion |
| 1 teaspoon | Turmeric |
| 14 oz | White Bean |
| 18 oz | Broth |
| 4 oz | Mayo |
| 10 oz | Smoked Fish |
| 2 sprigs | Tarragon | 

1. Sauté for 10 minutes
	1. Onion
2. Add and cook for 1 minute
	1. Tumeric
3. Add and cook for 5 minutes
	1. Beans
	2. Broth 
	3. Mayo
4. Add and Puree
	1. Smoked Fish
5. Garnish with Tarragon