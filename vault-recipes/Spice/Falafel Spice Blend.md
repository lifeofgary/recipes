---
tags:
  - untried
---
From: *Spice Diet* page 242
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 teaspoon | Paprika (smoked) |
| 2 teaspoon | Cumin |
| 1 teaspoon | Coriander |
| 1/2 teaspoon | Cinnamon |
| 1/2 teaspoon | Nutmeg |
| 1/4 teaspoon | Cardamom |
| 1/4 teaspoon | Cloves |
