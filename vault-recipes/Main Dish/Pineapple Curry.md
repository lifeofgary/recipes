---
tags:
  - untried
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| 2 lbs. |  Chicken |
| 1 | Onion |
| 1 | Bell Pepper |
| 2 Cans (14 oz) | Coconut Milk |
| 1 Can (20 oz) | Pineapple (chunks) |
| 2 Tablespoon | Curry Paste |
| 1 Tablespoon | Fish Sauce |

1. Cook for 5 minutes
	1. Oil
	2. Chicken
2. Add and cook for 8 minutes
	1. Onion
	2. Pepper
3. Add and Simmer for 20 minutes
	1. Milk
	2. Pineapple
	3. Curry Paste
	4. Fish Sauce