---
tags:
  - untried
---
From: *Unknown library book* page 147
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Flour (almond) |
| Dash | Salt |
| 2 Tablespoon | Oil (coconut) |
| 2 Tablespoon | Honey |

1. Mix
	1. Honey
	2. Flour