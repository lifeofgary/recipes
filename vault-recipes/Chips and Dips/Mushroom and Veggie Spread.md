---
tags:
  - untried
  - instapot
---
From: *Unknown library book* page 168
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil |
| 1 | Onion |
| 3 cloves | Garlic |
| 3 | Stalks Celery |
| 1 | Celery Rib |
| 3 | Carrots |
| 1 | Bell Pepper |
| 1 Cup | Mushrooms (Porcini) |
| 3 Cup | Broth |
| 32 oz | Tomatoes |
| 2/3 teaspoon | Salt |

1. Select Sauté for 5 minute
	1. Oil
	2. Onion
	3. Garlic
	4. Celery
	5. Carrot
	6. Bell Pepper
2. Add and cook for 3 minutes
	1. Mushrooms
3. Select Manual and set the timer for 7 minutes