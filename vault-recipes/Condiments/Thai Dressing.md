---
tags:
  - untried
---
From: [Inquiring Chef](https://inquiringchef.com/simple-dressing-for-thai-salads/)
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoons | Brown Sugar |
| 3 Tablespoons | Lime Juice |
| 1 Tablespoon | Fish Sauce |

1. Mix
	1. Brown Sugar
	2. Lime Juice
	3. Fish Sauce