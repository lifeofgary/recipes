---
tags:
  - bowl
---

*Great Bowls of Food*
## 1 - Pick a Fat (1 or 2 Tablespoons)
- Avocado Oil
- Ghee
- Olive Oil
## 2 - Choose a Base (4 Cups)
- Almond Milk
- Beef Broth
- Chicken Broth
- Coconut Milk
- Pureed Vegetables
- Tomato Puree
- Vegetable Broth
## 3 - Pick a Protein (1 Cup)
- Beef
- Black Bean
- Cannellini Bean
- Chicken
- Chickpeas
- Kidney Beans
- Lentils
- Pinto Beans
- Salmon
## 4 - Vegetable (2 Cup)
- Carrot
- Celery
- Kale
- Leeks
- Mushrooms
- Onions
- Parsnips
- Bell Peppers
- Potatoes
- Spinach
- Tomatoes
## 5 Spices (optional)
- Allspice
- Basil
- Bay Leaf
- Cinnamon
- Cloves
- Coriander
- Cumin
- Curry Powder
- Garlic Powder
- Italian Seasoning
- Mustard
- Nutmeg
- Onion Powder
- Oregano
- Paprika
- Parsley
- Rosemarry
- Salt
- Thyme
## 6 - Garnish (optional)
- Bacon
- Flaxseed
- Fresh Herbs
- Pine Nuts
- Pumpkin Seeds
- Sesame Seeds
- Sunflower Seeds
- [[Soup Garnishes]]
