---
tags:
  - untried
---
From: *Unknown library book* page
Rating: ?/5

| Amount       | Ingredient         |     |
| ------------ | ------------------ | --- |
| 1 side       | Salmon             |     |
| 1 Cup        | Salt               |     |
| 1/2 Cup      | Sugar              |     |
| 2 bunches    | Coriander (fresh)  |     |
| 1 bunch      | Parsley (fresh)    |     |
| 1/2 lbs      | Shallot            |     |
| 1/2 Cup      | Molasses           |     |
| 5 Leaves     | Bay                |     |
| 2 Tablespoon | Paprika            |     |
| 2 Tablespoon | Coriander (ground) |     |
| 8 teaspoons  | oil (mustard)      |     |



1. Blend and coat Salmon
	1. Salt
	2. Sugar
	3. Coriander
	4. Parsley Shallots
2. Cover refrigerate for 2 or 3 days
3. Sprinkle on Fish
	1. Paprika
	2. Coriander
4. Smoke Fish