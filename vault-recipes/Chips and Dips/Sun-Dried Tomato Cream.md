---
tags:
  - untried
---
From: *The Diary-Free and Gluten-Free Kitchen* Page 122
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Wine (white) |
| 1/4 Cup | Vinegar (white wine) |
| 1 1/2 teaspoon | Lemon Juice |
| 1/4 teaspoon | Parsley |
| 1/4 teaspoon | Thyme |
| 1/4 Cup | Celery Leaves |
| 1 | Bay Leaf |
| 1/4 Cup | Oil packed Sun Dried Tomatoes |
| 6 oz | Tofu (silken) |
| 1/4 Cup | Milk (non-dairy) |


1. Combine and Boil for 10 minutes
	1. Wine
	2. Vinegar
	3. Lemon Juice
	4. Parsley
	5. Thyme
	6. Celery Leaves
	7. Bay Leaf
2. Strain and discard solids
3. Add 
	1. Tomatoes
	2. Tofu
	3. Milk
	4. Garlic
4. Puree
5. Simmer for 3 minutes
