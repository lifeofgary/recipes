---
tags:
  - untried
---
From: *Vegan Holiday Cooking* page 62 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 can (14 oz) | Coconut Milk |
| 3 Tablespoon | Coconut Sugar |
| 1/3 Cup | Maple Syrup |
| 1 teaspoon | Vanilla |
| 1 Tablespoon | Cinnamon |
| Pinch | Salt |

1. Mix
	1. Coconut Milk
	2. Sugar
2. Boil for 1-2 minutes
3. Remove for burner
4. Add
	1. Maple Syrup
	2. Vanilla
	3. Cinnamon
	4. Salt
5. Refrigerate for at least 45 minutes
6. Put in Ice Cream Machine