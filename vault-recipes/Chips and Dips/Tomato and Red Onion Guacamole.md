From: *The Green Barbecue* page 153
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 | Avocado (unripe, 1/4 inch cubes) |
| 1/2 | Red Onion |
| 1 | Tomato |
| 2 | Limes (juice) |
| 3/4 oz | Cilantro |
| 1 Pinch | Salt |


