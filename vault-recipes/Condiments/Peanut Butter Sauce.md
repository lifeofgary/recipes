---
tags:
  - stars/5
---
From: *Vegan Richa's Everyday Kitchen* page 2 
Rating: 5/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Peanut Butter |
| 2 1/2" | Ginger |
| 3 Cloves | Garlic |
| 2 Tablespoon | Soy Sauce |
| 1 Tablespoon | Lime Juice |
| 2 teaspoon | Vinegar (Rice) |
| 1/2 teaspoon | Salt |
| 2 1/2 Tablespoon | Sugar |
| 1/2 teaspoon | Oil (Sesame) |
| 1 can (13.5 oz) | Coconut Milk |
| 1/2 teaspoon | Tamarind (optional) |

Blend everything