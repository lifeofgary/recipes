---
tags:
  - untried
  - instapot
---
From: *prep in an instant* page 57
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Cup | Rice (uncooked brown) |
| 1 Cup | Water |
| 3 | Medium Bell Peppers |
| 1 lb. | Turkey (ground) |
| 1 teaspoon | Garlic |
| 2 teaspoon | Chili Powder |
| 1/2 teaspoon | Paprika (smoked) |
| 1/2 teaspoon | Cumin |
| 1/2 teaspoon | Oregano |
| 1/4 teaspoon | Salt |
| 1 Cup | Mushrooms|
| 1 Cup | Zucchini |
| 1/2 Cup | Onion |
| 1/2 Cup | Black Beans |

1. Cook rice
2. Remove top of peppers and clean
3. In instapot use Saute to brown turkey and spices
4. Mix and stuff peppers with
   * turkey
   * black beans
   * rice
5. Place stuffed peppers on trivet and place in instapot.  Add water.  Cook on high for 5 minutes
