---
tags:
  - bowl
  - untried
---
From *Americas test kitchen - Bowls* pg 205

| Amount  | Ingredient               |
| ------- | ------------------------ |
| 1 tsp   | Oil                      |
| 12 oz   | Cherry Tomatoes (halved) |
| 1 tsp   | Sugar                    |
| 1 Clove | Garlic                   |
| 1 Tbsp  | Basil (fresh)            |
1. Sautee for 1 minute
	1. Tomatoes
	2. Sugar
2. Add and cook for 30 sec
	1. Garlic
3. Put in bowl and add
	1. Basil