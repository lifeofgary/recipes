---
tags:
  - untried
---
From: *All-Time Best Soups* page 174 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 | Leeks |
| 2 | Carrot |
| 1/2 | Celery Root |
| 1/2 Cup | Parsley |
| 3 Tablespoon | Onion Powder |
| 2 Tablespoon | Salt |
| 1 1/2 Tablespoon | Tomato Paste |
| 3 Tablespoon | Tamari |

1. Blend
	1. Leek
	2. Carrot
	3. Celery Root
	4. Parsley
	5. Onion Powder
	6. Salt
2. Blend in
	1. Tomato paste
3. Blend in Tamari
4. Freeze in airtight container