---
tags:
  - untried
---
From: *Spice Diet* page 240
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 6 Tablespoon | Paprika (smoked) |
| 3 Tablespoon | Paprika (sweet) |
| 1 Tablespoon | Chili Powder |
| 3 Tablespoon | Cilantro |
| 2 Tablespoon | Onion Powder |
| 1 Tablespoon | Cumin |
| 1 Tablespoon | Lemon Zest |

1. Sauté for 10 minutes