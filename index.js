
// Get URL arguments
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}		

function escapeHtml(unsafe) {
    return unsafe
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;");
}

function showResultCount(query, total, domElementId) {
    if (total == 0) {
        return;
    }

    var s = "";
    if (total > 1) {
        s = "s";
    }
    var found = "<p>Found " + total + " result" + s;
    if (query != "" && query != null) {
        query = escapeHtml(query);
        var forQuery = ' for <span class="result-query">' + query + '</span>';
    }
    else {
        var forQuery = "";
    }
    var element = document.getElementById(domElementId);
    element.innerHTML = found + forQuery + "</p>";
}

function showResults(results, elementId) {
    var html = [];

    results.forEach(  r => {
        let link = r.ref
        let title = link
        var result = ('<p><span class="result-title"><a href="' + link + '">'
                    + title.replace(/\.html/, "") + '</a></span></p>');
        html.push(result);

    })
    if (html.length) {
        html =  html.join("");
    }
    else {
        html = "<p>Your search returned no results.</p>";
    }
    let el = document.getElementById(elementId)
    el.innerHTML = html;
}

document.addEventListener("DOMContentLoaded", () => {
    let query = getParameterByName("q");
    if (query != "" && query != null) {
        document.forms.lunrSearchForm.q.value = query;
        let search_data = lunr.Index.load(LUNR_DATA);
        let results = search_data.search(query)
        let count = results.length
        showResultCount(query, count, "result_count")
        showResults(results, "search_results")
    }
})
