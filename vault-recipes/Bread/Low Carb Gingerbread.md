---
tags:
  - untried
---
From: *Unknown Library Book* page 23 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Milk (dairy free) |
| 1/4 Cup | Pumpkin Seeds |
| 1/4 Cup | Oil (coconut) |
| 2 Tablespoon | Flaxseed |
| 1 Cup | Flour (almond) |
| 1/4 teaspoon | Stevia |
| 1 teaspoon | Vanilla |
| 1 teaspoon | Baking Powder |
| 2 inch | Ginger |
| 1/2 Tablespoon | [[Five Spice]] |

1. Blend
	1. Milk
	2. Pumpkin Seed
	3. Oil
	4. Flaxseed
	5. Flour
	6. Stevia
	7. Vanilla
	8. Baking Powder
	9. Ginger
	10. [[Five Spice]]
2. Bake at 350F for 45 minutes