---
tags:
  - untried
  - instapot
---
From: *Instant Pot Cookbook* page 163
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Rice |
| 1/2 Cup | Carrot |
| 2 | Scallions |
| 1 Cup | Chicken |
| 1 1/2 Cup | Water |
| 1 Tablespoon | Tamari |
| 1 Tablespoon | Oil (olive) |
| | Salt |

1. Add to Instapot
	1. Rice
	2. Carrots
	3. Scallions
	4. Chicken
	5. Water
	6. Tamarii
	7. Oil
2. Cook on `Rice`