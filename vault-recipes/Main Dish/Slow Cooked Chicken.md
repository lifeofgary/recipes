---
tags:
  - untried
  - instapot
---
From: *Instant Pot Cookbook* page 112
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 | Chicken (whole) |
| 2 | Carrot |
| 1 Stalk | Celery |
| 1 | Onion |
| 4 Cloves | Garlic |
| 1 Tablespoon | [[Herbes de Provence]] |
| | Salt |
| 1 | Lemon (juiced) |

1. Stuff Chicken with
	1. Garlic
2. Sprinkle Chicken with
	1. Salt
	2. [[Herbes de Provence]]
3. Dice and put in the Instapot
	1. Carrot
	2. Celery
	3. Onion
4. Cook on  `Slow Cook` for 3 hours
5. Sprinkle with
	1. Lemon Juice