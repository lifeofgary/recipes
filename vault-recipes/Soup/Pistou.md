---
tags:
  - untried
---
From:  Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 50 g | Basil (fresh) |
| 2 cloves | Garlic |
| 25 g | Nutritional Yeast |
| 4 Tablespoons | Oil (olive) |

1. Sauté for 10 minutes