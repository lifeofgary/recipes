---
tags:
  - air-frier
  - stars/3
---
From: *The Vegan Air Fryer* page 64
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Can (15 oz) | Chickpeas |
| 1 teaspoon | Oil (peanut) |
| 1/2 teaspoon | Maple Syrup |
| 1 teaspoon | Paprika |
| 1 teaspoon | Garlic Powder |
| 1/2 teaspoon | Mustard (ground) |

1. Mix 
	1. Chickpeas
	2. Oil
	3. Maple Syrup
2. Toss to coat
	1. Chickpeas
3. Sprinkle on
	1. Paprika
	2. Garlic Powder
	3. Mustard
4. Air Fry at 400F for 15 minutes, shake every 5 minutes