---
tags:
  - untried
  - air-frier
---
From: *Vegan Air Fryer* page 60
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 | Portobello Mushrooms (large) |
| 1/2 Cup | Flour (soy) |
| 1/2 teaspoon | Onion Powder |
| 1/4 teaspoon | Oregano |
| 1/4 teaspoon | Basil |
| 1/4 teaspoon | Garlic Powder |
| 1/2 Cup | Ice Water | 
| 2 Tablespoon | Flax Egg |
| 1/8 Cup | Milk (soy) |
| 1 teaspoon | Tamari |
| 1 Cup | Panko Bread Crumbs |
| 1/4 teaspoon | Salt |
| | Spray oil |

1. Cut in 1/4 slices
	1. Mushroom
2. Mix
	1. Mushroom
	2. Flour
	3. Onion Powder
	4. Oregano
	5. Basil
	6. Garlic Powder
3. Mix in Bowl
	1. Egg
	2. Milk
	3. Tamari
4. Dip Mushrooms and shake off excess 
	1. Egg Mix
5. Dip in
	1. Panko
6. Spay oil on Basket
7. Air fry at 360 for 7 min