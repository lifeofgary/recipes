---
tags:
  - untried
---
From: *Cooking with Spice* page 19
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 3/4 lb | Chicken |
| 1 1/2 teaspoon | Curry Powder |
| 3 Tablespoon | Oil |
| 2 Tablespoon | Onion |
| 4 cloves | Garlic |
| 1 Tablespoon | Brown Sugar |
| 1 Tablespoon | Lime Juice |
| 1/2 Cup | Peanuts (toasted, chopped) |
| 1 Cup | Coconut Milk |
| 1/4 teaspoon | Salt |

## Chicken
1. Mix together
	1. Oil (1 Tablespoon)
	2. Curry Powder
	3. Chicken
2. Let marinate while making sauce
3. Sauté
	1. Chicken
## Peanut Sauce
1. Sauté for 5 minutes
	1. Oil (2 Tablespoon)
	2. Onion
2. Add and cook for 1 minute
	1. Garlic
3. Add and cook for 8 minutes
	1. Brown sugar
	2. Lime Juice
	3. Peanuts
	4. Salt
	5. Coconut milk