---
tags:
  - untried
---
From: *Spice Diet* page 239
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Tamari |
| 2 Tablespoon | Oil (sesame) |
| 3 Tablespoon | Honey |
| 1/2 teaspoon | Red Pepper Flakes |
| 1/2 teaspoon | Ginger (ground) |
| 1 Tablespoon | Garlic Powder |
| 1 Tablespoon | [[Five Spice]] |


1. Sauté for 10 minutes