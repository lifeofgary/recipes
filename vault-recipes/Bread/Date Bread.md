---
tags:
  - untried
---
From: *Unknown Library Book* page 86
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3/4 Cup | Coconut Milk (Full Fat) |
| 2/3 Cup | Water |
| 1 1/2 teaspoon | Cinnamon |
| 1 Cup | Date (chopped) |
| 3/4 Cup | Chickpea Flour |
| 6 Tablespoon | Coconut Flour |
| 1 1/2 Tablespoon | Starch (potato) |
| 1 1/2 teaspoon | Baking Soda |
| 1/2 teaspoon | salt |
| 1/2 Cup | Sugar (coconut) |
| 2 Tablespoon | psyllium husk |
| 2 Tablespoon | Coconut Oil |

1. Combine and bring to a boil
	1. Coconut Milk
	2. Water
	3. Cinnamon
2. Stir in
	1. Dates
3. Let stand for 20 minutes
4. . Mix 
	1. Chickpea Flour
	2. Coconut Flour
	3. Starch
	4. Baking Soda
	5. Salt
	6. Sugar
	7. Psyllium
	8. Coconut Oil
5. Let stand for 5 minutes to thicken
6. Add
	1. Date Mixture
7. Bake for 50 minutes