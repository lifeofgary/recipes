const path    = require("path");
const fs      = require("fs");
const lunr    = require("lunr");
const cheerio = require("cheerio");

const HTML_FOLDER  = "site";
const OUTPUT_INDEX = "lib/scripts/lunr_index.js";
let  DIRECTORIES_TO_IGNORE = []
DIRECTORIES_TO_IGNORE.push("assets")
DIRECTORIES_TO_IGNORE.push("css")
DIRECTORIES_TO_IGNORE.push("libs")
DIRECTORIES_TO_IGNORE.push("misc")

function is_html_file(filename) {
    lower = filename.toLowerCase();
    return (lower.endsWith(".htm") || lower.endsWith(".html"));
}

function find_html_files(folder) {
    if (!fs.existsSync(folder)) {
        console.log("Could not find folder: ", folder);
        return;
    }
    let files = fs.readdirSync(folder);
    let html_files = [];
    for (let i = 0; i < files.length; i++) {
        let filename = path.join(folder, files[i]);
        let stat = fs.lstatSync(filename);
        if (stat.isDirectory()) {
            // if (stat == "assets" || stat == "css" || stat == "libs" ) 
            if (DIRECTORIES_TO_IGNORE.includes(`${stat}`)) {
                continue
            }
            let recursed = find_html_files(filename);
            for (let j = 0; j < recursed.length; j++) {
                recursed[j] = path.join(files[i], recursed[j]).replace(/\\/g, "/");
            }
            html_files.push.apply(html_files, recursed);
        }
        else if (is_html_file(filename)){
            html_files.push(files[i]);
        };
    };
    return html_files;
};

function read_and_transform_file(root, file) {
    let filename = path.join(root, file);
    let txt = fs.readFileSync(filename).toString();
    let $ = cheerio.load(txt);
    let title = $("title").text();
    if (typeof title == 'undefined') {
        title = file;
    }
    let body = $("body").text()
    if (typeof body == 'undefined') body = "";

    let url =  filename.replace(/site\//, "")
 
    let ingredients = $("tr td:nth-child(2)")
    // for (let item of ingredients.contents()) {
    //     let data = item.data ? item.data : item.children[0].data  
    //     console.log("\t", data.replace(/\(.+\)/, "") )
    // } 
    let result = []
    ingredients.contents().map((index, item) => {
        let data = item.data ? item.data : item.children[0].data
        result.push(data.replace(/\(.+\)/, ""))
    })
    let data = {
        url,
        title,
        ingredient: result.join(" ")
    }
    return data;
}

function build_index(docs) {
    let idx = lunr(function () {
        this.ref('url');
        this.field('title'); 
        this.field('ingredient'); 
        docs.forEach(function (doc) {
                this.add(doc);
            }, this);
        });
    return idx;
}

function main() {
    files = find_html_files(HTML_FOLDER);
    let docs = [];
    for (let i = 0; i < files.length; i++) {
        docs.push(read_and_transform_file(HTML_FOLDER, files[i], i));
    }

    let idx  = build_index(docs);

    // let j = JSON.stringify(idx)
    // idx = lunr.Index.load(JSON.parse(j));


    // let results = idx.search("Cinnamon")
    // console.log("Number of results = ", results.length)
    // results.forEach( result => console.log(result.ref))
    // console.log(results[0])

    let js = "const LUNR_DATA = " + JSON.stringify(idx) + ";\n"
            //  "const PREVIEW_LOOKUP = " + JSON.stringify(prev) + ";";
    fs.writeFile(path.join(HTML_FOLDER, OUTPUT_INDEX), js, function(err) {
        if(err) {
            return console.log(err);
        }
    });
}

main();
