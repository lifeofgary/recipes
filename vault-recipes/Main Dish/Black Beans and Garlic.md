---
tags:
  - untried
---
From: *Rice Diet* Pg 330
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 Cup | Black Beans (cooked) |
| 1 Cup | Onion (yellow) |
| 1 Tablespoon | garlic |
| 1/2 teaspoon | oregano |
| 1/2 teaspoon | basil |
| 2 | bay leaves |


1. Sauté for 10 minutes
	1. Onion Garlic
2. Add
	1. Black Beans
	2. Bay Leaf
	3. Oregano
	4. Basil
	5. Bay Leaves
3. Simmer for 40 minutes
4. 