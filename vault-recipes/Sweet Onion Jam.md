---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 209

Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Tablespoon | Oil (olive) |
| 4 large or 7 small | Onion |
| 2 Tablespoon | [[Baharat]] |
| 2 teaspoon | Salt |
| 1/4 Cup | Wine (red) |
| 1/4 Cup | Honey |
| 1/4 Cup | Vinegar (white wine) |
| 1 Tablespoon | Mint |

1. Sauté for 1 minutes
	1. Oil
	2. Onion
	3. [[Baharat]] (1 Tablespoon)
	4. Salt (1 teaspoon)
2. Cover Bake for 1 hour at 400F
3. Uncover and back for 1 hour at 400F