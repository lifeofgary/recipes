---
tags:
  - untried
---
From: Nourish: The Paleo Healing Cookbook
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3/4 Cup | Palm Shortening |
| 1/2 Cup | Oil (avocado) |
| 2 Tablespoon | Lemon Juice |
| Pinch | Salt |

1. Blend
	1. Shortening
	2. Oil
	3. Lemon Juice
	4. Salt