---
tags:
  - untried
---
From: *Soups and Stews*? page 103
Rating: ?/5
## Dal

| Amount | Ingredient |
|----------|-------------|
| 1 teaspoon | Cumin |
| 1 teaspoon | Mustard Seed |
| 1 teaspoon | Curry Powder |
| 1/2 teaspoon | Coriander |
| 1/2 teaspoon | [[Garam Masala]] |
| 1 3/4 Cup | Onion (yellow, diced) |
| 1 Tablespoon | Ginger |
| 4 Cloves | Garlic |
| 7 Cup | Stock |
| 3/4 Cup | Lentils (Red) |
| 1 Cup | Carrot (Chopped) |
| 1 Cup | Tomato (Chopped) |
| 2 Tablespoon | Lemon Juice |
| 2 Cup | Spinach (Chopped) |
| 2 teaspoon | Salt |
| 2 Tablespoon | Cilantro (fresh) |

1. Cook for 1 minute
	1. Cumin
	2. Mustard Seed
	3. Curry Powder
	4. Coriander
	5. [[Garam Masala]]
2. Add  and brown
	1. Onion
	2. Ginger
	3. Garlic
3. Cook for 20 minutes on Medium
	1. Stock
	2. Lentils
	3. Carrot
4. Add Cilantro

## Chickpeas

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil (sesame) |
| 2 teaspoon | Curry Powder |
| 1 1/2 Cup or 1 can | Chickpeas (cooked)
| 1 teaspoon | Salt |

1. Sauté 
	1. Oil
	2. Curry Powder
2. Add and Brown, adding water as needed
	1. Chickpeas