---
tags:
  - untried
---
From: [Food.com](https://www.food.com/recipe/lemongrass-paste-460478)
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 teaspoons | Oil (olive) |
| 1 Tablespoon | Ginger (fresh) |
| 5 Cloves | Garlic |
| 1 Stalk (1 Tablespoon Dried) | Lemongrass |

1. Blend
	1. Oil
	2. Ginger
	3. Garlic
	4. Lemongrass
2. Add water if blending is difficult