---
tags:
  - untried
---
From: *All Time Best Soups* page 170
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil |
| 1 lb. | Ground Chicken |
| 1 | Onion |
| 4 Cup | Water |
| 4 Cup | Broth |
| 8 teaspoon | Gelatin |
| 2 | Bay Leaves |
| 2 teaspoon | Salt |

1. Sauté for 10 minutes
	1. Chicken
	2. Onion
2. Add
	1. Water
	2. Broth
	3. Gelatin
	4. Bay Leave
	5. Salt
3. Simmer for 30 minutes