---
tags:
  - untried
---
From: *Unknown library book* page 127
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil |
| 5 Clove | Garlic |
| 1/2 | Onion |
| 1 | Jalapeno |
| 2 Cup | Mushrooms |
| 1 Tablespoon | Salt |
| 1/2 teaspoon | Thyme |
| 1/4 teaspoon | Allspice |
| 1/8 teaspoon | Nutmeg |
| 1 Can (13 oz) | Coconut Milk |
| 1 Tablespoon | Starch |
| 1 lb. | Chicken (boneless) |
| 1 Cup | Peas |

1. Sauté for 3 minutes
	1. Oil
	2. Garlic
2. Add
	1. Onion
	2. Jalapeno
	3. Mushrooms
	4. Salt
	5. Thyme
	6. Allspice
	7. Nutmeg
3. Cook for 10 minutes
4. In bowl create Slurry
	1. Coconut Milk
	2. Starch
5. Add to `Onions`
6. Boil
7. Add 
	1. Chicken
8. Cook for 10 minutes
9. Serve with `Potato Wedges`