---
tags:
  - untried
---
From: *Great Bowls of Food* page 87 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 lb. | Chicken (boneless) |
| 1/4 teaspoon | Salt |
| 1 | Lime (juiced) |
| 8 | Pineapple slices |
| 4 Cup | Lettuce (romaine) |
| 2 Cup | Tomatoes (cherry, halved) |
| 1/2 | Onion (red) |
| 1 | Avocado |
|  | [[Jalapeno-Lime Vinaigrette]] page 141 |

1. Coat and marinate `Chicken` for 30 minutes with
	1. Salt
	2. Lime Juice
2. Grill until done
	1. Chicken
3. Grill for 4 minutes
	1. Pineapple
4. Slice Chicken
5. In 4 bowls distribute
	1. Lettuce
	2. Chicken
	3. Tomatoes
	4. Onion
	5. Avocado
	6. Grilled Pinapple
	7. [[Jalapeno-Lime Vinaigrette]]