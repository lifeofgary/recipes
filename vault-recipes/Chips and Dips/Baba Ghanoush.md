---
tags:
  - untried
  - air-frier
---
From: *Unknown Library Book*  
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 | Eggplants (cubed) |
| 3 Tablespoons | Oil (olive) |
| 2 teaspoon | Salt |
| 2 cloves | Garlic |
| 1 Tablespoon | Tahini |
| 1 | Lemon (juiced) |
| 1 teaspoon | Cumin |
| 1/4 teaspoon | Paprika (Smoked) |
| | Parsley (garnish) |

1. Preheat Air Frier to 400
2. Toss eggplant with 2 Tablespoon of oil
3. Cook for 10 - 15 minutes
4. Blend
	1. Eggplants
	2. Salt
	3. Garlic
	4. Tahini
	5. Lemon
	6. Cumin
	7. Paprikaw2

6. Sauté for 10 minutes