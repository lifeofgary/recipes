---
tags:
  - untried
---
From: *Nourish: The Paleo Healing Cookbook* page 214 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 3 Cups | Dark Cherries |
| 1/2 Cup | Apple Sauce |
| 1 Tablespoon | Oil (coconut or avocado) |
| 2 Tablespoon | Vinegar (balsamic) |
| 2 teaspoon | Thyme |
| 1/4 teaspoon | Salt |

1. Simmer for 10 minutes
	1. Cherries
	2. Apple Sauce
	3. Oil
	4. Vinegar
	5. Thyme
	6. Salt