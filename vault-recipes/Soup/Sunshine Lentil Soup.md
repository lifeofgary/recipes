---
tags:
  - untried
---
From: Unknown Library Book
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Oil (Olive) |
| 1 | Onion |
| 1 teaspoon | Cumin |
| 1 teaspoon | Turmeric |
| 7 oz | Lentil (red) |
| 34  oz | Broth |
| 2 oz | Lemon Juice |

1. Sauté for 10 Minutes
	1. Oil
	2. Onion
2. Add and cook for 1 minute
	1. Cumin
	2. Turmeric
3. Add and cook for 25 minutes
	1. Lentil
4. Garnish with Cilantro

