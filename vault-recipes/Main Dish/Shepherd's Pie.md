---
tags:
  - untried
---
From: *Dairy-Free and Gluten Free Kitchen*  page 88
Rating: ?/5

## Sauce

| Amount | Ingredient |
|----------|-------------|
| 1 1/2 Cup | Milk (non-dairy) |
| 1 1/2 Tablespoon | Flour (gluten-free) |
| 1 | Onion |
| 1/2 teaspoon | Thyme |
| 1/4 teaspoon | Rosemary |
| 1/4 teaspoon | Salt |
| 2 Tablespoon | Water |
| 3 Cloves | Garlic |
| 1/2 Cup | Wine (red) |
| 2 Tablespoons | Parsley |

1. Mix
	1. Milk
	2. Flour
2. Set `Milk` aside
3. Sauté for 6 minutes
	1. Onion
	2. Thyme
	3. Rosemary
	4. Salt
	5. Water
4. Add and Sauté for 5 minutes
	1. Garlic
	2. Wine
5. Add  and cook for 3 minutes
	1. Milk
6. Add Parsley

## Filling

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil (olive) |
| 1 lb. | Beef or Lamb |
| 1/2 teaspoon | Salt |
| 1/3 lb. | Mushrooms |
| 2 | Bell Pepper (red, cleaned) |
| 3/4 lb. | Carrot |
| 1/2 Cup | Stock |

1. Brown
	1. Oil
	2. Meat
	3. Salt
2. Add and cook for 5 minutes
	1. Mushrooms
	2. Bell Pepper
	3. Carrot
	4. Stock
3. Serve over Mashed Potatoes
