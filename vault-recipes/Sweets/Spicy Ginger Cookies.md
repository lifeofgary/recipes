---
tags:
  - untried
---
From: *Dairy Free and Gluten Free Kichen* page 166
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 1/2 Cup | Flour (gluten free) |
| 2 teaspoon | Baking Soda |
| 1 Tablespoon | Ginger (ground) |
| 1 teaspoon | Cinnamon |
| 1/4 teaspoon | Nutmeg |
| 1/8 teaspoon | Cloves |
| 4 Tablespoon | Crystallized Ginger |
| 3/4 Cup | [[Date Syrup]] | 
| 3 Tablespoon | Water |
| 6 | Prunes |
| 1 | [[Flax Egg]] |
| 1/4 Cup | Blackstrap Molasses |

1. Mix
	1. Flour
	2. Baking Soda
	3. Ginger
	4. Cinnamon
	5. Nutmeg
	6. Clove
2. Blend for 2 minutes
	1. Date Syrup
	2. Water
	3. Oil
	4. Prunes
	5. [[Flax Egg]]
	6. Molasses
3. Mix
	1. Wet ingredients
	2. Draw ingredients
4. Rest for 15 minutes
5. Cook at 350F for 10 minutes