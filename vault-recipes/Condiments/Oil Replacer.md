---
tags:
  - untried
---
From: *Unknown Library Book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cup | Water |
| 2 Tablespoon | Starch (arrow root) |
| 3/4 teaspoon | Xanthan Gum |

1. Whisk together
	1. Water
	2. Starch
	3. Xanthan Gum
2. Cook over medium heal for 3-5 minutes while whisking

Variation
	[[Aquafaba]]