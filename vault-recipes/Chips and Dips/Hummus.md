---
tags:
  - untried
  - hummas
---
From: *Unknown library book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 can (15 oz) | Garbanzo Beans |
| 1 Clove | Garlic (quarter) |
| 1 | Lemon (juiced) |
| 1/2 teaspoon | Salt |
| 1/4 Cup | Oil (olive) |
| | Parsley (flat-leaf) |

1. Drain and reserve liquid 
	1. Beans
2. Blend for 2 minutes
	1. Beans
	2. Garlic
	3. Lemon Juice
	4. Salt
3. Add through feed hole
	1. Oil
4. If too thick add 1 Tablespoon of reserve liquid at a time
5. Garnish with Parsley

Variation
1. Add 1/8 teaspoon 
	1. cloves or allspice
2. Add 1/4 Cup Roasted Red Pepper
3. Add 2 Tablespoon Tomato Paste