---
tags:
  - untried
---
From: [All Recipes.com](https://www.allrecipes.com/recipe/223272/herbs-de-provence/)
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoons | Rosemary |
| 1 Tablespoon | Fennel Seed |
| 2 Tablespoons | Savory |
| 2 Tablespoons | Thyme |
| 2 Tablespoons | Basil |
| 2 Tablespoons | Marjoram |
| 2 Tablespoons | Lavender Flowers |
| 2 Tablespoons | Parsley |
| 1 Tablespoon | Oregano |
| 1 Tablespoon | Tarragon |

1. Mix
	1. Rosemary
	2. Fennel Seed
	3. Savory
	4. Thyme
	5. Basil
	6. Marjoram
	7. Lavender Flowers
	8. Parsley
	9. Oregano
	10. Tarragon