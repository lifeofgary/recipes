---
tags:
  - untried
---
From:  [World Spice at Home](https://kcls.bibliocommons.com/v2/record/S82C1429542) page 218
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 5 | Dried chiles |
| 1 | Shallot |
| 2 Cloves | Garlic |
| 3 Tablespoon | [[Tikka Masala]] or [[Curry Powder#Kashmiri]] |
| 1 Tablespoon | Tomato paste |
| 1 Tablespoon | Ginger (fresh) |
| 2 teaspoon | Lemongrass paste |
| 1 Tablespoon | Honey |
| 1/2 teaspoon | Salt |

1. Sauté for 10 minutes

