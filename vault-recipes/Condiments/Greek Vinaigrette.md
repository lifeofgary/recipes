---
tags:
  - untried
---
From: *The Primal Kitchen Book* page 17
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/2 Cup | Oil (avocado) |
| 1/3 Cup | Vinegar (red wine) |
| 1 teaspoon | Oregano |
| 1/2 teaspoon | Coriander |
| 1/2 teaspoon | Marjoram |
| 1/2 teaspoon | Lemon Juice |
| 1/4 teaspoon | Oil of Oregano |
| 1/2 teaspoon | Salt |

Mix everything 