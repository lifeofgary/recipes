---
tags:
  - untried
  - air-frier
---
From: *Vegan Cooking in your air frier* page 32
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 teaspoon | Salt |
| 2 | Potato (large) |
| 4-8 Cup | Water |
| 1 | Ice Bath |
| | Oil Spray |

1. Slice
	1. Potato
2. Boil in for 5 minutes
	1. Water
	2. Salt
4. Cool with ice bath
5. Dry
6. Spray with
	1. Oil
7. Freeze for 1 hour on a baking sheet
8. Put them in a freeze bag

To reheat
1. Cook at 400F for 10 minute
2. Shake
3. Cook for 5 minutes
4. Shake
5. Cook for 5 minutes
