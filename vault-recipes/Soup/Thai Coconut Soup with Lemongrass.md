---
tags:
  - untried
---
From: *30 minute vegan: Soup's on*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 4 Cups | Stock |
| 2 Cans | Coconut Milk |
| 3 Stocks | Lemongrass |
| 1 Inch  | Galangal or Ginger |
| 2 Cloves | Garlic |
| 5 leaves | Kaffir Lime (or 3 Tablespoon Lime Juice) |
| 1 Cup | Protein (Shiitake or Chicken) |
| 1 Cup | Carrot |
| 1 Cup | Broccoli |
| 3/4 teaspoon | Salt |
| 2 Tablespoon | Tamari |
| 3/4 Cup | Peas (snow) |
| 2 Tablespoon | Cilantro (fresh) |
| 1 Tablespoon | Lime Juice |
| 1/4 Cup | Green Onion |
| 1 Tablespoon | Basil (Thai) |

1. Cook for 10 minutes
	1. Stock
	2. Coconut Milk
	3. Lemongrass
	4. Ginger
	5. Garlic
	6. Kaffir Lime Leaves
	7. Protein
	8. Carrot
2. Add and cook for 20 minutes
	1. Broccoli
3. Remove
	1. Lemongrass
	2. Kaffir Lime Leaves
	3. Galangal