---
tags:
  - untried
  - to-try
---
From: *Rice Diet* page 362
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | oil (Canola) |
| 2 Cup | Onion (Red) |
| 6 cloves | Garlic |
| 4 | Jalapenos |
| 2 Cup | Bell Pepper (red) |
| 1 Tablespoon | Cumin |
| 1 1/4 Cup | Tomato Sauce |
| 1 Tablespoon | Honey |
| 1 Tablespoon | Molasses |
| 2 Tablespoon | Mustard (dijon) |
| 15 oz can | Pinto Beans | 
| 15 oz can | Great Norther Beans |


1. Sauté for 5 minutes
	1. Onion
2. Add and Sauté for 5 mintues
	1. Garlic
	2. Jalapenos
	3. Bell Pepper
	4. Cumin
3. Add
	1. Tomato Sauce
	2. Molasses
	3. Honey
	4. Molasses
	5. Pinto Beans
	6. Great Norther Beans
4. Bake for 30 minutes at 350