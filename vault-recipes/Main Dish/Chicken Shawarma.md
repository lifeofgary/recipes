---
tags:
  - untried
  - air-frier
---
From: *Unknown Library Book*
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 8 | Chicken Thighs |
| 1 | Onion (Yellow) |
| 1 teaspoon | Cumin |
| 1 teaspoon | Coriander |
| 1/2 teaspoon | Turmeric |
| 1/2 teaspoon | Paprika |
| 1/2 teaspoon | Garlic (powder) |
| 1/4 teaspoon | Cinnamon |
| 1 teaspoon | Salt |
| 1 Tablespoon | Honey |
| 2 Tablespoon | Lemon Juice |
| 3 Tablespoon | Oil (Olive) |

1. Cut each thigh into 3 or 4 peices
2. Put pieces in glass baking dish with the onion
3. Mix together
	1. Cumin
	2. Coriander
	3. Turmeric
	4. Paprika
	5. Garlic
	6. Cinnamon
	7. Salt
	8. Honey
	9. Lemon Juice
4. Slowly pour in
	1. Oil
5. Marinade Chicken and Onion with mix for 3+ hours
6. Put in Air Frier and cook  at 360 for 3 minutes
7. Fry for 8 minutes at 160

8. Sauté for 10 minutes