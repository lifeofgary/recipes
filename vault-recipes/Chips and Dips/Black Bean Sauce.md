---
tags:
  - untried
  - sauce
---
From: *Unknown Library Book* 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Cups | Black Beans (cooked) |
| 1 Cup | Water (reserved black bean cooking liquid) |
| 1/2 Cup | Onion (Yellow) |
| 3 Clove | Garlic |
| 1 | Jalapeno |
| 3 Tablespoon | Lime Juice |
| 1/2 teaspoon | Cumin |
| 1/2 teaspoon | salt |
| 1/8 teaspoon | Pepper |

1. Blend for 1 minute
	1. Black Beans
	2. Reserved Liquid
	3. onion
	4. Garlic
	5. Jalapeno
	6. Lime Juice
	7. Cumin
	8. Salt
	9. Pepper
2. Cook over medium heat for 8 minutes (stirring)

Variation
1. For Dip - reduce reserve to 1/2 Cup