---
tags:
  - bowl
  - untried
---

From *Paleo Power Bowls* pg 296

| Amount   | Ingredient          |
| -------- | ------------------- |
| 2 crowns | Broccoli (florets)  |
| 1        | Onion (red, wedges) |
| 1        | Pepper (Red)        |
| 2        | Carrots             |
| 1/4 Cup  | Oil (olive)         |
| 2 tsp    | Salt                |
| 2 tsp    | Oregano             |
| 1 tsp    | Paprika             |
| 1 tsp    | Garlic              |
| 1 tsp    | Cumin               |
1. Place on cookie sheet
2. Drizzle oil
3. Add half spices
4. Roast at 425 for 25 minutes
5. Shake
6. Roast for 10 minutes (or until brown)