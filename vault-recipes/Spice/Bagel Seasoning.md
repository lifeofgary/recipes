---
tags:
  - untried
---
From: *PlantPure Comfort food* by Kim Campbell page 68 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1 Tablespoon | Seame seeds (black or white) |
| 1 Tablespoon | Poppy Seeds |
| 1 Tablespoon | Dried Onion |
| 1 Tablespoon | Dried Garlic |
| 2 teaspoon | Salt |
