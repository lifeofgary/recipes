---
tags:
  - untried
---
From: *Salt Fat Acid Heat* page 334
Rating: ?/5

| Amount                    | Ingredient             |
| ------------------------- | ---------------------- |
| 4 lb.                     | Chicken                |
|                           | Salt                   |
| 1 teaspoon + 1 Tablespoon | Cumin                  |
|                           | Oil (olive)            |
| 3 Tablespoon              | Margarine              |
| 2 Onion                   | Onion                  |
| 2                         | Bay Leaves             |
| Pinch                     | Saffron                |
| 2 1/2 Cup                 | Rice (cooked, basmati) |
| 1 Cup                     | Raisins                |
| 6                         | Dates (quartered)      |
| 4 1/2 Cup                 | Stock                  |
| 1 1/2                     | Lentils (cooked)       |

1. Season `Chicken`
	1. Salt
	2. Cumin (1 teaspoon)
2. Brown 
	1. Chicken
3. Cook for 25 minutes
	1. Onion
	2. Cumin
	3. Bay leaf
	4. Saffron
4. Add and brown
	1. Rice
5. Add
	1. Raisons
	2. Dates
6. Add
	1. Stock
	2. Lentils