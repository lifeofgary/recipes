---
tags:
  - untried
---
From: *All-Time Best Soups* Page 88
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 1/4 Cup | Coconut Milk |
| 3 Tablespoons | Yogurt |
| 2 Tablespoon + 1/2 teaspoon | Oil (olive) |
| 1/4 teaspoon | Lemon (zest) |
| 1/2 teaspoon | Lemon Juice |
| 1/4 teaspoon | Tarragon |
| 1 | [[Caramelized Onions ]]  |
| 3/4 teaspoon | Sugar (brown) |
| 3 oz | Mushrooms |
| 2 Cloves | Garlic |
| 3 Cup | Water |
| 3 Cup | Broth |
| 1/3 Cup | Rice |
| 12 oz | Chard |
| 9 oz | Kale |
| 1/4 Cup | Parsley |
| 2 Cup | Arugula |
| 1 teaspoon + 1/4 teaspoon | Salt |

1. Combine and chill
	1. Coconut milk
	2. Yogurt
	3. Oil (1/2 teaspoon)
	4. Lemon Zest
	5. Lemon Juice
	6. Tarragon
	7. Salt
2. Cook for 5 minutes
	1. [[Caramelized Onions ]] 
	2. Mushrooms
	3. Salt (1 teaspoon)
3. Add and Sauté for 5 minutes
	1. Garlic
4. Add
	1. Water
	2. Broth
	3. Rice
5. Simmer for 15 minutes
6. Stir in
	1. Chard
	2. Kale
	3. Parsley
7. Simmer for 10 minutes
8. Blend
9. Drizzle in chilled cream
