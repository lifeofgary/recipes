---
tags:
  - untried
  - instapot
---
From: *The Ultimate Instant Pot Cookbook* page 50 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 1/4 Cup | Pinto Beans (dry) |
| 8 Cup | Water |
| 2 teaspoon | Salt |
| 3 Tablespoon | Oil (olive) |
| 4 cloves | Garlic |
| 1 | Onion |
| 1 Can (4 oz) | Green Chiles |
| 1 teaspoon | Cumin |

1. Add to instapot
	1. Beans
	2. Water
	3. Salt
2. Cook on `Bean/Chili`
3. Natural release for 15 minutes
4. Drain Beans and reserve water
5. Add
	1. Oil (olive)
	2. Garlic
6. Sauté for 4 minutes
7. Add and Sauté for 1 minutes
	1. Green Chilis with water
	2. Cumin
8. Blend
	1. Beans
	2. Spices
	3. Add reserve water as needed