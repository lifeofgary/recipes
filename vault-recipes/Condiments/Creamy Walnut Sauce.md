---
tags:
  - untried
---
From: *Primal Blueprint Quick & Easy Meals* page 195
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| amount | ingredient |
| 1 Cup | Walnuts |
| 1 | Shallot (finely chopped) |
| 2 Tablespoon | Oil |
| 1 1/2 Cup | Coconut Milk |
1. Sauté for 10 minutes
	1. Walnuts
	2. Shallot
2. Add and simmer for 5 minutes
	1. 1 Cup Coconut Milk
3. Puree
4. Thin with
	1. 1/2 Cup Coconut Milk