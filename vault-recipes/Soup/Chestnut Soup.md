---
tags:
  - untried
---
From: 30 minute vegan: Soup's on
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 2 Tablespoon | Oil |
| 1 | Leek |
| 1 | Carrot |
| 1 | Celery |
| 12 oz | Chestnut |
| 25 oz | Broth |
| | Bacon |

1. Sauté for 10 minutes
	1. Oil
	2. Leek
	3. Carrot
	4. Celery
2. Simmer for 30 minutes
	1. Chestnut
	2. Broth
