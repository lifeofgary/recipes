---
tags:
  - untried
---
From: Unknown Library Book 
Rating: ?/5

| Amount | Ingredient |
|----------|-------------|
| 5 | Shallots (or 1 red onion) |
| 3 cloves | Garlic |
| 1 inch | Galangal (or fresh Ginger) |
| 1/3 Cup | Peanuts (raw)|
| 1/2 in | Shrimp Paste |
| 5 Cups | Stock |
| 3/4 Cup |  Peanuts (salted, crushed) |
| 2 Tablespoon | Sugar (Dark Brown) |
| 1 teaspoon | Tamarind |
| 4 oz | Green Beans |
| 2 oz | Peas or Potatoes |



1. Blend
	1. Shallots
	2. Garlic
	3. Galangal
	4. Raw Peanuts
	5. Shrimp Paste
2. Add and cook for 15 minutes
	1. Stock
	2. Sugar
	3. Salted Peanuts
3. Soak for 15 minutes
	1. Tamarind+
	2. 5 Tablespoons of Water
4. Add to soup and cook for 5 minutes
	1. Beans
	2. Peas or Potatoes
5. Add Tamarind Juice